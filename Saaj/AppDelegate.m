//
//  AppDelegate.m
//  KSIJ Mumbai
//
//  Created by Kishan on 20/10/16.
//  Copyright © 2016 Latitudetechnolabs. All rights reserved.
//

#import "AppDelegate.h"
#import "MainHomeViewController.h"
#import "RegistrationViewController.h"
#import "NewWeeklyViewController.h"
#import "EventDetailViewController.h"
@interface AppDelegate ()<HTTPClientDeleagte>
{
    NSString *device_Token;
    UIAlertView *objalert;
    NSMutableDictionary *dict;
}
@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [NSThread sleepForTimeInterval:4.0];
    
    [[UIApplication sharedApplication] setMinimumBackgroundFetchInterval:UIApplicationBackgroundFetchIntervalMinimum];
    
    if (launchOptions != nil)
    {
        UILocalNotification *locationNotification = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
       
        
        if (locationNotification != nil)
        {
            NSString *str_code=[locationNotification.userInfo valueForKey:@"NT"];
            if ([str_code isEqualToString:@"10"])
            {
                NSMutableArray *arr_eventdetail=[[NSMutableArray alloc] init];
                arr_eventdetail=[locationNotification.userInfo valueForKey:@"Eventdetail"];
                EventDetailViewController *vc=[[EventDetailViewController alloc]initWithNibName:@"EventDetailViewController" bundle:nil];
                vc.ArrEvent=arr_eventdetail;
                vc.str_notiFlag=@"Yes";
                self.Navig=[[UINavigationController alloc]initWithRootViewController:vc];
                self.Navig.navigationBarHidden=YES;
                self.window.rootViewController=self.Navig;
            }
            else
            {
                NSLog(@"From Local Notification");
                HomeScreen *vc=[[HomeScreen alloc]initWithNibName:@"HomeScreen" bundle:nil];
                self.Navig=[[UINavigationController alloc]initWithRootViewController:vc];
                self.Navig.navigationBarHidden=YES;
                self.window.rootViewController=self.Navig;
            }
            
        }
    }
    else
    {
        [self GetApiData];
        //get divice Tocken code
        
        UILocalNotification *locationNotification = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
        if (locationNotification)
        {
            application.applicationIconBadgeNumber = 0;
            NSString *path = [[NSBundle mainBundle]
                              pathForResource:@"alarm_sound" ofType:@"m4r"];
            audioPlayer = [[AVAudioPlayer alloc]initWithContentsOfURL:
                           [NSURL fileURLWithPath:path] error:NULL];
            [audioPlayer play];
        }
        
        
        NSString *str=[[NSUserDefaults standardUserDefaults] objectForKey:@"RegisterUser"];
        NSLog(@"%@",str);
        
        if ([str isEqualToString:@"YES"])
        {
            MainHomeViewController *vc=[[MainHomeViewController alloc]initWithNibName:@"MainHomeViewController" bundle:nil];
            self.Navig=[[UINavigationController alloc]initWithRootViewController:vc];
            self.Navig.navigationBarHidden=YES;
            self.window.rootViewController=self.Navig;
            [self.window makeKeyAndVisible];
        }
        else
        {
            RegistrationViewController *vc=[[RegistrationViewController alloc]initWithNibName:@"RegistrationViewController" bundle:nil];
            self.Navig=[[UINavigationController alloc]initWithRootViewController:vc];
            self.Navig.navigationBarHidden=YES;
            self.window.rootViewController=self.Navig;
            [self.window makeKeyAndVisible];
            
        }
    }
    
    
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 80000
    
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)])
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIRemoteNotificationTypeBadge |UIRemoteNotificationTypeSound |UIRemoteNotificationTypeAlert) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else
    {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    }
#else
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
#endif

    
    
    
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    
   
    
    return YES;
}
- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    device_Token = [[[[deviceToken description]
                      stringByReplacingOccurrencesOfString: @"<" withString: @""]
                     stringByReplacingOccurrencesOfString: @">" withString: @""]
                    stringByReplacingOccurrencesOfString: @" " withString: @""];
    
    NSLog(@"Device_Token -----> %@\n",device_Token);
    
    NSString* Identifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    //NSLog(@"output is : %@", Identifier);
    [[NSUserDefaults standardUserDefaults]setObject:device_Token forKey:@"DivID"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
  //  NSString *divid=[[NSUserDefaults standardUserDefaults]valueForKey:@"DivID"];
    
    NSDictionary *parameters = @{
                                 @"regID_ios" :device_Token,
                                 @"devicePlatform" :KDevicePlatform,
                                 @"deviceUUID": Identifier
                                 };
    
    NSString *registerURL = [NSString stringWithFormat:@"%@pushnotification_web.php",kAPIURL];
    ClientNoti = [[HTTPClient alloc] init];
    ClientNoti.delegate = self;
    [ClientNoti getResponseFromAPI:registerURL andParameters:parameters];
}

-(void)application:(UIApplication * )app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err
{
    
    NSString *str1 = [NSString stringWithFormat: @"Error: %@", err];
    NSLog(@"%@",str1);
    device_Token = @"1234-testing-deviciceID-40593960123";
    
    [[NSUserDefaults standardUserDefaults]setObject:device_Token forKey:@"DivID"];
    [[NSUserDefaults standardUserDefaults]synchronize];
}
#pragma mark - Notifications
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    
    NSLog(@"didReceiveRemoteNotification: %@", userInfo);
    [UIApplication sharedApplication].applicationIconBadgeNumber = [UIApplication sharedApplication].applicationIconBadgeNumber + 1;

    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:userInfo
                                                       options:(NSJSONWritingOptions)
                        (/* DISABLES CODE */ (YES) ? NSJSONWritingPrettyPrinted : 0)
                                                         error:&error];
    
    NSString *strMsg=@"";
    if (! jsonData)
    {
        strMsg=error.localizedDescription;
    }
    else
    {
        strMsg=[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
//    NSLog(@"didReceiveRemoteNotification1: %@", userInfo);
//    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
//    [[UIApplication sharedApplication] cancelAllLocalNotifications];
//    
//    NSError *error;
//    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:userInfo
//                                                       options:(NSJSONWritingOptions)
//                        (/* DISABLES CODE */ (YES) ? NSJSONWritingPrettyPrinted : 0)
//                                                         error:&error];
//    
//    NSString *strMsg=@"";
//    if (! jsonData)
//    {
//        strMsg=error.localizedDescription;
//    }
//    else {
//        strMsg=[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
//    }
    
    NSLog(@"didReceiveRemoteNotification1: %@", userInfo);
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    dict=[[NSMutableDictionary alloc]initWithDictionary:userInfo];
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:userInfo
                                                       options:(NSJSONWritingOptions)
                        (/* DISABLES CODE */ (YES) ? NSJSONWritingPrettyPrinted : 0)
                                                         error:&error];
    NSString *strMsg=@"";
    if (! jsonData)
    {
        strMsg=error.localizedDescription;
    }
    else {
        strMsg=[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    UIApplicationState state = [[UIApplication sharedApplication] applicationState];
    if (state == UIApplicationStateBackground || state == UIApplicationStateInactive)
    {
        if(userInfo.count>0)
        {
            NotificationSubScreen *vc=[[NotificationSubScreen alloc]initWithNibName:@"NotificationSubScreen" bundle:nil];
            self.Navig=[[UINavigationController alloc]initWithRootViewController:vc];
            NSMutableArray *Temp=[[NSMutableArray alloc]init];
            [Temp addObject:userInfo];
            vc.ArrpassDetail=Temp;
             vc.str_flag=@"noti";
            self.Navig.navigationBarHidden=YES;
            self.window.rootViewController=self.Navig;
        }
        
    }
    else
    {
        objalert=[[UIAlertView alloc]initWithTitle:@"Message" message:[[userInfo objectForKey:@"aps"]objectForKey:@"alert"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        objalert.tag=2;
        [objalert show];
    }

}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    [self GetApiData];
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
     application.applicationIconBadgeNumber = 0;
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
     NSLog(@"Notification user info is %@",[notification.userInfo valueForKey:@"NT"]);
   
    NSString *str_code=[notification.userInfo valueForKey:@"NT"];
    
    
    if ([str_code isEqualToString:@"10"])
    {
        UIApplicationState state = [application applicationState];
        application.applicationIconBadgeNumber = 0;
        
        if (state == UIApplicationStateActive)
        {
//            NSString *path = [[NSBundle mainBundle]pathForResource:@"alarm_sound" ofType:@"m4r"];
//            audioPlayer = [[AVAudioPlayer alloc]initWithContentsOfURL:[NSURL fileURLWithPath:path] error:NULL];
//            [audioPlayer play];
            
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:kAlertViewTitle message:[notification.userInfo objectForKey:@"AlarmName"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
           // alert.tag=1;
            
            [alert show];
        }
        else
        {
            NSMutableArray *arr_eventdetail=[[NSMutableArray alloc] init];
            arr_eventdetail=[notification.userInfo valueForKey:@"Eventdetail"];        EventDetailViewController *vc=[[EventDetailViewController alloc]initWithNibName:@"EventDetailViewController" bundle:nil];
            vc.ArrEvent=arr_eventdetail;
            vc.str_notiFlag=@"Yes";
            self.Navig=[[UINavigationController alloc]initWithRootViewController:vc];
            self.Navig.navigationBarHidden=YES;
            self.window.rootViewController=self.Navig;
            // Push Notification received in the background
        }
    }
    else
    {
        UIApplicationState state = [application applicationState];
        application.applicationIconBadgeNumber = 0;
        
        if (state == UIApplicationStateActive)
        {
            NSString *path = [[NSBundle mainBundle]pathForResource:@"alarm_sound" ofType:@"m4r"];
            audioPlayer = [[AVAudioPlayer alloc]initWithContentsOfURL:[NSURL fileURLWithPath:path] error:NULL];
            [audioPlayer play];
            
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:kAlertViewTitle message:[notification.userInfo objectForKey:@"AlarmName"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            alert.tag=1;
            
            [alert show];
        }
        else
        {
            HomeScreen *vc=[[HomeScreen alloc]initWithNibName:@"HomeScreen" bundle:nil];
            self.Navig=[[UINavigationController alloc]initWithRootViewController:vc];
            self.Navig.navigationBarHidden=YES;
            self.window.rootViewController=self.Navig;
            // Push Notification received in the background
        }
    }
   
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==1)
    {
        if (buttonIndex == 0)
        {
            [audioPlayer stop];
        }
    }
    else{
        if(dict.count>0)
        {
            NotificationSubScreen *vc=[[NotificationSubScreen alloc]initWithNibName:@"NotificationSubScreen" bundle:nil];
            self.Navig=[[UINavigationController alloc]initWithRootViewController:vc];
            NSMutableArray *Temp=[[NSMutableArray alloc]init];
            [Temp addObject:dict];
            vc.ArrpassDetail=Temp;
             vc.str_flag=@"noti";
            self.Navig.navigationBarHidden=YES;
            self.window.rootViewController=self.Navig;
        }
        
    }

}
//BG Fetch
-(void)application:(UIApplication *)application performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
   // HomeScreen *vc;
    
    UINavigationController *navigationController = (UINavigationController*)self.window.rootViewController;
    
    id topViewController = navigationController.topViewController;
    if ([topViewController isKindOfClass:[HomeScreen class]])
    {
        [(HomeScreen *)topViewController alarmSchedulerWithCompletionHandler:completionHandler];
    }
    else
    {
        NSLog(@"Not the right class %@.", [topViewController class]);
        completionHandler(UIBackgroundFetchResultFailed);
    }
}
-(void)getdonate
{
    // [SVProgressHUD show];
    NSString *registerURL = [NSString stringWithFormat:@"%@setting_web.php",kAPIURL];
    client_Donation = [[HTTPClient alloc] init];
    client_Donation.delegate = self;
    [client_Donation getResponseFromAPI:registerURL andParameters:nil];
}

#pragma mark - GetApiData
-(void)GetApiData
{
   // [SVProgressHUD show];
    NSString *registerURL = [NSString stringWithFormat:@"%@get_salat_data_web.php",kAPIURL];
    ClientGetList = [[HTTPClient alloc] init];
    ClientGetList.delegate = self;
    [ClientGetList getResponseFromAPI:registerURL andParameters:nil];
}
#pragma mark -
#pragma mark - HTTPClient Delegate methods
- (void)httpClient:(HTTPClient *)client didUpdateWithResponse:(id)response
{
    //NSLog(@"Response home screen: %@",response);
    
    [SVProgressHUD dismiss];
    if (client== ClientGetList)
    {
        
        NSMutableArray *arry_data=[[NSMutableArray alloc] init];
        arry_data=response;
        [[NSUserDefaults standardUserDefaults] setObject:arry_data forKey:@"PrayerTime"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        //[self getdonate];
        
        [[NSNotificationCenter defaultCenter]postNotificationName:@"tablereload" object:nil];
        
        
    }
    else if (client== ClientNoti)
    {
        NSString *code=[response objectForKey:@"code"];
        if ([code isEqualToString:@"200"])
        {
            
        }
    }
    else if (client==client_Donation)
    {
        NSLog(@"%@",response);
        
        NSString *urgetntAnnouncement=[[response valueForKey:@"setting"] valueForKey:@"urgent_announcements"];
        NSString *str_donationpagelink=[[response valueForKey:@"setting"] valueForKey:@"donation_page_link"];
        
         NSString *str_Youtubelink=[[response valueForKey:@"setting"] valueForKey:@"youtube_id_libary"];
        
        [[NSUserDefaults standardUserDefaults] setObject:response forKey:@"Address"];
        [[NSUserDefaults standardUserDefaults] synchronize];

        
        [[NSUserDefaults standardUserDefaults] setObject:str_donationpagelink forKey:@"donation"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSUserDefaults standardUserDefaults] setObject:str_Youtubelink forKey:@"Youtubelink"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSUserDefaults standardUserDefaults] setObject:urgetntAnnouncement forKey:@"Urgent"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        NSString *str_link=[[NSUserDefaults standardUserDefaults] objectForKey:@"donation"];
        
       // [[NSNotificationCenter defaultCenter]postNotificationName:@"setlbl" object:nil];
        
        NSLog(@"%@",str_link);
    }
}

- (void)httpClient:(HTTPClient *)client didFailedWithError:(NSError *)error
{
  //  [SVProgressHUD dismiss];
//    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:error.localizedDescription delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//    [alert show];
}


@end
