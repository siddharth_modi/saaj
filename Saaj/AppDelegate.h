//
//  AppDelegate.h
//  KSIJ Mumbai
//
//  Created by Kishan on 20/10/16.
//  Copyright © 2016 Latitudetechnolabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeScreen.h"
#include <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    AVAudioPlayer *audioPlayer;
    HTTPClient *ClientNotiList;
    HTTPClient *ClientGetList,*ClientNoti,*client_Donation;
}
@property (strong, nonatomic) UIWindow *window;
@property (strong , nonatomic) UINavigationController *Navig;
+(AppDelegate *)sharedAppDelegate;
- (void)redirectConsoleLogToDocumentFolder;
@end

