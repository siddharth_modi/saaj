//
//  ContactListCell.m
//  KSIJ Mumbai
//
//  Created by Kishan on 25/10/16.
//  Copyright © 2016 Latitudetechnolabs. All rights reserved.
//

#import "ContactListCell.h"

@implementation ContactListCell

@synthesize lblPhone1Title,lblPhone2title,lblPhone3Title;
@synthesize lbladdress,btnFax,btnWeb,btnMail,btnphone1,btnPhone2;
@synthesize p1,p2,p3;

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end
