//
//  HomeCell1.h
//  KSIJ Mumbai
//
//  Created by Kishan on 26/01/17.
//  Copyright © 2017 Latitudetechnolabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeCell1 : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *imgWatch;
@property (strong, nonatomic) IBOutlet UILabel *lblPrayerName;
@property (strong, nonatomic) IBOutlet UILabel *lblPrayerTime;
@property (strong, nonatomic) IBOutlet UISegmentedControl *Switch;

@end
