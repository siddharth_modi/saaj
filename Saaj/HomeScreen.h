//
//  HomeScreen.h
//  KSIJ Mumbai
//
//  Created by Kishan on 20/10/16.
//  Copyright © 2016 Latitudetechnolabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RKTabView.h>
#import "DonationScreen.h"
#import "ScheduleScreen.h"
#import "ProjectScreen.h"
#import "VideosScreen.h"
#import "ContactScreen.h"
#import "NotificationScreen.h"

#import "Constants.h"
#import "AFNetworking.h"
#import "HTTPClient.h"
#import "SVProgressHUD.h"
#import <UIImageView+WebCache.h>
#import "HomeCell.h"

@interface HomeScreen : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    
}


@property (strong, nonatomic) IBOutlet UIScrollView *MainScroll;
@property (strong, nonatomic) IBOutlet UIView *MainView;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;

@property (strong, nonatomic) IBOutlet UITableView *tblListAlarm;


@property (strong, nonatomic) IBOutlet UIView *ViewHeader;
@property (strong, nonatomic) IBOutlet UILabel *lbldayname;
@property (strong, nonatomic) IBOutlet UILabel *lblyearname1;
@property (strong, nonatomic) IBOutlet UILabel *lblYearName2;


-(void)alarmSchedulerWithCompletionHandler:(void (^)(UIBackgroundFetchResult)) completionHandler;

- (IBAction)btnNoti:(id)sender;



@end
