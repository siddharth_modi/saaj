//
//  ScheduleSubScreen.m
//  KSIJ Mumbai
//
//  Created by Kishan on 27/10/16.
//  Copyright © 2016 Latitudetechnolabs. All rights reserved.
//

#import "ScheduleSubScreen.h"
#import "MainHomeViewController.h"
#import "NewWeeklyViewController.h"
@interface ScheduleSubScreen ()<RKTabViewDelegate>
{
    RKTabItem *FirstTabItem,*SecondTabItem,*ThirdTabItem,*FourTabItem,*FiveTabItem,*SixTabItem,*HomeTabItem;
}
@end

@implementation ScheduleSubScreen
@synthesize viewEvent;
@synthesize ArrEvt,Tblevent;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    HomeTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"home"] imageDisabled:[UIImage imageNamed:@"home"]];
    HomeTabItem.titleString=@"Home";
    HomeTabItem.titleFontColor=[UIColor whiteColor];
    HomeTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];
    
    FirstTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabprayIconY"] imageDisabled:[UIImage imageNamed:@"TabprayIconW"]];
    FirstTabItem.titleString=@"Prayer Times";
    FirstTabItem.titleFontColor=[UIColor whiteColor];
    FirstTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];
    
    ThirdTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcalendarIconY"] imageDisabled:[UIImage imageNamed:@"TabcalendarIconW"]];
    ThirdTabItem.titleString=@"Calendar";
    ThirdTabItem.titleFontColor=[UIColor whiteColor];
    ThirdTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];
    
    FiveTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabvideoIconY"] imageDisabled:[UIImage imageNamed:@"TabvideoIconW"]];
    FiveTabItem.titleString=@"Media";
    FiveTabItem.titleFontColor=[UIColor whiteColor];
    FiveTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];
    
    SixTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcontactBookIconY"] imageDisabled:[UIImage imageNamed:@"TabcontactBookIconW"]];
    SixTabItem.titleString=@"Contact";
    SixTabItem.titleFontColor=[UIColor whiteColor];
    SixTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];

    
      //_lblTitle.text=[[ArrEvt objectAtIndex:0] valueForKey:@"EventLocation"];
    NSLog(@"%@",_str_Name);
    if (ArrEvt.count!=0)
    {
        [viewEvent setHidden:YES];
        
        NSLog(@"%@",[[ArrEvt objectAtIndex:0]valueForKey:@"EventLocation"]);
        
        if ([[[ArrEvt objectAtIndex:0]valueForKey:@"EventLocation"] isEqualToString:@""])
        {
            _lblTitle.text=@"Others";
        }
        else
        {
            _lblTitle.text=_str_Name;
        }
    }
    else
    {
        _lblTitle.text=_str_Name;
    }
    self.Tblevent.tableFooterView = [[UIView alloc] init];
    self.Tblevent.backgroundColor=[UIColor clearColor];
    
    self.Tblevent.rowHeight =UITableViewAutomaticDimension;
    self.Tblevent.estimatedRowHeight = 75;
    
}
-(void)viewDidAppear:(BOOL)animated
{
    RKTabView *tabView = [[RKTabView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-60,self.view.bounds.size.width, 60)];
    tabView.delegate = self;
    tabView.horizontalInsets = HorizontalEdgeInsetsMake(0, 0);;
    
    tabView.backgroundColor=[UIColor colorWithRed:(48.0f/255.0f) green:(82.0f/255.0f) blue:(1.0f/255.0f) alpha:1];
    
    tabView.tabItems =@[ HomeTabItem,FirstTabItem, ThirdTabItem, FiveTabItem,SixTabItem];
    
    [self.view addSubview:tabView];
    
    viewEvent.layer.borderWidth = 1;
    viewEvent.layer.borderColor = [UIColor lightGrayColor].CGColor;
}

#pragma mark -
#pragma mark - TabBar Delegate methods

- (void)tabView:(RKTabView *)tabView tabBecameEnabledAtIndex:(NSUInteger)index tab:(RKTabItem *)tabItem
{
    NSLog(@"Tab %tu enabled", index);
    
    if (index==0)
    {
        MainHomeViewController *vc=[[MainHomeViewController alloc]initWithNibName:@"MainHomeViewController" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    if (index==1)
    {
        HomeScreen *vc=[[HomeScreen alloc]initWithNibName:@"HomeScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    else if (index==2)
    {
        NewWeeklyViewController *vc=[[NewWeeklyViewController alloc]initWithNibName:@"NewWeeklyViewController" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
        
        
    }
    else if (index==3)
    {
        VideosScreen *vc=[[VideosScreen alloc]initWithNibName:@"VideosScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    else if (index==4)
    {
        ContactScreen *vc=[[ContactScreen alloc]initWithNibName:@"ContactScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
        
    }}
- (void)tabView:(RKTabView *)tabView tabBecameDisabledAtIndex:(NSUInteger)index tab:(RKTabItem *)tabItem
{
    NSLog(@"Tab %tu disabled", index);
}

#pragma mark - Button Notification
- (IBAction)btnNoti:(id)sender
{
    NotificationScreen *vc=[[NotificationScreen alloc]initWithNibName:@"NotificationScreen" bundle:nil];
    [self.navigationController pushViewController:vc animated:NO];
}

#pragma mark -
#pragma mark - Tableview Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return ArrEvt.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"StatusCellIdentifier";
    ScheduleSubCell *cell = (ScheduleSubCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ScheduleSubCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
//    cell.layer.borderWidth = 1;
//    cell.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    NSString *str1=[[ArrEvt objectAtIndex:indexPath.row] objectForKey:@"EventDateChange"];
    NSString *str2=[[ArrEvt objectAtIndex:indexPath.row] objectForKey:@"EventStartChange"];
    cell.lblDateTime.text =[NSString stringWithFormat:@"%@ %@",str1,str2];
    
    NSString *str_EventTitle=[[ArrEvt objectAtIndex:indexPath.row] objectForKey:@"EventTitle"];
    NSString *str2_EventDetail=[[ArrEvt objectAtIndex:indexPath.row] objectForKey:@"EventDetails"];
    
    if ([str2_EventDetail  isEqualToString:@""])
    {
        cell.lblEventDetail.text=str_EventTitle;
        
    }
    else
    {
        cell.lblEventDetail.text=[NSString stringWithFormat:@"%@ , %@",str_EventTitle,str2_EventDetail];

    }
    
    NSString *strColor=[[ArrEvt objectAtIndex:indexPath.row] objectForKey:@"ColorCode"];
    if ([strColor isEqualToString:@"G"])
    {
        cell.lblEventDetail.textColor=[UIColor colorWithRed:0.19 green:0.40 blue:0.04 alpha:1];
    }
    else if ([strColor isEqualToString:@"R"])
    {
        cell.lblEventDetail.textColor=[UIColor colorWithRed:0.93 green:0.17 blue:0.05 alpha:1];
    }
    else
    {
        cell.lblEventDetail.textColor=[UIColor blackColor];
    }
    [cell.lblEventDetail sizeToFit];
    
    return cell;
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CalenderScreen *vc=[[CalenderScreen alloc]initWithNibName:@"CalenderScreen" bundle:nil];
    //vc.ArrEvent=[@[[ArrEvt objectAtIndex:indexPath.row]] mutableCopy];
    vc.PasSelectedDate=[[ArrEvt objectAtIndex:indexPath.row] objectForKey:@"EventDate"];
    vc.ArrEvent=ArrEvt;
    [self.navigationController pushViewController:vc animated:NO];
}

#pragma mark - didReceiveMemoryWarning

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
