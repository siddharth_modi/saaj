//
//  ScheduleSubCell.h
//  KSIJ Mumbai
//
//  Created by Kishan on 10/11/16.
//  Copyright © 2016 Latitudetechnolabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScheduleSubCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblDateTime;
@property (strong, nonatomic) IBOutlet UILabel *lblEventDetail;
@end
