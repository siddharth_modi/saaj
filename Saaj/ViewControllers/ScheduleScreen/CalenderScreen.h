//
//  CalenderScreen.h
//  KSIJ Mumbai
//
//  Created by Kishan on 10/11/16.
//  Copyright © 2016 Latitudetechnolabs. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <RKTabView.h>
#import "HomeScreen.h"
#import "DonationScreen.h"
#import "ScheduleScreen.h"
#import "ProjectScreen.h"
#import "VideosScreen.h"
#import "ContactScreen.h"
#import "NotificationScreen.h"

#import "Constants.h"
#import "AFNetworking.h"
#import "HTTPClient.h"
#import "SVProgressHUD.h"
#import <UIImageView+WebCache.h>

#import "EventCell.h"

#import <JTCalendar/JTCalendar.h>

@interface CalenderScreen : UIViewController<JTCalendarDelegate>
{
    
}
@property(strong,nonatomic)NSString *PasSelectedDate;

@property (strong, nonatomic)NSMutableArray *ArrEvent;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;

@property (strong, nonatomic) IBOutlet UITableView *tblCalenderEvent;
@property (strong, nonatomic) IBOutlet UIView *viewHeader;

@property (weak, nonatomic) IBOutlet JTCalendarMenuView *calendarMenuView;
@property (strong, nonatomic) IBOutlet JTHorizontalCalendarView *viewHeaderMain;
@property (strong, nonatomic) JTCalendarManager *calendarManager;



@property (strong, nonatomic) IBOutlet UIView *viewFooter;



@property (strong, nonatomic) IBOutlet UIView *viewNodata;

- (IBAction)btnNoti:(id)sender;


@end
