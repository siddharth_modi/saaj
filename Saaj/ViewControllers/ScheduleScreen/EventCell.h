//
//  EventCell.h
//  KSIJ Mumbai
//
//  Created by Kishan on 10/11/16.
//  Copyright © 2016 Latitudetechnolabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lbldateTime;
@property (strong, nonatomic) IBOutlet UILabel *lblEventTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblLocationTitle;
@property (strong, nonatomic) IBOutlet UIButton *btnAddCalender;
@property (strong, nonatomic) IBOutlet UILabel *lbl_speaker;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btn_height;

@end
