//
//  ScheduleSubScreen.h
//  KSIJ Mumbai
//
//  Created by Kishan on 27/10/16.
//  Copyright © 2016 Latitudetechnolabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RKTabView.h>
#import "HomeScreen.h"
#import "DonationScreen.h"
#import "ScheduleScreen.h"
#import "ProjectScreen.h"
#import "VideosScreen.h"
#import "ContactScreen.h"
#import "NotificationScreen.h"
#import "CalenderScreen.h"

#import "Constants.h"
#import "AFNetworking.h"
#import "HTTPClient.h"
#import "SVProgressHUD.h"
#import <UIImageView+WebCache.h>

#import "ScheduleSubCell.h"

@interface ScheduleSubScreen : UIViewController
{
    
}
@property (strong, nonatomic)NSMutableArray *ArrEvt;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UITableView *Tblevent;
@property (strong, nonatomic) IBOutlet UIView *viewEvent;
@property(nonatomic,retain)NSString *str_Name;


- (IBAction)btnNoti:(id)sender;

@end
