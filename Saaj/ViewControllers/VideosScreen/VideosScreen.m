//
//  VideosScreen.m
//  KSIJ Mumbai
//
//  Created by Kishan on 21/10/16.
//  Copyright © 2016 Latitudetechnolabs. All rights reserved.
//

#import "VideosScreen.h"
#import "MainHomeViewController.h"
#import "NewWeeklyViewController.h"

@interface VideosScreen ()<RKTabViewDelegate>
{
    RKTabItem *FirstTabItem,*SecondTabItem,*ThirdTabItem,*FourTabItem,*FiveTabItem,*SixTabItem,*HomeTabItem;
    
    
}
@end

@implementation VideosScreen

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _ViewLiveStream.clipsToBounds = YES;
    _ViewLiveStream.layer.cornerRadius =5;
    
    _ViewMediaLibrary.clipsToBounds = YES;
    _ViewMediaLibrary.layer.cornerRadius =5;
    
    HomeTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"home"] imageDisabled:[UIImage imageNamed:@"home"]];
    HomeTabItem.titleString=@"Home";
    HomeTabItem.titleFontColor=[UIColor whiteColor];
    HomeTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];
    
    FirstTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabprayIconY"] imageDisabled:[UIImage imageNamed:@"TabprayIconW"]];
    FirstTabItem.titleString=@"Prayer Times";
    FirstTabItem.titleFontColor=[UIColor whiteColor];
    FirstTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];
    
    ThirdTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcalendarIconY"] imageDisabled:[UIImage imageNamed:@"TabcalendarIconW"]];
    ThirdTabItem.titleString=@"Calendar";
    ThirdTabItem.titleFontColor=[UIColor whiteColor];
    ThirdTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];
    
    FiveTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabvideoIconY"] imageDisabled:[UIImage imageNamed:@"TabvideoIconW"]];
    FiveTabItem.titleString=@"Media";
    FiveTabItem.titleFontColor=[UIColor whiteColor];
    FiveTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];
    
    SixTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcontactBookIconY"] imageDisabled:[UIImage imageNamed:@"TabcontactBookIconW"]];
    SixTabItem.titleString=@"Contact";
    SixTabItem.titleFontColor=[UIColor whiteColor];
    SixTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];

    
    FiveTabItem.tabState = TabStateEnabled;
    
}
-(void)viewDidAppear:(BOOL)animated
{
    RKTabView *tabView = [[RKTabView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-60,self.view.bounds.size.width, 60)];
    tabView.delegate = self;
    tabView.horizontalInsets = HorizontalEdgeInsetsMake(0, 0);;
    
    tabView.backgroundColor=[UIColor colorWithRed:(48.0f/255.0f) green:(82.0f/255.0f) blue:(1.0f/255.0f) alpha:1];
    
    tabView.tabItems =@[HomeTabItem,FirstTabItem,  ThirdTabItem, FiveTabItem,  SixTabItem];
    
    [self.view addSubview:tabView];
}


#pragma mark - TabBar Delegate Method
- (void)tabView:(RKTabView *)tabView tabBecameEnabledAtIndex:(NSUInteger)index tab:(RKTabItem *)tabItem
{
    NSLog(@"Tab %tu enabled", index);
    if (index==0)
    {
        MainHomeViewController *vc=[[MainHomeViewController alloc]initWithNibName:@"MainHomeViewController" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    if (index==1)
    {
        HomeScreen *vc=[[HomeScreen alloc]initWithNibName:@"HomeScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    else if (index==2)
    {
        NewWeeklyViewController *vc=[[NewWeeklyViewController alloc]initWithNibName:@"NewWeeklyViewController" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    else if (index==4)
    {
        ContactScreen *vc=[[ContactScreen alloc]initWithNibName:@"ContactScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
        
    }
}
- (void)tabView:(RKTabView *)tabView tabBecameDisabledAtIndex:(NSUInteger)index tab:(RKTabItem *)tabItem
{
    NSLog(@"Tab %tu disabled", index);
}

#pragma mark - Button Clicked Event
- (IBAction)btnLiveStream:(id)sender
{
    LiveStreamScreen *vc=[[LiveStreamScreen alloc]initWithNibName:@"LiveStreamScreen" bundle:nil];
    [self.navigationController pushViewController:vc animated:NO];
}

- (IBAction)btnMediaLibrary:(id)sender
{
    MedialibraryScreen *vc=[[MedialibraryScreen alloc]initWithNibName:@"MedialibraryScreen" bundle:nil];
    [self.navigationController pushViewController:vc animated:NO];
}

- (IBAction)btnNoti:(id)sender
{
    NotificationScreen *vc=[[NotificationScreen alloc]initWithNibName:@"NotificationScreen" bundle:nil];
    [self.navigationController pushViewController:vc animated:NO];
}
#pragma mark - didReceiveMemoryWarning

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
@end
