//
//  MainHomeViewController.m
//  Saaj
//
//  Created by mac  on 12/27/16.
//  Copyright © 2016 mac . All rights reserved.
//

#import "MainHomeViewController.h"
#import "DonationScreen.h"
#import "SurveyVCViewController.h"
#import "NewCalendarViewController.h"
#import "NewWeeklyViewController.h"
#import "HomeScreen.h"
#import "NewsViewController.h"
#import "RegistrationViewController.h"

@interface MainHomeViewController ()<HTTPClientDeleagte>
{
    HTTPClient *ClientContactList;
    NSMutableArray *alertList;
}

@end

@implementation MainHomeViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    _lbl_alert2.layer.cornerRadius=_lbl_alert2.frame.size.height/2;
    _lbl_alert2.layer.masksToBounds=YES;
    
    [_lbl_alert1 layoutIfNeeded];
    _lbl_alert1.layer.cornerRadius=_lbl_alert1.frame.size.height/2;
    _lbl_alert1.layer.masksToBounds=YES;
    
    _lbl_alert2.hidden=YES;
    _lbl_alert1.hidden=YES;
    
    
    
    
    [self GetContactDeatil];
    
//    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(setlbl_name) name:@"setlbl" object:nil];
//    
//    [[NSNotificationCenter defaultCenter]postNotificationName:@"setlbl" object:nil];
//    UITapGestureRecognizer *tapped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(MenuButtonTapped:)];
//    tapped.delegate = self;
//    tapped.numberOfTapsRequired = 1;
//    _img_Menu_ID.userInteractionEnabled = YES;
//    [_img_Menu_ID addGestureRecognizer:tapped];
}
-(void)setlbl_name
{
   NSString *str_link=[[NSUserDefaults standardUserDefaults] objectForKey:@"Urgent"];
    
    if (![str_link isEqualToString:@""])
    {
        // [_btn_announcement setTitle:[[response valueForKey:@"setting"] valueForKey:@"urgent_announcements"] forState:UIControlStateNormal];
        //_lbl_announcement.text=str_link;
    }
    else
    {
       // [_btn_announcement setTitle:@"" forState:UIControlStateNormal];
       // _lbl_announcement.text=@"";
    }
}
#pragma mark ---------- BUTTON CLICK EVENT ----------
//
//- (IBAction)btn_registration:(id)sender
//{
//    RegistrationViewController *vc=[[RegistrationViewController alloc]initWithNibName:@"RegistrationViewController" bundle:nil];
//    
//    [self.navigationController pushViewController:vc animated:NO];
//}
//
- (void)MenuButtonTapped:(UIButton *)sender
{
    int btnTag = (int)[sender tag];
    switch (btnTag)
    {
        case 101:
        {
            NewsViewController *vc=[[NewsViewController alloc]initWithNibName:@"NewsViewController" bundle:nil];
            vc.str_flag=@"1";
            [self.navigationController pushViewController:vc animated:NO];
            break;
        }
        case 102:
        {
            break;
        }
        case 103:
        {
            break;
        }
        case 104:
        {
            NewWeeklyViewController *vc=[[NewWeeklyViewController alloc]initWithNibName:@"NewWeeklyViewController" bundle:nil];
            [self.navigationController pushViewController:vc animated:NO];
            break;
        }
        case 105:
        {
            HomeScreen *vc=[[HomeScreen alloc]initWithNibName:@"HomeScreen" bundle:nil];
            [self.navigationController pushViewController:vc animated:NO];
            break;
        }
        case 106:
        {
            DonationScreen *vc=[[DonationScreen alloc]initWithNibName:@"DonationScreen" bundle:nil];
            [self.navigationController pushViewController:vc animated:NO];
            
            break;
        }
        case 107:
        {
            SurveyVCViewController *vc=[[SurveyVCViewController alloc]initWithNibName:@"SurveyVCViewController" bundle:nil];
            [self.navigationController pushViewController:vc animated:NO];

            break;
        }
        case 108:
        {
            NewsViewController *vc=[[NewsViewController alloc]initWithNibName:@"NewsViewController" bundle:nil];
            vc.str_flag=@"2";
            [self.navigationController pushViewController:vc animated:NO];
            break;
        }
        case 109:
        {
            break;
        }
        default:
            break;
    }
}

#pragma mark --------------------

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)btn_OnTapped:(id)sender
{
    int btnTag = (int)[sender tag];
    switch (btnTag)
    {
        case 101:
        {
            HomeScreen *vc=[[HomeScreen alloc]initWithNibName:@"HomeScreen" bundle:nil];
            [self.navigationController pushViewController:vc animated:NO];
            break;
        }
        case 102:
        {
            NewWeeklyViewController *vc=[[NewWeeklyViewController alloc]initWithNibName:@"NewWeeklyViewController" bundle:nil];
            [self.navigationController pushViewController:vc animated:NO];
            break;
        }
        case 103:
        {
            NewsViewController *vc=[[NewsViewController alloc]initWithNibName:@"NewsViewController" bundle:nil];
            vc.str_flag=@"1";
            [self.navigationController pushViewController:vc animated:NO];

            break;
        }
        case 104:
        {
            SurveyVCViewController *vc=[[SurveyVCViewController alloc]initWithNibName:@"SurveyVCViewController" bundle:nil];
            vc.str_flag=@"1";
            [self.navigationController pushViewController:vc animated:NO];
            break;
        }
        case 105:
        {
            NSString *str_channelId1=[[NSUserDefaults standardUserDefaults] objectForKey:@"Youtubelink"];
            if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.youtube.com/channel/%@",str_channelId1]]])
            {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.youtube.com/channel/%@",str_channelId1]]];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:@"Invalid url" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
                [alert show];
            }            break;
        }
        case 106:
        {
            ContactScreen *vc=[[ContactScreen alloc]initWithNibName:@"ContactScreen" bundle:nil];
            [self.navigationController pushViewController:vc animated:NO];
            
            break;
        }
        case 107:
        {
            NewsViewController *vc=[[NewsViewController alloc]initWithNibName:@"NewsViewController" bundle:nil];
            vc.str_flag=@"2";
            [self.navigationController pushViewController:vc animated:NO];
            break;
        }
        case 108:
        {
            SurveyVCViewController *vc=[[SurveyVCViewController alloc]initWithNibName:@"SurveyVCViewController" bundle:nil];
            vc.str_flag=@"2";
            [self.navigationController pushViewController:vc animated:NO];
            break;
        }
        case 109:
        {
            break;
        }
        case 110:
        {
            NotificationScreen *vc=[[NotificationScreen alloc]initWithNibName:@"NotificationScreen" bundle:nil];
            [self.navigationController pushViewController:vc animated:NO];

            break;
        }
        default:
            break;
    }

}

#pragma mark - GetContactDeatil
-(void)GetContactDeatil
{
    [SVProgressHUD show];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    
    NSString *registerURL = [NSString stringWithFormat:@"%@get_alert_data_web.php",kAPIURL];
    ClientContactList = [[HTTPClient alloc] init];
    ClientContactList.delegate = self;
    [ClientContactList getResponseFromAPI:registerURL andParameters:nil];
}
- (void)httpClient:(HTTPClient *)client didUpdateWithResponse:(id)response
{
    //NSLog(@"Response Project screen: %@",response);
    
    [SVProgressHUD dismiss];
    if (client== ClientContactList)
    {
        NSLog(@"%@",response);
        
        alertList=[[NSMutableArray alloc] init];
        alertList=[response valueForKey:@"alert_list"];
        
        if (alertList.count!=0)
        {
            NSString *urgetntAnnouncement=[[alertList objectAtIndex:0] valueForKey:@"urgent_announcements"];
            if ([urgetntAnnouncement isEqualToString:@"1"])
            {
                _lbl_announcement.text=[[alertList objectAtIndex:0]valueForKey:@"message_title"];
            }
            else
            {
                [_btn_announcement setTitle:@"" forState:UIControlStateNormal];
                _lbl_announcement.text=@"";

            }
        }
        else
        {
            [_btn_announcement setTitle:@"" forState:UIControlStateNormal];
            _btn_announcement.hidden=YES;
            _lbl_announcement.text=@"";
        }
       
//        if (![urgetntAnnouncement isEqualToString:@""])
//        {
//           // [_btn_announcement setTitle:[[response valueForKey:@"setting"] valueForKey:@"urgent_announcements"] forState:UIControlStateNormal];
//            _lbl_announcement.text=[[response valueForKey:@"setting"] valueForKey:@"urgent_announcements"];
//        }
//        else
//        {
//            [_btn_announcement setTitle:@"" forState:UIControlStateNormal];
//            _lbl_announcement.text=@"";
//        }
    }
}
- (void)httpClient:(HTTPClient *)client didFailedWithError:(NSError *)error
{
    [SVProgressHUD dismiss];
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:error.localizedDescription delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

- (IBAction)btn_announcement:(id)sender
{
    NSMutableArray * arr_UrgentAnnouncement=[[NSMutableArray alloc] init];
    
    for (int i=0; i<alertList.count; i++)
    {
         NSString *urgetntAnnouncement1=[[alertList objectAtIndex:i] valueForKey:@"urgent_announcements"];
        if ([urgetntAnnouncement1 isEqualToString:@"1"])
        {
            [ arr_UrgentAnnouncement addObject:[alertList objectAtIndex:i]];
        }
    }
    NSLog(@"hello %@",arr_UrgentAnnouncement);
    if (arr_UrgentAnnouncement.count==1)
    {
        NotificationSubScreen *vc=[[NotificationSubScreen alloc]initWithNibName:@"NotificationSubScreen" bundle:nil];
        
        NSMutableArray *Temp=[[NSMutableArray alloc]init];
        [Temp addObject:[arr_UrgentAnnouncement objectAtIndex:0]];
        vc.str_flag2=@"YES";
        vc.ArrpassDetail=Temp;
        [self.navigationController pushViewController:vc animated:NO];
    }
    else
    {
        NotificationScreen *vc=[[NotificationScreen alloc]initWithNibName:@"NotificationScreen" bundle:nil];
        vc.arr_urgentAnnouncement=arr_UrgentAnnouncement;
        [self.navigationController pushViewController:vc animated:NO];
    }
}
@end
