//
//  MainHomeViewController.h
//  Saaj
//
//  Created by mac  on 12/27/16.
//  Copyright © 2016 mac . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainHomeViewController : UIViewController<UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *img_Menu_ID;
@property (weak, nonatomic) IBOutlet UILabel *lbl_alert2;
@property (weak, nonatomic) IBOutlet UILabel *lbl_alert1;
- (IBAction)btn_announcement:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lbl_announcement;

@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *test;
- (IBAction)btn_OnTapped:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btn_announcement;

@end
