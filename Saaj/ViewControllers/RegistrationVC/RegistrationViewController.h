//
//  RegistrationViewController.h
//  Saaj
//
//  Created by mac  on 1/5/17.
//  Copyright © 2017 mac . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegistrationViewController : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *txt_firstname;
@property (weak, nonatomic) IBOutlet UITextField *txt_surname;
@property (weak, nonatomic) IBOutlet UITextField *txt_Email;

@property (weak, nonatomic) IBOutlet UITextField *txt_cellphone;
@property (weak, nonatomic) IBOutlet UIButton *btn_Process;
@property (weak, nonatomic) IBOutlet UIButton *btn_Male;
@property (weak, nonatomic) IBOutlet UIButton *btn_Female;
@property (weak, nonatomic) IBOutlet UIView *view_Email;
@property (weak, nonatomic) IBOutlet UIView *view_cellphone;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cellPhone_TopConstraints;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnProcess_TopConstraints;
@property (weak, nonatomic) IBOutlet UITextField *txt_country_Code;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *view_height;

- (IBAction)btn_Process:(id)sender;
- (IBAction)btn_Male:(id)sender;
- (IBAction)btn_Female:(id)sender;

@end
