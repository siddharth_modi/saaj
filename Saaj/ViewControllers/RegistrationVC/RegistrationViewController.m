//
//  RegistrationViewController.m
//  Saaj
//
//  Created by mac  on 1/5/17.
//  Copyright © 2017 mac . All rights reserved.
//

#import "RegistrationViewController.h"
#import "Constants.h"
#import "AFNetworking.h"
#import "HTTPClient.h"
#import "SVProgressHUD.h"
#import "OTPViewController.h"

#define MAX_LENGTH 10
#define MAX_LENGTH2 3
@interface RegistrationViewController ()<HTTPClientDeleagte>
{
    HTTPClient *client_register ,*ClientEmail ,*Client_PhoneNo, *Client_SendOTP;
    NSMutableArray *arr_registerdata;
    NSMutableDictionary *parameter1;
    BOOL radio_btn;
    NSString *str_Gender;
    
    NSString *str_countryCode;
}
@end

@implementation RegistrationViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [_btn_Process layoutIfNeeded];
     [_view_Email layoutIfNeeded];
     [_view_cellphone layoutIfNeeded];
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenHeight = screenRect.size.height;
    
    
    NSLog(@"%f",screenHeight);
    
    NSLog(@"%f",_view_height.constant);
    _view_height.constant=screenHeight;
    str_Gender=@"";
    radio_btn=NO;
    
    
    [_btn_Male setImage:[UIImage imageNamed:@"Radio"] forState:UIControlStateNormal];
    [_btn_Female setImage:[UIImage imageNamed:@"Radio"] forState:UIControlStateNormal];
    
    UIToolbar* keyboardToolbar = [[UIToolbar alloc] init];
    [keyboardToolbar sizeToFit];
    UIBarButtonItem *flexBarButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                      target:nil action:nil];
    UIBarButtonItem *doneBarButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                      target:self.view action:@selector(endEditing:)];
    keyboardToolbar.items = @[flexBarButton, doneBarButton];
    
    self.txt_cellphone.inputAccessoryView = keyboardToolbar;
    self.txt_country_Code.inputAccessoryView=keyboardToolbar;
    
    NSLog(@" height of btn %f",_btn_Process.frame.size.height);
    [_btn_Process layoutIfNeeded];
    _btn_Process.layer.cornerRadius=10 ;
    _btn_Process.layer.masksToBounds=YES;
    
    
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self.view
                                            action:@selector(endEditing:)];
    [self.view addGestureRecognizer:singleFingerTap];
    
    // Do any additional setup after loading the view from its nib.
}
- (NSDictionary *)getCountryCodeDictionary
{
    return [NSDictionary dictionaryWithObjectsAndKeys:@"972", @"IL",
            @"93", @"AF", @"355", @"AL", @"213", @"DZ", @"1", @"AS",
            @"376", @"AD", @"244", @"AO", @"1", @"AI", @"1", @"AG",
            @"54", @"AR", @"374", @"AM", @"297", @"AW", @"61", @"AU",
            @"43", @"AT", @"994", @"AZ", @"1", @"BS", @"973", @"BH",
            @"880", @"BD", @"1", @"BB", @"375", @"BY", @"32", @"BE",
            @"501", @"BZ", @"229", @"BJ", @"1", @"BM", @"975", @"BT",
            @"387", @"BA", @"267", @"BW", @"55", @"BR", @"246", @"IO",
            @"359", @"BG", @"226", @"BF", @"257", @"BI", @"855", @"KH",
            @"237", @"CM", @"1", @"CA", @"238", @"CV", @"345", @"KY",
            @"236", @"CF", @"235", @"TD", @"56", @"CL", @"86", @"CN",
            @"61", @"CX", @"57", @"CO", @"269", @"KM", @"242", @"CG",
            @"682", @"CK", @"506", @"CR", @"385", @"HR", @"53", @"CU",
            @"537", @"CY", @"420", @"CZ", @"45", @"DK", @"253", @"DJ",
            @"1", @"DM", @"1", @"DO", @"593", @"EC", @"20", @"EG",
            @"503", @"SV", @"240", @"GQ", @"291", @"ER", @"372", @"EE",
            @"251", @"ET", @"298", @"FO", @"679", @"FJ", @"358", @"FI",
            @"33", @"FR", @"594", @"GF", @"689", @"PF", @"241", @"GA",
            @"220", @"GM", @"995", @"GE", @"49", @"DE", @"233", @"GH",
            @"350", @"GI", @"30", @"GR", @"299", @"GL", @"1", @"GD",
            @"590", @"GP", @"1", @"GU", @"502", @"GT", @"224", @"GN",
            @"245", @"GW", @"595", @"GY", @"509", @"HT", @"504", @"HN",
            @"36", @"HU", @"354", @"IS", @"91", @"IN", @"62", @"ID",
            @"964", @"IQ", @"353", @"IE", @"972", @"IL", @"39", @"IT",
            @"1", @"JM", @"81", @"JP", @"962", @"JO", @"77", @"KZ",
            @"254", @"KE", @"686", @"KI", @"965", @"KW", @"996", @"KG",
            @"371", @"LV", @"961", @"LB", @"266", @"LS", @"231", @"LR",
            @"423", @"LI", @"370", @"LT", @"352", @"LU", @"261", @"MG",
            @"265", @"MW", @"60", @"MY", @"960", @"MV", @"223", @"ML",
            @"356", @"MT", @"692", @"MH", @"596", @"MQ", @"222", @"MR",
            @"230", @"MU", @"262", @"YT", @"52", @"MX", @"377", @"MC",
            @"976", @"MN", @"382", @"ME", @"1", @"MS", @"212", @"MA",
            @"95", @"MM", @"264", @"NA", @"674", @"NR", @"977", @"NP",
            @"31", @"NL", @"599", @"AN", @"687", @"NC", @"64", @"NZ",
            @"505", @"NI", @"227", @"NE", @"234", @"NG", @"683", @"NU",
            @"672", @"NF", @"1", @"MP", @"47", @"NO", @"968", @"OM",
            @"92", @"PK", @"680", @"PW", @"507", @"PA", @"675", @"PG",
            @"595", @"PY", @"51", @"PE", @"63", @"PH", @"48", @"PL",
            @"351", @"PT", @"1", @"PR", @"974", @"QA", @"40", @"RO",
            @"250", @"RW", @"685", @"WS", @"378", @"SM", @"966", @"SA",
            @"221", @"SN", @"381", @"RS", @"248", @"SC", @"232", @"SL",
            @"65", @"SG", @"421", @"SK", @"386", @"SI", @"677", @"SB",
            @"27", @"ZA", @"500", @"GS", @"34", @"ES", @"94", @"LK",
            @"249", @"SD", @"597", @"SR", @"268", @"SZ", @"46", @"SE",
            @"41", @"CH", @"992", @"TJ", @"66", @"TH", @"228", @"TG",
            @"690", @"TK", @"676", @"TO", @"1", @"TT", @"216", @"TN",
            @"90", @"TR", @"993", @"TM", @"1", @"TC", @"688", @"TV",
            @"256", @"UG", @"380", @"UA", @"971", @"AE", @"44", @"GB",
            @"1", @"US", @"598", @"UY", @"998", @"UZ", @"678", @"VU",
            @"681", @"WF", @"967", @"YE", @"260", @"ZM", @"263", @"ZW",
            @"591", @"BO", @"673", @"BN", @"61", @"CC", @"243", @"CD",
            @"225", @"CI", @"500", @"FK", @"44", @"GG", @"379", @"VA",
            @"852", @"HK", @"98", @"IR", @"44", @"IM", @"44", @"JE",
            @"850", @"KP", @"82", @"KR", @"856", @"LA", @"218", @"LY",
            @"853", @"MO", @"389", @"MK", @"691", @"FM", @"373", @"MD",
            @"258", @"MZ", @"970", @"PS", @"872", @"PN", @"262", @"RE",
            @"7", @"RU", @"590", @"BL", @"290", @"SH", @"1", @"KN",
            @"1", @"LC", @"590", @"MF", @"508", @"PM", @"1", @"VC",
            @"239", @"ST", @"252", @"SO", @"47", @"SJ", @"963", @"SY",
            @"886", @"TW", @"255", @"TZ", @"670", @"TL", @"58", @"VE",
            @"84", @"VN", @"1", @"VG", @"1", @"VI", nil];
}


-(void)viewWillAppear:(BOOL)animated
{
    NSLocale *currentLocale = [NSLocale currentLocale];  // get the current locale.
   str_countryCode = [currentLocale objectForKey:NSLocaleCountryCode];
    NSLog(@"%@",[NSString stringWithFormat:@"+%@",[[self getCountryCodeDictionary] objectForKey:str_countryCode]]);
    str_countryCode=[NSString stringWithFormat:@"%@",[[self getCountryCodeDictionary] objectForKey:str_countryCode]];

    NSLog(@"%@",str_countryCode);
    
    UILabel *plusLabel = [[UILabel alloc] init];
    [plusLabel setFont:[UIFont systemFontOfSize:14 weight:0]];
    plusLabel.text = [NSString stringWithFormat:@" +"];
  //  plusLabel.backgroundColor=[UIColor redColor];
    [plusLabel sizeToFit];
    self.txt_country_Code.leftView = plusLabel;
    self.txt_country_Code.leftViewMode = UITextFieldViewModeAlways;
    _txt_country_Code.text=str_countryCode;
    
    
//    if ([str_countryCode isEqualToString:@"KE"] ||[str_countryCode isEqualToString:@"KEN"])
//    {
//        _view_Email.hidden=YES;
//        _cellPhone_TopConstraints.constant=-_view_Email.frame.size.height;
//    }
//    else
//    {
//        _view_cellphone.hidden=YES;
//        _btnProcess_TopConstraints.constant=-(_view_cellphone.frame.size.height-15)/2;
//    }

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Textfield Deledate Method
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
//    if (textField==_txt_firstname)
//    {
//        [_txt_surname becomeFirstResponder];
//    }
//    else if (textField==_txt_surname)
//    {
//      [textField resignFirstResponder];
//    }
//    else if (textField==_txt_Email)
//    {
//        [_txt_cellphone becomeFirstResponder];
//    }
//    else
//    {
        [textField resignFirstResponder];
   // }
    
    return YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
//    if (textField==_txt_cellphone)
//    {
//        if (textField.text.length >= MAX_LENGTH && range.length == 0)
//        {
//            return NO; // return NO to not change text
//        }
//        else
//        {
//            return YES;
//        }
//    }
    if (textField==_txt_country_Code)
    {
        if (textField.text.length >= MAX_LENGTH2 && range.length == 0)
        {
            return NO; // return NO to not change text
        }
        else
        {
            return YES;
        }
    }

    return YES;
}

#pragma mark -Button Process Method

- (IBAction)btn_Process:(id)sender
{

        if ([_txt_firstname.text isEqualToString:@""])
        {
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Saaj" message:@"Please Enter First Name." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
        else if ([_txt_surname.text isEqualToString:@""])
        {
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Saaj" message:@"Please Enter Last Name." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
        else if ([str_Gender isEqualToString:@""])
        {
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Saaj" message:@"Please Select Gender." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
        else if ([_txt_Email.text isEqualToString:@""])
        {
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Saaj" message:@"Please Enter Email." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
        else if ([self NSStringIsValidEmail:_txt_Email.text]==NO)
        {
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Saaj" message:@"Invalid Email." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
        else if ([_txt_country_Code.text isEqualToString:@""])
        {
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Saaj" message:@"Please Enter Country Code" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
        else if ([_txt_cellphone.text isEqualToString:@""])
        {
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Saaj" message:@"Please Enter Cellphone Number." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
        else if (_txt_cellphone.text.length<9)
        {
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Saaj" message:@"Please Enter at least 9 Digit Mobile Number." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
        else
        {
            NSLog(@"Register");
            if (_txt_cellphone.text.length!=0)
            {
                NSString *someText = _txt_cellphone.text;
                
                NSString * firstLetter1 = [someText substringWithRange:[someText rangeOfComposedCharacterSequenceAtIndex:0]];
                // NSString *firstLetter = [someText substringFromIndex:1];
                if ([firstLetter1 isEqualToString:@"0"])
                {
                    NSRange range = NSMakeRange(0,1);
                    NSString *newText = [someText stringByReplacingCharactersInRange:range withString:@""];
                    NSLog(@"%@",newText);
                    _txt_cellphone.text=newText;
                }
            }
            [self SendOTP];
//            if ([str_countryCode isEqualToString:@"KE"] ||[str_countryCode isEqualToString:@"KEN"])
//            {
//                  [self CheckExistPhoneNo];
//            }
//            else
//            {
//                [self CheckExistEmail:_txt_Email.text];
//            }
        }
}

#pragma mark - Check Email-Id Valid or Not

-(BOOL)NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

#pragma mark -Button Select Male

- (IBAction)btn_Male:(id)sender
{
    [_btn_Male setImage:[UIImage imageNamed:@"Radio_Fill"] forState:UIControlStateNormal];
    [_btn_Female setImage:[UIImage imageNamed:@"Radio"] forState:UIControlStateNormal];
    str_Gender=@"1";
   
}

#pragma mark -Button Select Female

- (IBAction)btn_Female:(id)sender
{
    [_btn_Male setImage:[UIImage imageNamed:@"Radio"] forState:UIControlStateNormal];
    [_btn_Female setImage:[UIImage imageNamed:@"Radio_Fill"] forState:UIControlStateNormal];
     str_Gender=@"2";
}

#pragma mark - GetEventMonthlyList
-(void)SetRegistrationInfo
{
    [SVProgressHUD show];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    
    parameter1=[[NSMutableDictionary alloc] init];
    
      [parameter1 setValue:_txt_firstname.text forKey:@"first_name"];
      [parameter1 setValue:_txt_surname.text forKey:@"surname"];
      [parameter1 setValue:str_Gender forKey:@"gender"];
      [parameter1 setValue:_txt_Email.text forKey:@"email"];
      [parameter1 setValue:_txt_cellphone.text forKey:@"mobile_no"];
    
    NSLog(@"%@ param ",parameter1);
    
    NSString *registerURL = [NSString stringWithFormat:@"http://islamiccenterapp.com/saaj/webservices/register.php"];
    client_register = [[HTTPClient alloc] init];
    client_register.delegate = self;
    [client_register getResponseFromAPI:registerURL andParameters:parameter1];
}

#pragma mark -
#pragma mark - HTTPClient Delegate methods
- (void)httpClient:(HTTPClient *)client didUpdateWithResponse:(id)response
{
    //NSLog(@"Response home screen: %@",response);
    
   
    if (client == client_register)
    {
        
        NSLog(@" Response is == %@",[response valueForKey:@"status"]);
        arr_registerdata=[[NSMutableArray alloc] init];
        
        NSString *str_statuscode=[NSString stringWithFormat:@"%@",[response valueForKey:@"status"]];
        
        if ([str_statuscode isEqualToString:@"200"])
        {
            
            
        }
        else
        {
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Saaj" message:[response valueForKey:@"data"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
    else if (client==ClientEmail)
    {
        NSLog(@" Response is == %@",[response valueForKey:@"status"]);
        
        NSString *str_statuscode=[NSString stringWithFormat:@"%@",[response valueForKey:@"status"]];
        
        if ([str_statuscode isEqualToString:@"200"])
        {
          
             [self SendOTP];
        }
        else
        {
             [SVProgressHUD dismiss];
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Saaj" message:[response valueForKey:@"data"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
    else if (client==Client_PhoneNo)
    {
        NSLog(@" Response is == %@",[response valueForKey:@"status"]);
        
        NSString *str_statuscode=[NSString stringWithFormat:@"%@",[response valueForKey:@"status"]];
        
        if ([str_statuscode isEqualToString:@"200"])
        {
            [self SendOTP];
            
        }
        else
        {
             [SVProgressHUD dismiss];
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Saaj" message:[response valueForKey:@"data"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
    else if (client==Client_SendOTP)
    {
        NSLog(@" Response is == %@",[response valueForKey:@"status"]);
         [SVProgressHUD dismiss];
        NSString *str_statuscode=[NSString stringWithFormat:@"%@",[response valueForKey:@"status"]];
        
        if ([str_statuscode isEqualToString:@"200"])
        {
            parameter1=[[NSMutableDictionary alloc] init];
            
            NSString *str_deviceID=[[NSUserDefaults standardUserDefaults] objectForKey:@"DivID"];
           
            [parameter1 setValue:_txt_Email.text forKey:@"email"];
            [parameter1 setValue:[NSString stringWithFormat:@"%@%@",_txt_country_Code.text,_txt_cellphone.text] forKey:@"mobile_no"];
            [parameter1 setValue:_txt_firstname.text forKey:@"first_name"];
            [parameter1 setValue:_txt_surname.text forKey:@"surname"];
            [parameter1 setValue:str_Gender forKey:@"gender"];
            [parameter1 setValue:str_deviceID forKey:@"device_id"];
            [parameter1 setValue:@"IOS" forKey:@"device"];
           
            OTPViewController *vc=[[OTPViewController alloc] initWithNibName:@"OTPViewController" bundle:nil];
            vc.dic_registerData=parameter1;
            vc.str_CountryFlag=str_countryCode;
            [self.navigationController pushViewController:vc animated:NO];
        }
        else
        {
             [SVProgressHUD dismiss];
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Saaj" message:[response valueForKey:@"data"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
    }

}
- (void)httpClient:(HTTPClient *)client didFailedWithError:(NSError *)error
{
    [SVProgressHUD dismiss];
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:error.localizedDescription delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

#pragma mark - CheckExistEmail API
-(void)CheckExistEmail:(NSString *)Email
{
    [SVProgressHUD show];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    
    NSMutableDictionary *parameter=[[NSMutableDictionary alloc] init];
    
   [parameter setValue:Email forKey:@"email"];
    
    NSString *registerURL = [NSString stringWithFormat:@"%@email_exists.php",kAPIURL];
    ClientEmail = [[HTTPClient alloc] init];
    ClientEmail.delegate = self;
    [ClientEmail getResponseFromAPI:registerURL andParameters:parameter];
}
#pragma mark - CheckExistEmail API
-(void)SendOTP
{
    [SVProgressHUD show];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    
    NSMutableDictionary *parameter=[[NSMutableDictionary alloc] init];
    
    NSString *str_phoneno=[NSString stringWithFormat:@"%@%@",_txt_country_Code.text,_txt_cellphone.text];
    [parameter setValue:str_phoneno forKey:@"mobile_no"];
    [parameter setValue:_txt_firstname.text forKey:@"first_name"];
//    if ([str_countryCode isEqualToString:@"KE"]||[str_countryCode isEqualToString:@"KEN"])
//    {
//           }
//    else
//    {
//        //[parameter setValue:_txt_Email.text forKey:@"email"];
//    }
    NSString *registerURL = [NSString stringWithFormat:@"%@send_otp.php",kAPIURL];
    Client_SendOTP = [[HTTPClient alloc] init];
    Client_SendOTP.delegate = self;
    [Client_SendOTP getResponseFromAPI:registerURL andParameters:parameter];
}

#pragma mark - CheckExistPhoneNo API

-(void)CheckExistPhoneNo
{
//    [SVProgressHUD show];
//    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    NSMutableDictionary *parameter=[[NSMutableDictionary alloc] init];
    
    [parameter setValue:_txt_cellphone.text forKey:@"mobile_no"];
    
    NSString *registerURL = [NSString stringWithFormat:@"%@mobile_exists.php",kAPIURL];
    Client_PhoneNo = [[HTTPClient alloc] init];
    Client_PhoneNo.delegate = self;
    [Client_PhoneNo getResponseFromAPI:registerURL andParameters:parameter];
}
@end
