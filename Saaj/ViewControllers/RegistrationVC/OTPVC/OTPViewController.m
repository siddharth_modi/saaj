//
//  OTPViewController.m
//  Saaj
//
//  Created by mac  on 1/9/17.
//  Copyright © 2017 mac . All rights reserved.
//

#import "OTPViewController.h"
#import "Constants.h"
#import "AFNetworking.h"
#import "HTTPClient.h"
#import "SVProgressHUD.h"
#import "MainHomeViewController.h"
@interface OTPViewController ()<HTTPClientDeleagte>
{
    HTTPClient *client_register ,*Client_VarifyOTP ,*Client_Pushnotification;
}
@end

@implementation OTPViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [_btn_VerifyOTP layoutIfNeeded];
    _btn_VerifyOTP.layer.cornerRadius=10;
    _btn_VerifyOTP.layer.masksToBounds=YES;
    
    _lbl_header.text=@"Please enter the verification code you received by SMS.";
    
//    if ([_str_CountryFlag isEqualToString:@"KE"] ||[_str_CountryFlag isEqualToString:@"KEN"])
//    {
//        _lbl_header.text=@"Please enter the verification code you received by SMS.";
//    }
//    else
//    {
//        _lbl_header.text=@"Please enter the verification code you received by Email.";
//    }
    
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self.view
                                            action:@selector(endEditing:)];
    [self.view addGestureRecognizer:singleFingerTap];

    // Do any additional setup after loading the view from its nib.
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - GetEventMonthlyList

-(void)SetRegistrationInfo
{
    NSLog(@"%@ param ",_dic_registerData);
    
    NSString *registerURL = [NSString stringWithFormat:@"%@register.php",kAPIURL];
    client_register = [[HTTPClient alloc] init];
    client_register.delegate = self;
    [client_register getResponseFromAPI:registerURL andParameters:_dic_registerData];
}
#pragma mark - GetEventMonthlyList

-(void)VarifyOTP
{
    [SVProgressHUD show];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
     NSMutableDictionary *parameter=[[NSMutableDictionary alloc] init];
    
    NSString *str_phoneno=[NSString stringWithFormat:@"%@",[_dic_registerData valueForKey:@"mobile_no"]];
    [parameter setValue:str_phoneno forKey:@"mobile_no"];
//    if ([_str_CountryFlag isEqualToString:@"KE"] ||[_str_CountryFlag isEqualToString:@"KEN"])
//    {
//       
//    }
//    else
//    {
//        [parameter setValue:[_dic_registerData valueForKey:@"email"] forKey:@"email"];
//    }
    [parameter setValue:_txt_OTP.text forKey:@"verification_code"];

    NSLog(@"%@ param ",parameter);
    NSString *registerURL = [NSString stringWithFormat:@"%@verify_mob.php",kAPIURL];
    Client_VarifyOTP = [[HTTPClient alloc] init];
    Client_VarifyOTP.delegate = self;
    [Client_VarifyOTP getResponseFromAPI:registerURL andParameters:parameter];
}

#pragma mark - GetEventMonthlyList

-(void)SendDeviceID
{
    [SVProgressHUD show];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    NSMutableDictionary *parameter=[[NSMutableDictionary alloc] init];
    
    NSString *str_phoneno=[NSString stringWithFormat:@"254%@",[_dic_registerData valueForKey:@"mobile_no"]];
    [parameter setValue:str_phoneno forKey:@"regID_ios"];
    [parameter setValue:@"" forKey:@"regID"];
    [parameter setValue:str_phoneno forKey:@"devicePlatform"];
    [parameter setValue:_txt_OTP.text forKey:@"deviceUUID"];
    NSLog(@"%@ param ",parameter);
    NSString *registerURL = [NSString stringWithFormat:@"%@verify_mob.php",kAPIURL];
    Client_VarifyOTP = [[HTTPClient alloc] init];
    Client_VarifyOTP.delegate = self;
    [Client_VarifyOTP getResponseFromAPI:registerURL andParameters:parameter];
}



#pragma mark -
#pragma mark - HTTPClient Delegate methods

- (void)httpClient:(HTTPClient *)client didUpdateWithResponse:(id)response
{
    //NSLog(@"Response home screen: %@",response);
    if (client == client_register)
    {
        NSLog(@" Response is == %@",[response valueForKey:@"status"]);
         [SVProgressHUD dismiss];
        NSString *str_statuscode=[NSString stringWithFormat:@"%@",[response valueForKey:@"status"]];
        
        if ([str_statuscode isEqualToString:@"200"])
        {
            [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:@"RegisterUser"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            MainHomeViewController *vc=[[MainHomeViewController alloc] initWithNibName:@"MainHomeViewController" bundle:nil];
            [self.navigationController pushViewController:vc animated:NO];
        }
        else
        {
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Saaj" message:[response valueForKey:@"data"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
    else if (client==Client_VarifyOTP)
    {
        NSLog(@" Response is == %@",[response valueForKey:@"status"]);
        
        NSString *str_statuscode=[NSString stringWithFormat:@"%@",[response valueForKey:@"status"]];
        
        if ([str_statuscode isEqualToString:@"200"])
        {
            [self SetRegistrationInfo];
        }
        else
        {
             [SVProgressHUD dismiss];
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Saaj" message:[response valueForKey:@"data"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
}
- (void)httpClient:(HTTPClient *)client didFailedWithError:(NSError *)error
{
    [SVProgressHUD dismiss];
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:error.localizedDescription delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btn_VerifyOTP:(id)sender
{
    NSLog(@"Hello OTO Button Tapped");
    [self VarifyOTP];
}
- (IBAction)btn_back:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}
@end
