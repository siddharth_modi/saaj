//
//  OTPViewController.h
//  Saaj
//
//  Created by mac  on 1/9/17.
//  Copyright © 2017 mac . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OTPViewController : UIViewController


@property(nonatomic,retain)NSMutableDictionary *dic_registerData;
@property (weak, nonatomic) IBOutlet UITextField *txt_OTP;
@property (weak, nonatomic) IBOutlet UIButton *btn_VerifyOTP;
@property (nonatomic,retain)NSString *str_CountryFlag;
- (IBAction)btn_VerifyOTP:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lbl_header;
- (IBAction)btn_back:(id)sender;

@end
