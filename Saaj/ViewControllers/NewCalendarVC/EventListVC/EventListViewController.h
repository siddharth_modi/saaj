//
//  EventListViewController.h
//  Saaj
//
//  Created by mac  on 1/3/17.
//  Copyright © 2017 mac . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventListViewController : UIViewController

@property(nonatomic,retain)NSMutableArray *arr_AllEvent;
@property(nonatomic,retain)NSMutableArray *arr_AllEventDate;
@property(nonatomic,retain)NSString *Selecteddate;
@end
