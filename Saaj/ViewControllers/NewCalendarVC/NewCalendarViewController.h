//
//  NewCalendarViewController.h
//  Saaj
//
//  Created by mac  on 12/30/16.
//  Copyright © 2016 mac . All rights reserved.
//

#import <UIKit/UIKit.h>
#import <JTCalendar/JTCalendar.h>

@interface NewCalendarViewController : UIViewController<JTCalendarDelegate,UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UIButton *btn_previous;

@property (weak, nonatomic) IBOutlet JTCalendarMenuView *calendarMenuView;
@property (weak, nonatomic) IBOutlet UIButton *btn_next;
@property (strong, nonatomic) IBOutlet JTHorizontalCalendarView *viewHeaderMain;
@property (strong, nonatomic) JTCalendarManager *calendarManager;
- (IBAction)btn_next:(id)sender;
- (IBAction)btn_previous:(id)sender;

@property (weak, nonatomic) IBOutlet UITableView *tbl_MonthlyEvent;

- (IBAction)btn_monthly:(id)sender;

- (IBAction)btn_Weekly:(id)sender;

- (IBAction)btn_Notification:(id)sender;

@end
