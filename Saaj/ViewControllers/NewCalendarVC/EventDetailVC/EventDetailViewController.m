//
//  EventDetailViewController.m
//  Saaj
//
//  Created by mac  on 1/4/17.
//  Copyright © 2017 mac . All rights reserved.
//

#import "EventDetailViewController.h"
#import <RKTabView.h>
#import "DonationScreen.h"
#import "ScheduleScreen.h"
#import "ProjectScreen.h"
#import "VideosScreen.h"
#import "ContactScreen.h"
#import "NotificationScreen.h"

#import "Constants.h"
#import "AFNetworking.h"
#import "HTTPClient.h"
#import "SVProgressHUD.h"
#import <UIImageView+WebCache.h>
#import "HomeCell.h"
#import "MainHomeViewController.h"
#import "NewCalendarViewController.h"
#import "NotificationScreen.h"
#import "EventCell.h"
#import "NewWeeklyViewController.h"
#import "EventdetailTableViewCell.h"
#import <EventKit/EventKit.h>
#import <EventKitUI/EventKitUI.h>

@interface EventDetailViewController ()<RKTabViewDelegate>
{
    RKTabItem *FirstTabItem,*SecondTabItem,*ThirdTabItem,*FourTabItem,*FiveTabItem,*SixTabItem ,*HomeTabItem;
      EKEventStore *store;
}
@end

@implementation EventDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    NSLog(@"%@",_ArrEvent);

    self.tbl_EventDetail.rowHeight =UITableViewAutomaticDimension;
    self.tbl_EventDetail.estimatedRowHeight = 210;
    
    if ([_str_notiFlag isEqualToString:@"Yes"])
    {
        _btn_Back.hidden=YES;
    }
    else
    {
        _btn_Back.hidden=NO;
    }
    
    HomeTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"home"] imageDisabled:[UIImage imageNamed:@"home"]];
    HomeTabItem.titleString=@"Home";
    HomeTabItem.titleFontColor=[UIColor whiteColor];
    HomeTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];
    
    FirstTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabprayIconY"] imageDisabled:[UIImage imageNamed:@"TabprayIconW"]];
    FirstTabItem.titleString=@"Prayer Times";
    FirstTabItem.titleFontColor=[UIColor whiteColor];
    FirstTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];
    
    ThirdTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcalendarIconY"] imageDisabled:[UIImage imageNamed:@"TabcalendarIconW"]];
    ThirdTabItem.titleString=@"Calendar";
    ThirdTabItem.titleFontColor=[UIColor whiteColor];
    ThirdTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];
    
    FiveTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabvideoIconW"] imageDisabled:[UIImage imageNamed:@"TabvideoIconW"]];
    FiveTabItem.titleString=@"Media";
    FiveTabItem.titleFontColor=[UIColor whiteColor];
    FiveTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];
    
    SixTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcontactBookIconY"] imageDisabled:[UIImage imageNamed:@"TabcontactBookIconW"]];
    SixTabItem.titleString=@"Contact";
    SixTabItem.titleFontColor=[UIColor whiteColor];
    SixTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewDidAppear:(BOOL)animated
{
    RKTabView *tabView = [[RKTabView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-60,self.view.bounds.size.width, 60)];
    tabView.delegate = self;
    tabView.horizontalInsets = HorizontalEdgeInsetsMake(0, 0);
    tabView.backgroundColor=[UIColor colorWithRed:(48.0f/255.0f) green:(82.0f/255.0f) blue:(1.0f/255.0f) alpha:1];
    tabView.tabItems =@[HomeTabItem,FirstTabItem,  ThirdTabItem, FiveTabItem,  SixTabItem];
    
    [self.view addSubview:tabView];
}
#pragma mark - RKTabViewDelegate

- (void)tabView:(RKTabView *)tabView tabBecameEnabledAtIndex:(NSUInteger)index tab:(RKTabItem *)tabItem
{
    NSLog(@"Tab %tu enabled", index);
    
    if (index==0)
    {
        MainHomeViewController *vc=[[MainHomeViewController alloc]initWithNibName:@"MainHomeViewController" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    if (index==1)
    {
        HomeScreen *vc=[[HomeScreen alloc]initWithNibName:@"HomeScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    else if (index==2)
    {
        NewWeeklyViewController *vc=[[NewWeeklyViewController alloc]initWithNibName:@"NewWeeklyViewController" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
        
        
    }
    else if (index==3)
    {
        NSString *str_channelId1=[[NSUserDefaults standardUserDefaults] objectForKey:@"Youtubelink"];
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.youtube.com/channel/%@",str_channelId1]]])
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.youtube.com/channel/%@",str_channelId1]]];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:@"Invalid url" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
            [alert show];
        }
        [self setagain_click];
    }
    else if (index==4)
    {
        ContactScreen *vc=[[ContactScreen alloc]initWithNibName:@"ContactScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
        
    }
}

- (void)tabView:(RKTabView *)tabView tabBecameDisabledAtIndex:(NSUInteger)index tab:(RKTabItem *)tabItem
{
    NSLog(@"Tab %tu disabled", index);
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark -
#pragma mark - Tableview Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _ArrEvent.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"StatusCellIdentifier";
    
    EventdetailTableViewCell *cell = (EventdetailTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"EventdetailTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
   
    
    NSString *str1=[[_ArrEvent objectAtIndex:indexPath.row] objectForKey:@"EventStartChange"];
   // NSString *str2=[[_ArrEvent objectAtIndex:indexPath.row] objectForKey:@"EventEndChange"];
    
    NSString *strColor=[[_ArrEvent objectAtIndex:indexPath.row] objectForKey:@"ColorCode"];
    if ([strColor isEqualToString:@"G"])
    {
        cell.lbl_title.textColor=[UIColor colorWithRed:0.19 green:0.40 blue:0.04 alpha:1];
        cell.lbl_date.textColor=[UIColor colorWithRed:0.19 green:0.40 blue:0.04 alpha:1];
        cell.lbl_Time.textColor=[UIColor colorWithRed:0.19 green:0.40 blue:0.04 alpha:1];
        cell.lbl_aboutofSpeaker.textColor=[UIColor colorWithRed:0.19 green:0.40 blue:0.04 alpha:1];
    }
    else if ([strColor isEqualToString:@"R"])
    {
        cell.lbl_title.textColor=[UIColor colorWithRed:0.93 green:0.17 blue:0.05 alpha:1];
        cell.lbl_date.textColor=[UIColor colorWithRed:0.93 green:0.17 blue:0.05 alpha:1];
        cell.lbl_Time.textColor=[UIColor colorWithRed:0.93 green:0.17 blue:0.05 alpha:1];
        cell.lbl_aboutofSpeaker.textColor=[UIColor colorWithRed:0.93 green:0.17 blue:0.05 alpha:1];
    }
    else
    {
        cell.lbl_title.textColor=[UIColor blackColor];
        cell.lbl_date.textColor=[UIColor blackColor];
        cell.lbl_Time.textColor=[UIColor blackColor];
        cell.lbl_aboutofSpeaker.textColor=[UIColor blackColor];
    }

    
    
    cell.btn_addtocalendar.hidden=NO;
    cell.btn_addtocalendar.tag=indexPath.row;
    
    cell.btn_addtocalendar.layer.cornerRadius=5;
    cell.btn_addtocalendar.layer.masksToBounds=YES;
    [cell.btn_addtocalendar addTarget:self action:@selector(btnAddCalender1:)forControlEvents:UIControlEventTouchUpInside];
    
    
    
    cell.lbl_title.text=[[_ArrEvent objectAtIndex:indexPath.row] objectForKey:@"EventTitle"];
    cell.lbl_date.text=[self setDateFormat:[[_ArrEvent objectAtIndex:indexPath.row] objectForKey:@"EventDate"]];
    cell.lbl_Time.text=[NSString stringWithFormat:@"%@",str1];
    
    NSString *str_speakername=[[_ArrEvent objectAtIndex:indexPath.row] objectForKey:@"Speaker"];
    if ([str_speakername isEqualToString:@""])
    {
        cell.lbl_SpeakerName.hidden=YES;
        
        cell.img_speaker.hidden=YES;
       
    }
     NSString *str_speakername1=[[_ArrEvent objectAtIndex:indexPath.row] objectForKey:@"SpeakerDetails"];
   
     if ([str_speakername1 isEqualToString:@""])
     {
          cell.lbl_SpeakerDetail.hidden=YES;
          cell.lbl_aboutofSpeaker.hidden=YES;
          cell.img_user.hidden=YES;
     }
    cell.lbl_SpeakerDetail.text=[NSString stringWithFormat:@"%@",[[_ArrEvent objectAtIndex:indexPath.row] objectForKey:@"SpeakerDetails"]];
    cell.lbl_EventDetail.text=[NSString stringWithFormat:@"%@",[[_ArrEvent objectAtIndex:indexPath.row] objectForKey:@"EventDetails"]];
    
    NSString *str_location=[[_ArrEvent objectAtIndex:indexPath.row] objectForKey:@"EventLocation"];
    
    if ([str_location  isEqualToString:@""])
    {
        cell.img_location.hidden=YES;
        cell.lbl_EventDetailTopConstraints.constant=-cell.lbl_EventDetailTopConstraints.constant;
    }
    
    cell.lbl_Location.text=str_location;
    
    NSRange range = [[NSString stringWithFormat:@"Speaker Name: %@",[[_ArrEvent objectAtIndex:indexPath.row] objectForKey:@"Speaker"]] rangeOfString:@"Speaker Name:"];
    
    if (range.location == NSNotFound)
    {
        NSLog(@"The string (testString) does not contain 'how are you doing' as a substring");
    }
    else
    {
        NSLog(@"Found the range of the substring at (%lu, %lu)", (unsigned long)range.location, range.location + range.length);
        //Create mutable string from original one
        NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Speaker Name : %@",[[_ArrEvent objectAtIndex:indexPath.row] objectForKey:@"Speaker"]]];
        if ([strColor isEqualToString:@"G"])
        {
           
            [attString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:0.19 green:0.40 blue:0.04 alpha:1] range:range];
        }
        else if ([strColor isEqualToString:@"R"])
        {
            [attString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:0.93 green:0.17 blue:0.05 alpha:1] range:range];
        }
        else
        {
            [attString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:range];
        }

        
      //  [attString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:(48/255.0) green:(82/255.0) blue:(1/255.0) alpha:1.0] range:range];
        
        //Add it to the label - notice its not text property but it's attributeText
        cell.lbl_SpeakerName.attributedText = attString;
    }
    return cell;
}
-(NSString *)getTimeStamp
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd hh:mm:ss";
    NSTimeZone *gmt = [NSTimeZone systemTimeZone];
    [dateFormatter setTimeZone:gmt];
    NSString *timeStamp = [dateFormatter stringFromDate:[NSDate date]];
    
    return timeStamp;
}
- (void)btnAddCalender1:(UIButton *)sender
{
    NSString *Timestamp=[NSString stringWithFormat:@"%@",[self getTimeStamp]];
    
    NSString *datestr=[[_ArrEvent objectAtIndex:sender.tag] objectForKey:@"EventDate"];
    NSString *timestr=[[_ArrEvent objectAtIndex:sender.tag] objectForKey:@"EventStartChange"];
    
    NSString *finalstr=[NSString stringWithFormat:@"%@ %@",datestr,timestr];
    
    
    NSString *apidate=[self TimeFormateChange1:finalstr];
    
    
    
    NSDateFormatter *datePickerFormat = [[NSDateFormatter alloc] init];
    [datePickerFormat setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    NSDate *currentDate = [datePickerFormat dateFromString:Timestamp];
    NSDate *serverDate = [datePickerFormat dateFromString:apidate];
    
    NSLog(@"date %@",currentDate);
    NSLog(@"date %@",serverDate);
    
    NSComparisonResult result;
    result = [currentDate compare:serverDate]; // comparing two dates
    
    if(result == NSOrderedAscending)
    {
        NSLog(@"current date is less");
        
        NSString *sTitle=[[_ArrEvent objectAtIndex:sender.tag] objectForKey:@"EventTitle"];
        NSString *sTime=[[_ArrEvent objectAtIndex:sender.tag] objectForKey:@"EventStartChange"];
       // NSString *sLocation=[[_ArrEvent objectAtIndex:sender.tag] objectForKey:@"EventLocation"];
        NSString *FString=[NSString stringWithFormat:@"%@ :-   %@",sTitle,sTime];
        
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = serverDate;
        //localNotification.fireDate=[NSDate dateWithTimeIntervalSinceNow:30];
        localNotification.alertBody = FString;
        localNotification.soundName = @"alarm_sound.m4r";
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.applicationIconBadgeNumber = 0;
        NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
        [userinfo setValue:FString forKey:@"AlarmName"];
        [userinfo setValue:@"10" forKey:@"NT"];
        [userinfo setValue:_ArrEvent  forKey:@"Eventdetail"];
        localNotification.userInfo=userinfo;
        
        
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
        
        
        UILocalNotification* localNotification1 = [[UILocalNotification alloc] init];
        
        NSDate *date_before2hr=[serverDate dateByAddingTimeInterval:-60*60*2];
        localNotification1.fireDate =date_before2hr;
        //localNotification.fireDate=[NSDate dateWithTimeIntervalSinceNow:30];
        localNotification1.alertBody = FString;
        localNotification1.soundName = @"";
        localNotification1.timeZone = [NSTimeZone defaultTimeZone];
        localNotification1.applicationIconBadgeNumber = 0;
        NSMutableDictionary * userinfo1=[[NSMutableDictionary alloc]init];
        [userinfo1 setValue:FString forKey:@"AlarmName"];
        [userinfo1 setValue:@"10" forKey:@"NT"];
        [userinfo setValue:_ArrEvent forKey:@"Eventdetail"];
        localNotification1.userInfo=userinfo;
        
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification1];
        
        
        
        [self AddEventInDefaultCalendar:sTitle :serverDate];
        
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kAlertViewTitle message:@"Event has been added to calander" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];

    }
    else if(result == NSOrderedDescending)
    {
        NSLog(@"server date is less");
    }
    else if(result == NSOrderedSame)
    {
        NSLog(@"Both dates are same");
    }
    else
    {
        NSLog(@"Date cannot be compared");
    }
    
    
    
}
-(void)AddEventInDefaultCalendar:(NSString *)str_EventTitle :(NSDate *)str_EventDate
{
    
    // Create a new event object.
    store=[[EKEventStore alloc] init];
    //    [SVProgressHUD show];
    //    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    
    if ([store respondsToSelector:@selector(requestAccessToEntityType:completion:)])
    {
        [store requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError * _Nullable error)
         {
             if (granted)
             {
                 EKEvent *event = [EKEvent eventWithEventStore:store];
                 // Set the event title.
                 event.title = str_EventTitle;
                 // Set its calendar.
                 event.calendar = store.defaultCalendarForNewEvents;
                 event.location=@"SAAJ";
                 
                 NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init] ;
                 dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss.SSSSSS";
                 // NSDate* serverDate = [dateFormatter dateFromString:@"2011-04-21 03:31:37.310396"];
                 NSString *str_Date=[dateFormatter stringFromDate:str_EventDate];
                 [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss.SSSSSS"];
                 NSDate *lastdate=[dateFormatter dateFromString:str_Date];
                 
                 NSLog(@"%@", lastdate);
                 
                 
                 event.startDate = lastdate;
                 event.endDate=[[NSDate alloc] initWithTimeInterval:14400 sinceDate:event.startDate];
                 NSError *error;
                 
                 
                 NSDate* startDate =lastdate;
                 NSDate* endDate =[[NSDate alloc] initWithTimeInterval:14400 sinceDate:event.startDate];
                 NSPredicate *fetchCalendarEvents = [store predicateForEventsWithStartDate:startDate endDate:endDate calendars:nil];
                 
                 NSMutableArray *eventList=[[NSMutableArray alloc] init];
                 eventList = [NSMutableArray arrayWithArray:[store eventsMatchingPredicate:fetchCalendarEvents]];
                 
                 NSLog(@"allevent:%@",eventList);
                 
                 if (eventList.count==0)
                 {
                     BOOL savedEvent=[store saveEvent:event span:EKSpanThisEvent commit:YES error:&error];
                     
                     if (savedEvent)
                     {
                    
                     }
                 }
                 else
                 {
                     BOOL allreadyrecord=NO;
                     
                     for (int i=0; i<eventList.count; i++)
                     {
                         NSLog(@"Event calendar Title is = %@  insert event title=%@ ",[[eventList objectAtIndex:i] valueForKey:@"title"],str_EventTitle);
                         
                         NSString *str_geteventtitle=[NSString stringWithFormat:@"%@",[[eventList objectAtIndex:i] valueForKey:@"title"]];
                         NSString *str_latesttitle=[NSString stringWithFormat:@"%@",str_EventTitle];
                         
                         if ([str_geteventtitle isEqualToString:str_latesttitle])
                         {
                             NSLog(@"NO add");
                             allreadyrecord=NO;
                         }
                         else
                         {
                             allreadyrecord=YES;
                             
                             BOOL savedEvent=[store saveEvent:event span:EKSpanThisEvent commit:YES error:&error];
                             if (savedEvent)
                             {
                                 NSLog(@"successfully added");
                             }
                             break;
                         }
                         
                         
                         //                         if (![str_geteventtitle isEqualToString:str_EventTitle])
                         //                         {
                         //                             NSComparisonResult result;
                         //                             result = [[[eventList objectAtIndex:i] valueForKey:@"startDate"] compare:lastdate]; // comparing two dates
                         //
                         //                             if(result == NSOrderedSame)
                         //                             {
                         //                                 BOOL savedEvent=[store saveEvent:event span:EKSpanThisEvent commit:YES error:&error];
                         //
                         //                                 if (savedEvent)
                         //                                 {
                         //
                         //                                     UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kAlertViewTitle message:@"Event has been added to calander" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                         //                                     alert.tag=1;
                         //                                     [alert show];
                         //                                      break;
                         //                                 }
                         //
                         //                             }
                         //
                         //                         }
                         //                         else
                         //                         {
                         //                             if (![[[eventList objectAtIndex:i] valueForKey:@"location"] isEqualToString:@"KSIMC LONDON"])
                         //                             {
                         //                                 BOOL savedEvent=[store saveEvent:event span:EKSpanThisEvent commit:YES error:&error];
                         //
                         //
                         //                                 if (savedEvent)
                         //                                 {
                         //                                     UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kAlertViewTitle message:@"Event has been added to calander" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                         //                                     alert.tag=1;
                         //                                     [alert show];
                         //
                         //                                 }
                         //                             }
                         //                             else
                         //                             {
                         //
                         //
                         //                                 UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kAlertViewTitle message:@"Event Already added" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                         //                                 alert.tag=1;
                         //                                 [alert show];
                         //                             }
                         //                             break;
                         //                         }
                     }
                     
                     //                     if (allreadyrecord==NO)
                     //                     {
                     //                         UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kAlertViewTitle message:@"Event Already added" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                     //                         alert.tag=1;
                     //                         [alert show];
                     //                     }
                     //                     else
                     //                     {
                     //                         UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kAlertViewTitle message:@"Event has been added to calander" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                     //                         alert.tag=1;
                     //                         [alert show];
                     //                     }
                     //                     NSLog(allreadyrecord ? @"Yes" : @"No");
                 }
             }
             
             NSLog(@"nosd jiosjc");
             // [SVProgressHUD dismiss];
         }];
    }
}
-(NSString *)TimeFormateChange1:(NSString *)DateStr
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"YYYY-MM-dd hh:mm a";
    NSDate *date = [dateFormatter dateFromString:DateStr];
    dateFormatter.dateFormat = @"YYYY-MM-dd HH:mm:ss";
    return [dateFormatter stringFromDate:date];
}
-(NSString *)setDateFormat:(NSString *)EventDate
{
    NSDateFormatter *dateformat=[[NSDateFormatter alloc] init];
    [dateformat setDateFormat:@"yyyy-MM-dd"];
    NSDate *str_date=[dateformat dateFromString:EventDate];
    [dateformat setDateFormat:@"dd MMM yyyy"];
    NSString *finalDate = [dateformat stringFromDate:str_date];
    NSLog(@"%@", finalDate);
    return finalDate;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btn_Notification:(id)sender
{
    NotificationScreen *vc=[[NotificationScreen alloc]initWithNibName:@"NotificationScreen" bundle:nil];
    [self.navigationController pushViewController:vc animated:NO];
}

- (IBAction)btn_back:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}
-(void)setagain_click
{
    HomeTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"home"] imageDisabled:[UIImage imageNamed:@"home"]];
    HomeTabItem.titleString=@"Home";
    HomeTabItem.titleFontColor=[UIColor whiteColor];
    HomeTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];
    
    FirstTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabprayIconY"] imageDisabled:[UIImage imageNamed:@"TabprayIconW"]];
    FirstTabItem.titleString=@"Prayer Times";
    FirstTabItem.titleFontColor=[UIColor whiteColor];
    FirstTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];
    
    ThirdTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcalendarIconY"] imageDisabled:[UIImage imageNamed:@"TabcalendarIconW"]];
    ThirdTabItem.titleString=@"Calendar";
    ThirdTabItem.titleFontColor=[UIColor whiteColor];
    ThirdTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];
    
    FiveTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabvideoIconW"] imageDisabled:[UIImage imageNamed:@"TabvideoIconW"]];
    FiveTabItem.titleString=@"Media";
    FiveTabItem.titleFontColor=[UIColor whiteColor];
    FiveTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];
    
    SixTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcontactBookIconY"] imageDisabled:[UIImage imageNamed:@"TabcontactBookIconW"]];
    SixTabItem.titleString=@"Contact";
    SixTabItem.titleFontColor=[UIColor whiteColor];
    SixTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];
    
    RKTabView *tabView = [[RKTabView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-60,self.view.bounds.size.width, 60)];
    tabView.delegate = self;
    tabView.horizontalInsets = HorizontalEdgeInsetsMake(0, 0);;
    
    tabView.backgroundColor=[UIColor colorWithRed:(48.0f/255.0f) green:(82.0f/255.0f) blue:(1.0f/255.0f) alpha:1];
    
    tabView.tabItems =@[HomeTabItem,FirstTabItem, ThirdTabItem, FiveTabItem,  SixTabItem];
    
    [self.view addSubview:tabView];
    
}

@end
