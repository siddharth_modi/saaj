//
//  EventDetailViewController.h
//  Saaj
//
//  Created by mac  on 1/4/17.
//  Copyright © 2017 mac . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventDetailViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *lbl_Title;
@property (weak, nonatomic) IBOutlet UITableView *tbl_EventDetail;

@property(strong,nonatomic)NSString *PasSelectedDate;

@property (strong, nonatomic)NSMutableArray *ArrEvent;
@property (weak, nonatomic) IBOutlet UIButton *btn_Back;
@property(nonatomic,retain)NSString *str_notiFlag;
- (IBAction)btn_Notification:(id)sender;
- (IBAction)btn_back:(id)sender;

@end
