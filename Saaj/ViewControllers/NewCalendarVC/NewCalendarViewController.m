//
//  NewCalendarViewController.m
//  Saaj
//
//  Created by mac  on 12/30/16.
//  Copyright © 2016 mac . All rights reserved.
//

#import "NewCalendarViewController.h"
#import <RKTabView.h>
#import "DonationScreen.h"
#import "ScheduleScreen.h"
#import "ProjectScreen.h"
#import "VideosScreen.h"
#import "ContactScreen.h"
#import "NotificationScreen.h"

#import "Constants.h"
#import "AFNetworking.h"
#import "HTTPClient.h"
#import "SVProgressHUD.h"
#import <UIImageView+WebCache.h>
#import "HomeCell.h"
#import "MainHomeViewController.h"
#import "NewWeeklyViewController.h"
#import "EventListViewController.h"
#import "NewWeeklyViewController.h"
#import "WeeklyEventTableViewCell.h"
#import "EventDetailViewController.h"
@interface NewCalendarViewController ()<RKTabViewDelegate,HTTPClientDeleagte>
{
    RKTabItem *FirstTabItem,*SecondTabItem,*ThirdTabItem,*FourTabItem,*FiveTabItem,*SixTabItem ,*HomeTabItem;
     NSMutableDictionary *_eventsByDate;
    HTTPClient *Client_MonthlyEvent;
    NSDate *_dateSelected;
    
    NSString *start_Date;
    NSString *End_date;
//    NSString *str_NextMonthEnddate;
//    NSString *str_NextMonthStartDate;
    
    NSString *str_Prev_MonthEnddate;
    NSString *str_Prev_MonthStartDate;
    
    NSMutableArray *arr_monthlyEvent;
    
    NSMutableArray *arr_AllEventDate;
    
    NSMutableArray *arr_DayEvent;
    
    BOOL next_btnTap;
    NSMutableArray *ArrColordate;

}
@property(strong,nonatomic)NSString *PasSelectedDate;
@end

@implementation NewCalendarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
   
    
    arr_monthlyEvent=[[NSMutableArray alloc] init];
    _viewHeaderMain.hidden=NO;
    _calendarMenuView.hidden=NO;
     _tbl_MonthlyEvent.hidden=YES;
    next_btnTap=NO;
    HomeTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"home"] imageDisabled:[UIImage imageNamed:@"home"]];
    HomeTabItem.titleString=@"Home";
    HomeTabItem.titleFontColor=[UIColor whiteColor];
    HomeTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];
    
    FirstTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabprayIconY"] imageDisabled:[UIImage imageNamed:@"TabprayIconW"]];
    FirstTabItem.titleString=@"Prayer Times";
    FirstTabItem.titleFontColor=[UIColor whiteColor];
    FirstTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];
    
    ThirdTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcalendarIconY"] imageDisabled:[UIImage imageNamed:@"TabcalendarIconY"]];
    ThirdTabItem.titleString=@"Calendar";
    ThirdTabItem.titleFontColor=[UIColor whiteColor];
    ThirdTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];
    ThirdTabItem.tabState = TabStateEnabled;
    
    FiveTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabvideoIconW"] imageDisabled:[UIImage imageNamed:@"TabvideoIconW"]];
    FiveTabItem.titleString=@"Media";
    FiveTabItem.titleFontColor=[UIColor whiteColor];
    FiveTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];
    
    SixTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcontactBookIconY"] imageDisabled:[UIImage imageNamed:@"TabcontactBookIconW"]];
    SixTabItem.titleString=@"Contact";
    SixTabItem.titleFontColor=[UIColor whiteColor];
    SixTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];
    
    [self GetMonthStartAndEndDate:[NSDate date]];
    //set calendar
    
    _calendarManager = [JTCalendarManager new];
    _calendarManager.delegate = self;
    
    // set selected Event
    _dateSelected=[self TimeFormateChange3:_PasSelectedDate];
    
    //create Event
   // [self createRandomEvents];
    
    _viewHeaderMain.layer.borderWidth=1;
    _viewHeaderMain.layer.borderColor=[UIColor colorWithRed:(48.0f/255.0f) green:(82.0f/255.0f) blue:(1.0f/255.0f) alpha:1].CGColor;
    _calendarMenuView.layer.borderWidth=1;
    _calendarMenuView.layer.borderColor=[UIColor colorWithRed:(48.0f/255.0f) green:(82.0f/255.0f) blue:(1.0f/255.0f) alpha:1].CGColor;
    
    _calendarMenuView.contentRatio = .75;
    _calendarManager.settings.weekDayFormat = JTCalendarWeekDayFormatShort;
    
    [_btn_next addTarget:self action:@selector(setNextpage) forControlEvents:UIControlEventTouchUpInside];
    [_btn_previous addTarget:self action:@selector(setPreviouspage) forControlEvents:UIControlEventTouchUpInside];
    [_calendarManager setMenuView:_calendarMenuView];
    [_calendarManager setContentView:_viewHeaderMain];
    [_calendarManager setDate:[NSDate date]];
    
    // Do any additional setup after loading the view from its nib.
}
-(void)setNextpage
{
   [_viewHeaderMain loadNextPageWithAnimation];
}
-(void)setPreviouspage
{
    [_viewHeaderMain loadPreviousPageWithAnimation];
}
-(void)viewDidAppear:(BOOL)animated
{
    RKTabView *tabView = [[RKTabView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-60,self.view.bounds.size.width, 60)];
    tabView.delegate = self;
    tabView.horizontalInsets = HorizontalEdgeInsetsMake(0, 0);
    
    tabView.backgroundColor=[UIColor colorWithRed:(48.0f/255.0f) green:(82.0f/255.0f) blue:(1.0f/255.0f) alpha:1];
    tabView.tabItems =@[HomeTabItem,FirstTabItem, ThirdTabItem, FiveTabItem,  SixTabItem];
    [self.view addSubview:tabView];
}

#pragma mark - RKTabViewDelegate

- (void)tabView:(RKTabView *)tabView tabBecameEnabledAtIndex:(NSUInteger)index tab:(RKTabItem *)tabItem
{
    NSLog(@"Tab %tu enabled", index);
    
    if (index==0)
    {
        MainHomeViewController *vc=[[MainHomeViewController alloc]initWithNibName:@"MainHomeViewController" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    if (index==1)
    {
        HomeScreen *vc=[[HomeScreen alloc]initWithNibName:@"HomeScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    else if (index==2)
    {
        NewWeeklyViewController *vc=[[NewWeeklyViewController alloc]initWithNibName:@"NewWeeklyViewController" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    else if (index==3)
    {
        NSString *str_channelId1=[[NSUserDefaults standardUserDefaults] objectForKey:@"Youtubelink"];
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.youtube.com/channel/%@",str_channelId1]]])
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.youtube.com/channel/%@",str_channelId1]]];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:@"Invalid url" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
    else if (index==4)
    {
        ContactScreen *vc=[[ContactScreen alloc]initWithNibName:@"ContactScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
        
    }
}
- (void)tabView:(RKTabView *)tabView tabBecameDisabledAtIndex:(NSUInteger)index tab:(RKTabItem *)tabItem
{
    NSLog(@"Tab %tu disabled", index);
}
#pragma mark - CalendarManager delegate

// Exemple of implementation of prepareDayView method
// Used to customize the appearance of dayView
- (void)calendar:(JTCalendarManager *)calendar prepareDayView:(JTCalendarDayView *)dayView
{
    // Today
    if([dayView isFromAnotherMonth])
    {
        dayView.hidden = YES;
    }

    NSLog(@" date is == %@",dayView.date);
    if([_calendarManager.dateHelper date:[NSDate date] isTheSameDayThan:dayView.date])
    {
        dayView.circleView.hidden = NO;
        dayView.circleView.backgroundColor = [UIColor blueColor];
        dayView.dotView.backgroundColor = [UIColor whiteColor];
        dayView.textLabel.textColor = [UIColor whiteColor];
    }
    // Selected date
    else if(_dateSelected && [_calendarManager.dateHelper date:_dateSelected isTheSameDayThan:dayView.date]){
        dayView.circleView.hidden = NO;
        dayView.circleView.backgroundColor = [UIColor clearColor];
        dayView.dotView.backgroundColor = [UIColor whiteColor];
        dayView.textLabel.textColor = [UIColor lightGrayColor];
    }
    // Other month
    else if(![_calendarManager.dateHelper date:_viewHeaderMain.date isTheSameMonthThan:dayView.date])
    {
        dayView.circleView.hidden = YES;
        dayView.dotView.backgroundColor = [UIColor redColor];
        dayView.textLabel.textColor = [UIColor lightGrayColor];
    }
    // Another day of the current month
    else
    {
        dayView.circleView.hidden = YES;
        dayView.dotView.backgroundColor = [UIColor redColor];
        dayView.textLabel.textColor = [UIColor blackColor];
    }
    if([self haveEventForDay:dayView.date])
    {
        //dayView.dotView.hidden = NO;
        
        NSString *st=[NSString stringWithFormat:@"%@",dayView.date];
        NSString *firstWord = [[st componentsSeparatedByString:@" "] objectAtIndex:0];
        NSString *dateviewdate=[self GetNextDate:firstWord];
        
        for (int i=0; i<ArrColordate.count; i++)
        {
            NSString *colordate=[[ArrColordate objectAtIndex:i] objectForKey:@"date"];
            
            if ([dateviewdate isEqualToString:colordate])
            {
                NSString *colorcode=[[ArrColordate objectAtIndex:i] objectForKey:@"color"];
                
                if ([colorcode isEqualToString:@"R"])
                {
                    dayView.circleView.backgroundColor =[UIColor colorWithRed:0.93 green:0.17 blue:0.05 alpha:1];
                }
                else if ([colorcode isEqualToString:@"G"])
                {
                    dayView.circleView.backgroundColor =[UIColor colorWithRed:0.19 green:0.40 blue:0.04 alpha:1];
                }
                else if ([colorcode isEqualToString:@"B"])
                {
                    dayView.circleView.backgroundColor =[UIColor blackColor];
                }
            }
        }
        
        dayView.circleView.hidden = NO;
        dayView.dotView.backgroundColor = [UIColor whiteColor];
        dayView.textLabel.textColor = [UIColor whiteColor];
        

//        dayView.circleView.hidden=NO;
//         dayView.circleView.backgroundColor = [UIColor colorWithRed:(48.0f/255.0f) green:(82.0f/255.0f) blue:(1.0f/255.0f) alpha:1];
//        dayView.textLabel.textColor=[UIColor whiteColor];
//        dayView.textLabel.font=[UIFont systemFontOfSize:17 weight:5];
//        dayView.dotView.hidden = YES;
    }
    else
    {
        dayView.dotView.hidden = YES;
    }
}
-(NSString*)GetNextDate:(NSString *)dateString
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd"];
    NSDate *dateFromString = [[NSDate alloc] init];
    dateFromString = [dateFormatter dateFromString:dateString];
    
    NSDateComponents *components= [[NSDateComponents alloc] init];
    [components setDay:1];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *dateIncremented= [calendar dateByAddingComponents:components toDate:dateFromString options:0];
    
    NSDateFormatter *myDateFormatter = [[NSDateFormatter alloc] init];
    [myDateFormatter setDateFormat:@"YYYY-MM-dd"];
    NSString *stringFromDate = [myDateFormatter stringFromDate:dateIncremented];
    
    return stringFromDate;
}

- (BOOL)haveEventForDay:(NSDate *)date
{
    NSString *key = [[self dateFormatter] stringFromDate:date];
    
    if ([arr_AllEventDate containsObject:key])
    {
         return YES;
    }
//    if(_eventsByDate[key] && [_eventsByDate[key] count] > 0)
//    {
//        return YES;
//    }
    
    return NO;
    
}
- (void)createRandomEvents
{
    _eventsByDate = [NSMutableDictionary new];
    
    for(int i = 0; i < 30; ++i)
    {
        // Generate 30 random dates between now and 60 days later
        NSDate *randomDate = [NSDate dateWithTimeInterval:(rand() % (3600 * 24 * 60)) sinceDate:[NSDate date]];
        
        // Use the date as key for eventsByDate
        NSString *key = [[self dateFormatter] stringFromDate:randomDate];
        
        if(!_eventsByDate[key])
        {
            _eventsByDate[key] = [NSMutableArray new];
        }
        
        [_eventsByDate[key] addObject:randomDate];
    }
}

- (NSDateFormatter *)dateFormatter
{
    static NSDateFormatter *dateFormatter;
    if(!dateFormatter)
    {
        dateFormatter = [NSDateFormatter new];
        dateFormatter.dateFormat = @"dd-MM-yyyy";
    }
    return dateFormatter;
}

- (UIView *)calendarBuildMenuItemView:(JTCalendarManager *)calendar
{
    UILabel *label = [UILabel new];
    
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont systemFontOfSize:18];
    label.font=[UIFont systemFontOfSize:16 weight:5];
    label.textColor=[UIColor whiteColor];
    
    return label;
}

- (void)calendar:(JTCalendarManager *)calendar prepareMenuItemView:(UILabel *)menuItemView date:(NSDate *)date
{
    static NSDateFormatter *dateFormatter;
    if(!dateFormatter)
    {
        dateFormatter = [NSDateFormatter new];
        dateFormatter.dateFormat = @"MMMM yyyy";
    }
    menuItemView.text = [dateFormatter stringFromDate:date];
}
- (void)calendar:(JTCalendarManager *)calendar didTouchDayView:(JTCalendarDayView *)dayView
{
    _dateSelected = dayView.date;
    NSString *Selecteddate = [[self dateFormatter] stringFromDate:dayView.date];
    NSLog(@" Selected  date %@",Selecteddate);
    
    arr_DayEvent=[[NSMutableArray alloc] init];
    for (int i=0; i<arr_AllEventDate.count; i++)
    {
        if ([[arr_AllEventDate objectAtIndex:i] isEqualToString:Selecteddate])
        {
            [arr_DayEvent addObject:[arr_monthlyEvent objectAtIndex:i]];
        }
    }
    NSLog(@"%@",arr_DayEvent);
    if (arr_DayEvent.count!=0)
    {
        if (arr_DayEvent.count==1)
        {
            EventDetailViewController *vc=[[EventDetailViewController alloc]initWithNibName:@"EventDetailViewController" bundle:nil];
            NSMutableDictionary *dic=[arr_DayEvent objectAtIndex:0];
            NSMutableArray *arr_selectevent=[[NSMutableArray alloc] init];
            [arr_selectevent addObject:dic];
            vc.ArrEvent=arr_selectevent;
            [self.navigationController pushViewController:vc animated:NO];
        }
        else
        {
            _tbl_MonthlyEvent.hidden=NO;
            _viewHeaderMain.hidden=YES;
            _calendarMenuView.hidden=YES;
            
            [_tbl_MonthlyEvent reloadData];
        }
    }
    
    
    
    
    
//    if (arr_DayEvent.count==0)
//    {
//        _tbl_MonthlyEvent.hidden=YES;
//    }
//    else
//    {
//        _tbl_MonthlyEvent.hidden=NO;
//        _viewHeaderMain.hidden=YES;
//        _calendarMenuView.hidden=YES;
//        
//        [_tbl_MonthlyEvent reloadData];
//    }
    
//    if ([arr_AllEventDate containsObject:Selecteddate])
//    {
//        NewWeeklyViewController *vc=[[NewWeeklyViewController alloc]initWithNibName:@"NewWeeklyViewController" bundle:nil];
//        vc.arr_AllEvent=arr_monthlyEvent;
//        vc.Selecteddate=Selecteddate;
//        vc.arr_AllEventDate=arr_AllEventDate;
//        vc.str_Flag=@"calendar";
//        [self.navigationController pushViewController:vc animated:NO];
//    }
//    
   
    // Animation for the circleView
//    dayView.circleView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.1, 0.1);
//    [UIView transitionWithView:dayView
//                      duration:.3
//                       options:0
//                    animations:^
//     {
//         dayView.circleView.transform = CGAffineTransformIdentity;
//         [_calendarManager reload];
//     } completion:nil];
//    
    
    // Don't change page in week mode because block the selection of days in first and last weeks of the month
    if(_calendarManager.settings.weekModeEnabled)
    {
        return;
    }
    
    // Load the previous or next page if touch a day from another month
    
    if(![_calendarManager.dateHelper date:_viewHeaderMain.date isTheSameMonthThan:dayView.date]){
        if([_viewHeaderMain.date compare:dayView.date] == NSOrderedAscending){
            [_viewHeaderMain loadNextPageWithAnimation];
        }
        else
        {
            [_viewHeaderMain loadPreviousPageWithAnimation];
        }
    }
}

#pragma Calendar Next Month Method
- (void)calendarDidLoadNextPage:(JTCalendarManager *)calendar
{
      NSLog(@"Next month is %@",calendar.date);
      [self GetMonthStartAndEndDate:calendar.date];
}
-(NSString *)SetDateFormat_YYYY_MM_DD:(NSDate *)Date
{
    NSDateFormatter *dateformat=[[NSDateFormatter alloc] init];
    [dateformat setDateFormat:@"yyyy-MM-dd T HH:mm:ss ZZZZ"];
    
    NSString *strDate=[dateformat stringFromDate:Date];
    
    strDate=[self SetDateFormatyyyy_MM:Date];
    return strDate;
}

-(NSString *)SetDateFormatyyyy_MM:(NSDate *)Date
{
    NSDateFormatter *dateformat=[[NSDateFormatter alloc] init];
    [dateformat setDateFormat:@"yyyy-MM"];
    
    NSString *strDate=[dateformat stringFromDate:Date];
    
    return strDate;
}

#pragma Calendar Previous Month Method

- (void)calendarDidLoadPreviousPage:(JTCalendarManager *)calendar
{
    NSLog(@"Previous page loaded");
    NSLog(@"Next month is %@",calendar.date);
//    if (next_btnTap==NO)
//    {
         [self GetMonthStartAndEndDate:calendar.date];
 //   }
   
}
-(void)GetMonthStartAndEndDate:(NSDate *)date
{
    NSDate *today = date; //Get a date object for today's date
    NSCalendar *c = [NSCalendar currentCalendar];
    NSRange days = [c rangeOfUnit:NSDayCalendarUnit
                           inUnit:NSMonthCalendarUnit
                          forDate:today];
    
    NSLog(@"Total days %lu",(unsigned long)days.length);
    
    NSString *str_getMonth=[self SetDateFormat_YYYY_MM_DD:date];
    
    start_Date=[NSString stringWithFormat:@"%@-1",str_getMonth];
    End_date=[NSString stringWithFormat:@"%@-%lu",str_getMonth,(unsigned long)days.length];
    NSLog(@"Next Month \n Start Date  %@ \n Enddate  = %@ " ,str_Prev_MonthStartDate,str_Prev_MonthEnddate);
    
    [self GetEventMOnthlyList:start_Date :End_date];
}

#pragma mark - GetEventMonthlyList

-(void)GetEventMOnthlyList:(NSString *)StartDate :(NSString *)EndDate
{
    [SVProgressHUD show];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    
    NSMutableDictionary *parameter=[[NSMutableDictionary alloc] init];
    
    [parameter setValue:StartDate forKey:@"start_date"];
    [parameter setValue:EndDate forKey:@"end_date"];
    [parameter setValue:@"1" forKey:@"category"];

    
    NSLog(@"%@ param ",parameter);
    
    NSString *registerURL = [NSString stringWithFormat:@"%@get_events_data.php",kAPIURL];
    Client_MonthlyEvent = [[HTTPClient alloc] init];
    Client_MonthlyEvent.delegate = self;
    [Client_MonthlyEvent getResponseFromAPI:registerURL andParameters:parameter];
}

#pragma mark -
#pragma mark - HTTPClient Delegate methods
- (void)httpClient:(HTTPClient *)client didUpdateWithResponse:(id)response
{
    //NSLog(@"Response home screen: %@",response);
    
    [SVProgressHUD dismiss];
    if (client == Client_MonthlyEvent)
    {
        NSLog(@" Response is == %@",response);
        arr_AllEventDate=[[NSMutableArray alloc] init];
        arr_monthlyEvent=[[NSMutableArray alloc] init];
        arr_monthlyEvent=[[response valueForKey:@"data"] valueForKey:@"event"];
         ArrColordate=[[NSMutableArray alloc]init];
        NSLog(@" array wekkly Event %@",arr_monthlyEvent);
        
        NSString *str_totalEvent=[NSString stringWithFormat:@"%@",arr_monthlyEvent];
        
        if (![str_totalEvent isEqualToString:@""])
        {
            for (int i=0; i<arr_monthlyEvent.count; i++)
            {
                
                NSString *eventDate = [[arr_monthlyEvent objectAtIndex:i] valueForKey:@"EventDate"];
                
                NSDateFormatter *dateformat=[[NSDateFormatter alloc] init];
                [dateformat setDateFormat:@"yyyy-MM-dd"];
                NSDate *str_date=[dateformat dateFromString:eventDate];
                [dateformat setDateFormat:@"dd-MM-yyyy"];
                NSString *finalDate = [dateformat stringFromDate:str_date];
                NSLog(@"%@", finalDate);
                //  NSDate *date=[dateformat dateFromString:[arr_monthlyEvent valueForKey:@"EventDate"]];
                NSString *dateis=[[arr_monthlyEvent objectAtIndex:i] objectForKey:@"EventDate"];
                NSString *coloris=[[arr_monthlyEvent objectAtIndex:i] objectForKey:@"ColorCode"];
                
                NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
                [dict setValue:dateis forKey:@"date"];
                [dict setValue:coloris forKey:@"color"];
                [ArrColordate addObject:dict];
                [arr_AllEventDate addObject:finalDate];
            }
            NSLog(@" all event date %@",arr_AllEventDate);
        }
        [_calendarManager reload];

    }
}

- (void)httpClient:(HTTPClient *)client didFailedWithError:(NSError *)error
{
    [SVProgressHUD dismiss];
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:error.localizedDescription delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

#pragma mark -
#pragma mark -DateFormat methods
-(NSString *)TimeFormateChange1:(NSString *)DateStr
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"YYYY-MM-dd hh:mm a";
    NSDate *date = [dateFormatter dateFromString:DateStr];
    dateFormatter.dateFormat = @"YYYY-MM-dd HH:mm:ss";
    return [dateFormatter stringFromDate:date];
}


-(NSDate *)TimeFormateChange3:(NSString *)DateStr
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"YYYY-MM-dd";
    NSDate *date = [dateFormatter dateFromString:DateStr];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss ZZZ";
    return date;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma  --------- Tableview delegate Method ------------

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return arr_DayEvent.count;
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    WeeklyEventTableViewCell *cell = (WeeklyEventTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cell"];
        if(cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"WeeklyEventTableViewCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        [cell.img_bg layoutIfNeeded];
        NSString *myDateString = [[arr_DayEvent objectAtIndex:indexPath.row] valueForKey:@"EventDate"];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        
        NSDate *date = [dateFormatter dateFromString:myDateString];
        dateFormatter.dateFormat = @"MMM yyyy";
        NSString *dayName = [dateFormatter stringFromDate:date];
        NSLog(@"%@ is a %@", myDateString, dayName);
        cell.lbl_day.text=dayName;
        
        
        NSString *EventDate=[self setDateFormat:[[arr_DayEvent objectAtIndex:indexPath.row] valueForKey:@"EventDate"]];
        
        cell.lbl_date.text=EventDate;
        cell.backgroundColor=[UIColor clearColor];
        cell.lbl_EventDetail.text=[[arr_DayEvent objectAtIndex:indexPath.row] valueForKey:@"EventDetails"];
        cell.lbl_EventTitle.text=[NSString stringWithFormat:@"%@",[[arr_DayEvent objectAtIndex:indexPath.row] valueForKey:@"EventTitle"]];
    
        
        //    else
        //    {
        //         cell.textLabel.text=[NSString stringWithFormat:@"%@   - %@  - %@",EventDate,[[arr_WeeklyEvent objectAtIndex:indexPath.row] valueForKey:@"EventTitle"],[[arr_WeeklyEvent objectAtIndex:indexPath.row] valueForKey:@"EventDetails"]];
        //    }
        //
    
    NSString *colorcode=[[arr_DayEvent objectAtIndex:indexPath.row] valueForKey:@"ColorCode"];
    
    if ([colorcode isEqualToString:@"R"])
    {
        cell.lbl_EventTitle.textColor =[UIColor colorWithRed:0.93 green:0.17 blue:0.05 alpha:1];
        cell.lbl_datebg.backgroundColor =[UIColor colorWithRed:0.93 green:0.17 blue:0.05 alpha:1];
        
    }
    else if ([colorcode isEqualToString:@"G"])
    {
        cell.lbl_EventTitle.textColor =[UIColor colorWithRed:0.19 green:0.40 blue:0.04 alpha:1];
        cell.lbl_datebg.backgroundColor =[UIColor colorWithRed:0.19 green:0.40 blue:0.04 alpha:1];
    }
    else if ([colorcode isEqualToString:@"B"])
    {
        cell.lbl_EventTitle.textColor =[UIColor blackColor];
        cell.lbl_datebg.backgroundColor =[UIColor blackColor];
    }
        cell.textLabel.numberOfLines = 0;
        [cell.textLabel sizeToFit];
        cell.backgroundColor=[UIColor clearColor];
        
        return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    EventDetailViewController *vc=[[EventDetailViewController alloc]initWithNibName:@"EventDetailViewController" bundle:nil];
    NSMutableDictionary *dic=[arr_DayEvent objectAtIndex:indexPath.row];
    NSMutableArray *arr_selectevent=[[NSMutableArray alloc] init];
    [arr_selectevent addObject:dic];
    vc.ArrEvent=arr_selectevent;
    [self.navigationController pushViewController:vc animated:NO];
    
}
-(NSString *)setDateFormat:(NSString *)EventDate
{
    NSDateFormatter *dateformat=[[NSDateFormatter alloc] init];
    [dateformat setDateFormat:@"yyyy-MM-dd"];
    NSDate *str_date=[dateformat dateFromString:EventDate];
    [dateformat setDateFormat:@"dd"];
    NSString *finalDate = [dateformat stringFromDate:str_date];
    NSLog(@"%@", finalDate);
    return finalDate;
}
#pragma mark - Weekly Button Method

- (IBAction)btn_monthly:(id)sender
{
    _viewHeaderMain.hidden=NO;
    _calendarMenuView.hidden=NO;
    _tbl_MonthlyEvent.hidden=YES;
}
- (IBAction)btn_Weekly:(id)sender
{
    NewWeeklyViewController *vc=[[NewWeeklyViewController alloc]initWithNibName:@"NewWeeklyViewController" bundle:nil];
    [self.navigationController pushViewController:vc animated:NO];

}

#pragma mark - Notification Button Method

- (IBAction)btn_Notification:(id)sender
{
    NotificationScreen *vc=[[NotificationScreen alloc]initWithNibName:@"NotificationScreen" bundle:nil];
    [self.navigationController pushViewController:vc animated:NO];
}
//- (IBAction)btn_next:(id)sender
//{
//    next_btnTap=YES;
//  //  [self calendarDidLoadNextPage:_calendarManager];
//    [_calendarManager.contentView loadNextPage];
//  //  [_viewHeaderMain loadNextPage];
//}
//
//- (IBAction)btn_previous:(id)sender
//{
//     [_viewHeaderMain loadPreviousPage];
//}
@end
