//
//  NewsViewController.h
//  Saaj
//
//  Created by mac  on 1/5/17.
//  Copyright © 2017 mac . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tbl_News;
- (IBAction)btn_Notification:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *img_dashbordlogo;
@property(nonatomic,retain)NSString *str_flag;

@end
