//
//  NewsTableViewCell.h
//  Saaj
//
//  Created by mac  on 1/12/17.
//  Copyright © 2017 mac . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *img_News;
@property (weak, nonatomic) IBOutlet UILabel *lbl_NewsTitle;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Newsdetail;
@property (weak, nonatomic) IBOutlet UILabel *lbl_date;

@end
