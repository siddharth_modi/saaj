//
//  NewsDetailViewController.h
//  Saaj
//
//  Created by mac  on 1/12/17.
//  Copyright © 2017 mac . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsDetailViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *img_news;
@property (weak, nonatomic) IBOutlet UILabel *lbl_NewsTitle;
@property (weak, nonatomic) IBOutlet UITextView *txtview_NewsDetail;
@property(nonatomic,retain)NSMutableArray *Arr_NewsDetails;
@property (weak, nonatomic) IBOutlet UILabel *lbl_newsDetail;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollview_news;
@property (weak, nonatomic) IBOutlet UIImageView *img_dashbordlogo;
- (IBAction)btn_Notification:(id)sender;
- (IBAction)btn_Back:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *view_height;
@property(nonatomic,retain)NSString *str_flag;
@property(nonatomic,retain)NSString *str_Date;
@property (weak, nonatomic) IBOutlet UILabel *lbl_date;


@end
