//
//  SurveyFormViewController.h
//  Saaj
//
//  Created by mac  on 12/29/16.
//  Copyright © 2016 mac . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SurveyFormViewController : UIViewController

@property(nonatomic,retain)NSString *str_URL;
- (IBAction)btn_Notification:(id)sender;
@property (weak, nonatomic) IBOutlet UIWebView *webview_Survey;
- (IBAction)btn_Back:(id)sender;
@property(nonatomic,retain)NSString *str_flag;
@property (weak, nonatomic) IBOutlet UIImageView *img_logo;
@end
