//
//  SurveyTableViewCell.h
//  Saaj
//
//  Created by mac  on 12/29/16.
//  Copyright © 2016 mac . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SurveyTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *img_bg;
@property (weak, nonatomic) IBOutlet UILabel *lbl_surveyname;

@end
