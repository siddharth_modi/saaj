//
//  SurveyVCViewController.h
//  Saaj
//
//  Created by mac  on 12/29/16.
//  Copyright © 2016 mac . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SurveyVCViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tbl_survey;
@property(nonatomic,retain)NSString *str_flag;
- (IBAction)btn_Notification:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *img_dashbordlogo;

@end
