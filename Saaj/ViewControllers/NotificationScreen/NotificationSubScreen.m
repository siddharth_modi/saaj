//
//  NotificationSubScreen.m
//  KSIJ Mumbai
//
//  Created by Kishan on 14/12/16.
//  Copyright © 2016 Latitudetechnolabs. All rights reserved.
//

#import "NotificationSubScreen.h"
#import "MainHomeViewController.h"
#import "NewWeeklyViewController.h"

@interface NotificationSubScreen ()<RKTabViewDelegate>
{
    RKTabItem *FirstTabItem,*SecondTabItem,*ThirdTabItem,*FourTabItem,*FiveTabItem,*SixTabItem,*HomeTabItem;
    UILabel *lblDetailText;
    
}
@end

@implementation NotificationSubScreen
@synthesize ArrpassDetail;
@synthesize TblAlertdetail,lblTitle;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    HomeTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"home"] imageDisabled:[UIImage imageNamed:@"home"]];
    HomeTabItem.titleString=@"Home";
    HomeTabItem.titleFontColor=[UIColor whiteColor];
    HomeTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];
    
    FirstTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabprayIconY"] imageDisabled:[UIImage imageNamed:@"TabprayIconW"]];
    FirstTabItem.titleString=@"Prayer Times";
    FirstTabItem.titleFontColor=[UIColor whiteColor];
    FirstTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];
    
    ThirdTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcalendarIconY"] imageDisabled:[UIImage imageNamed:@"TabcalendarIconW"]];
    ThirdTabItem.titleString=@"Calendar";
    ThirdTabItem.titleFontColor=[UIColor whiteColor];
    ThirdTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];
    
    FiveTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabvideoIconY"] imageDisabled:[UIImage imageNamed:@"TabvideoIconW"]];
    FiveTabItem.titleString=@"Media";
    FiveTabItem.titleFontColor=[UIColor whiteColor];
    FiveTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];
    
    SixTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcontactBookIconY"] imageDisabled:[UIImage imageNamed:@"TabcontactBookIconW"]];
    SixTabItem.titleString=@"Contact";
    SixTabItem.titleFontColor=[UIColor whiteColor];
    SixTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];

    
    self.TblAlertdetail.tableFooterView = [[UIView alloc] init];
    self.TblAlertdetail.backgroundColor=[UIColor clearColor];
    
    self.TblAlertdetail.rowHeight =UITableViewAutomaticDimension;
    self.TblAlertdetail.estimatedRowHeight = 95;
    
    if ([_str_flag2 isEqualToString:@"YES"])
    {
        _lbl_announcement.text=@"URGENT ANNOUNCEMENT";
    }
    else
    {
         _lbl_announcement.text=@"ANNOUNCEMENT";
    }
    
    if ([_str_flag isEqualToString:@"noti"])
    {
        NSString *str_titile=[[[[ArrpassDetail objectAtIndex:0] objectForKey:@"aps"] objectForKey:@"mtitle"] uppercaseString];
         lblTitle.text=str_titile;
        lblDetailText.text=[[[[ArrpassDetail objectAtIndex:0] objectForKey:@"aps"] objectForKey:@"mdesc"] uppercaseString];
        [TblAlertdetail reloadData];
        _btn_back.hidden=YES;
    }
    else
    {
         _btn_back.hidden=NO;
       lblTitle.text=[[[ArrpassDetail objectAtIndex:0] objectForKey:@"message_title"] uppercaseString];
    }
   // lblTitle.text=[[ArrpassDetail valueForKey:@"message_title"] uppercaseString];

    
}
-(void)viewDidAppear:(BOOL)animated
{
    RKTabView *tabView = [[RKTabView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-60,self.view.bounds.size.width, 60)];
    tabView.delegate = self;
    tabView.horizontalInsets = HorizontalEdgeInsetsMake(0, 0);;
    
    tabView.backgroundColor=[UIColor colorWithRed:(48.0f/255.0f) green:(82.0f/255.0f) blue:(1.0f/255.0f) alpha:1];
    
    tabView.tabItems =@[HomeTabItem,FirstTabItem, ThirdTabItem, FiveTabItem,  SixTabItem];
    
    [self.view addSubview:tabView];
}
#pragma mark - RKTabViewDelegate
- (void)tabView:(RKTabView *)tabView tabBecameEnabledAtIndex:(NSUInteger)index tab:(RKTabItem *)tabItem
{
    NSLog(@"Tab %tu enabled", index);
    
    if (index==0)
    {
        MainHomeViewController *vc=[[MainHomeViewController alloc]initWithNibName:@"MainHomeViewController" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    if (index==1)
    {
        HomeScreen *vc=[[HomeScreen alloc]initWithNibName:@"HomeScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    else if (index==2)
    {
        NewWeeklyViewController *vc=[[NewWeeklyViewController alloc]initWithNibName:@"NewWeeklyViewController" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    else if (index==3)
    {
        NSString *str_channelId1=[[NSUserDefaults standardUserDefaults] objectForKey:@"Youtubelink"];
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.youtube.com/channel/%@",str_channelId1]]])
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.youtube.com/channel/%@",str_channelId1]]];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:@"Invalid url" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
            [alert show];
        }

    }
    else if (index==4)
    {
        ContactScreen *vc=[[ContactScreen alloc]initWithNibName:@"ContactScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
        
    }}
- (void)tabView:(RKTabView *)tabView tabBecameDisabledAtIndex:(NSUInteger)index tab:(RKTabItem *)tabItem
{
    NSLog(@"Tab %tu disabled", index);
}

#pragma mark -
#pragma mark - button Notification
- (IBAction)btnNoti:(id)sender
{
    NotificationScreen *vc=[[NotificationScreen alloc]initWithNibName:@"NotificationScreen" bundle:nil];
    [self.navigationController pushViewController:vc animated:NO];
}

#pragma mark -
#pragma mark - Tableview Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return ArrpassDetail.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"StatusCellIdentifier";
    NotificationSubCell *cell = (NotificationSubCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"NotificationSubCell" owner:self options:nil];
        
        cell = [nib objectAtIndex:0];
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.layer.borderWidth = 1;
    cell.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    if ([_str_flag isEqualToString:@"noti"])
    {
        NSString *str_titile=[[[[ArrpassDetail objectAtIndex:0] objectForKey:@"aps"] objectForKey:@"mtitle"] uppercaseString];
        cell.lblEventDetail.text=str_titile;
        cell.lbl_eventDetailsss.text=[[[[ArrpassDetail objectAtIndex:0] objectForKey:@"aps"] objectForKey:@"mdesc"] uppercaseString];
        cell.lblDateTime.text=[[[[ArrpassDetail objectAtIndex:0] objectForKey:@"aps"] objectForKey:@"alert_date"] uppercaseString];

    }
    else
    {
        cell.lblDateTime.text=[[ArrpassDetail objectAtIndex:indexPath.row] objectForKey:@"alert_date_format"];
        
        cell.lblEventDetail.text=[[ArrpassDetail objectAtIndex:indexPath.row] objectForKey:@"message_title"];
        cell.lbl_eventDetailsss.text=[[ArrpassDetail objectAtIndex:indexPath.row] objectForKey:@"message_details"];
    }

//    static NSString *CellIdentifier = @"newFriendCell";
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//    if (cell == nil)
//    {
//        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
//    }
//    
//    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
//    cell.layer.borderWidth = 1;
//    cell.layer.borderColor = [UIColor lightGrayColor].CGColor;
//    
//    
//    UIImageView *ImgDateIcon=[[UIImageView alloc]initWithFrame:CGRectMake(8, 8, 23, 23)];
//    ImgDateIcon.image=[UIImage imageNamed:@"calendorIcon_blue"];
//    [cell addSubview:ImgDateIcon];
//    
//    
//    
//    UILabel *lblDatetime=[[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(ImgDateIcon.frame)+8, 8, tableView.frame.size.width-45, 23)];
//    lblDatetime.text=[[ArrpassDetail objectAtIndex:indexPath.row] objectForKey:@"alert_date_format"];
//    [lblDatetime setFont:[UIFont systemFontOfSize:13]];
//    lblDatetime.textColor=[UIColor colorWithRed:0.47 green:0.63 blue:0.42 alpha:1];
//    [cell addSubview:lblDatetime];
//    
//    
//    
//    UILabel *lblDetailTitle=[[UILabel alloc]initWithFrame:CGRectMake(8, ImgDateIcon.frame.origin.y+ImgDateIcon.frame.size.height+8, tableView.frame.size.width-16, 23)];
//    lblDetailTitle.text=[[ArrpassDetail objectAtIndex:indexPath.row] objectForKey:@"message_title"];
//    lblDetailTitle.textColor=[UIColor colorWithRed:0.93 green:0.17 blue:0.05 alpha:1];
//    [cell addSubview:lblDetailTitle];
//
//    
//    
//    lblDetailText=[[UILabel alloc]initWithFrame:CGRectMake(8, lblDetailTitle.frame.origin.y+lblDetailTitle.frame.size.height+8, tableView.frame.size.width-16, 120)];
//    lblDetailText.numberOfLines=0;
//    [lblDetailText setFont:[UIFont systemFontOfSize:13]];
//    lblDetailText.text=[[ArrpassDetail objectAtIndex:indexPath.row] objectForKey:@"message_details"];
//    lblDetailText.textColor=[UIColor grayColor];
//    
//    CGRect addframe = lblDetailText.frame;
//    CGSize constraintis = CGSizeMake(CGRectGetWidth(lblDetailText.frame), MAXFLOAT);
//    CGSize sizeis = [lblDetailText sizeThatFits:constraintis];
//    addframe.size.height = sizeis.height;
//    lblDetailText.frame = addframe;
//    
//    [cell addSubview:lblDetailText];
    
    return cell;
    
}
//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    
//    UIFont *cellFont = [UIFont systemFontOfSize:13];
//    CGSize maximumLabelSize = CGSizeMake([[UIScreen mainScreen] bounds].size.width, CGFLOAT_MAX);
//    float hight=[self getLabelHeight:maximumLabelSize string:[[ArrpassDetail objectAtIndex:indexPath.row] valueForKey:@"message_details"] font:cellFont];
//    
//    if (hight>25)
//    {
//        return hight+110;
//    }
//    return 90.0;
//}

//-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    
//    NSMutableString *strauditType = [[ArrpassDetail objectAtIndex:indexPath.row] objectForKey:@"message_details"];
//    
//    CGRect textRect = [strauditType boundingRectWithSize:(CGSize){225, MAXFLOAT}
//                                                 options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
//                                              attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:13.0]}
//                                                 context:nil];
//    CGSize messageSize = textRect.size;
//    return messageSize.height  + 10.0f;
//    
//}

-(CGFloat)getLabelHeight:(CGSize)labelSize string: (NSString *)string font: (UIFont *)font
{
    CGSize size;
    
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [string boundingRectWithSize:labelSize
                                              options:NSStringDrawingUsesLineFragmentOrigin
                                           attributes:@{NSFontAttributeName:font}
                                              context:context].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    
    return size.height;
}

#pragma mark - DidReceiveMemoryWarning

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
- (IBAction)btn_back:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}
@end
