//
//  NotificationScreen.m
//  KSIJ Mumbai
//
//  Created by Kishan on 21/10/16.
//  Copyright © 2016 Latitudetechnolabs. All rights reserved.
//

#import "NotificationScreen.h"
#import "MainHomeViewController.h"
#import "NewWeeklyViewController.h"
@interface NotificationScreen ()<RKTabViewDelegate,HTTPClientDeleagte>
{
    HTTPClient *ClientNotiList;
    NSMutableArray *ArrNotiList;
    
    
    
    RKTabItem *FirstTabItem,*SecondTabItem,*ThirdTabItem,*FourTabItem,*FiveTabItem,*SixTabItem,*HomeTabItem;
}
@end

@implementation NotificationScreen
@synthesize viewNoti;
@synthesize TblNotiList;


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    HomeTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"home"] imageDisabled:[UIImage imageNamed:@"home"]];
    HomeTabItem.titleString=@"Home";
    HomeTabItem.titleFontColor=[UIColor whiteColor];
    HomeTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];
    
    FirstTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabprayIconY"] imageDisabled:[UIImage imageNamed:@"TabprayIconW"]];
    FirstTabItem.titleString=@"Prayer Times";
    FirstTabItem.titleFontColor=[UIColor whiteColor];
    FirstTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];
    
    ThirdTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcalendarIconY"] imageDisabled:[UIImage imageNamed:@"TabcalendarIconW"]];
    ThirdTabItem.titleString=@"Calendar";
    ThirdTabItem.titleFontColor=[UIColor whiteColor];
    ThirdTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];
    
    FiveTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabvideoIconY"] imageDisabled:[UIImage imageNamed:@"TabvideoIconW"]];
    FiveTabItem.titleString=@"Media";
    FiveTabItem.titleFontColor=[UIColor whiteColor];
    FiveTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];
    
    SixTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcontactBookIconY"] imageDisabled:[UIImage imageNamed:@"TabcontactBookIconW"]];
    SixTabItem.titleString=@"Contact";
    SixTabItem.titleFontColor=[UIColor whiteColor];
    SixTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];

    
    viewNoti.layer.borderWidth = 1;
    viewNoti.layer.borderColor = [UIColor lightGrayColor].CGColor;
    [viewNoti setHidden:YES];
    
    self.TblNotiList.tableFooterView = [[UIView alloc] init];
    self.TblNotiList.backgroundColor=[UIColor clearColor];
   
}
-(void)viewDidAppear:(BOOL)animated
{
    RKTabView *tabView = [[RKTabView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-60,self.view.bounds.size.width, 60)];
    tabView.delegate = self;
    tabView.horizontalInsets = HorizontalEdgeInsetsMake(0, 0);;
    
    tabView.backgroundColor=[UIColor colorWithRed:(48.0f/255.0f) green:(82.0f/255.0f) blue:(1.0f/255.0f) alpha:1];
    
    tabView.tabItems =@[HomeTabItem,FirstTabItem, ThirdTabItem, FiveTabItem,  SixTabItem];
    
    [self.view addSubview:tabView];
}
#pragma mark - RKTabViewDelegate
- (void)tabView:(RKTabView *)tabView tabBecameEnabledAtIndex:(NSUInteger)index tab:(RKTabItem *)tabItem
{
    NSLog(@"Tab %tu enabled", index);
    if (index==0)
    {
        MainHomeViewController *vc=[[MainHomeViewController alloc]initWithNibName:@"MainHomeViewController" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    if (index==1)
    {
        HomeScreen *vc=[[HomeScreen alloc]initWithNibName:@"HomeScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    else if (index==2)
    {
        NewWeeklyViewController *vc=[[NewWeeklyViewController alloc]initWithNibName:@"NewWeeklyViewController" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];

        
    }
    else if (index==3)
    {
        NSString *str_channelId1=[[NSUserDefaults standardUserDefaults] objectForKey:@"Youtubelink"];
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.youtube.com/channel/%@",str_channelId1]]])
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.youtube.com/channel/%@",str_channelId1]]];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:@"Invalid url" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
            [alert show];
        }

    }
    else if (index==4)
    {
        ContactScreen *vc=[[ContactScreen alloc]initWithNibName:@"ContactScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
        
    }
    
}
- (void)tabView:(RKTabView *)tabView tabBecameDisabledAtIndex:(NSUInteger)index tab:(RKTabItem *)tabItem
{
    NSLog(@"Tab %tu disabled", index);
}
-(void)viewWillAppear:(BOOL)animated
{
    
    if (_arr_urgentAnnouncement.count==0 ||_arr_urgentAnnouncement==nil)
    {
         [self GetNotificationList];
        _lbl_headerAnnouncement.text=@"ANNOUNCEMENT";
    }
    else
    {
        ArrNotiList=_arr_urgentAnnouncement.mutableCopy;
        _lbl_headerAnnouncement.text=@"URGENT ANNOUNCEMENT";
        [TblNotiList reloadData];
    }
   
}



#pragma mark - GetNotiFicationList
-(void)GetNotificationList
{
    [SVProgressHUD show];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];

    NSString *registerURL = [NSString stringWithFormat:@"%@get_alert_data_web.php",kAPIURL];
    ClientNotiList = [[HTTPClient alloc] init];
    ClientNotiList.delegate = self;
    [ClientNotiList getResponseFromAPI:registerURL andParameters:nil];
}

#pragma mark -
#pragma mark - HTTPClient Delegate methods
- (void)httpClient:(HTTPClient *)client didUpdateWithResponse:(id)response
{
    //NSLog(@"Response home screen: %@",response);
    
    [SVProgressHUD dismiss];
    if (client == ClientNotiList)
    {
        NSString *AltListcount=[[response objectForKey:@"alert_list_count"] stringValue];
        
        if (AltListcount!=0)
        {
            ArrNotiList=[[NSMutableArray alloc]init];
            ArrNotiList=[response objectForKey:@"alert_list"];
            
            NSMutableArray *arr_alert=[[NSMutableArray alloc] init];
            
            for (int i=0; i<ArrNotiList.count; i++)
            {
                NSString *urgetntAnnouncement1=[[ArrNotiList objectAtIndex:i] valueForKey:@"urgent_announcements"];
                if ([urgetntAnnouncement1 isEqualToString:@"0"])
                {
                    [ arr_alert addObject:[ArrNotiList objectAtIndex:i]];
                }
            }
            
            NSLog(@"%@",arr_alert);
            
            ArrNotiList=arr_alert.mutableCopy;
            
            
            if (ArrNotiList.count==0)
            {
                viewNoti.hidden=NO;
            }
            else
            {
                 viewNoti.hidden=YES;
            }
            
            [TblNotiList reloadData];
        }
        else
        {
            [viewNoti setHidden:NO];
        }
    }
}
- (void)httpClient:(HTTPClient *)client didFailedWithError:(NSError *)error
{
    [SVProgressHUD dismiss];
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:error.localizedDescription delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}


#pragma mark -
#pragma mark - Tableview Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return ArrNotiList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"StatusCellIdentifier";
    NotificationCell *cell = (NotificationCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"NotificationCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.layer.borderWidth = 1;
    cell.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    cell.lblDateTime.text =[[ArrNotiList objectAtIndex:indexPath.row] objectForKey:@"alert_date_format"];
    cell.lblEventDetail.text=[[ArrNotiList objectAtIndex:indexPath.row] objectForKey:@"message_title"];
    cell.lblEventDetail.textColor=[UIColor colorWithRed:0.93 green:0.17 blue:0.05 alpha:1];
    
    [cell.lblEventDetail sizeToFit];
    return cell;
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NotificationSubScreen *vc=[[NotificationSubScreen alloc]initWithNibName:@"NotificationSubScreen" bundle:nil];
    
    NSMutableArray *Temp=[[NSMutableArray alloc]init];
    [Temp addObject:[ArrNotiList objectAtIndex:indexPath.row]];
    vc.ArrpassDetail=Temp;
    
     if (_arr_urgentAnnouncement.count==0 ||_arr_urgentAnnouncement==nil)
     {
         vc.str_flag2=@"No";
     }
    else
    {
        vc.str_flag2=@"YES";
    }
    [self.navigationController pushViewController:vc animated:NO];
}

#pragma mark -
#pragma mark - button Notification
- (IBAction)btnNoti:(id)sender
{
    [self GetNotificationList];
}

#pragma mark - DidReceiveMemoryWarning
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


@end
