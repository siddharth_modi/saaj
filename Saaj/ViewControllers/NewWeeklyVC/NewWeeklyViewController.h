//
//  NewWeeklyViewController.h
//  Saaj
//
//  Created by mac  on 12/30/16.
//  Copyright © 2016 mac . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewWeeklyViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tbl_WeeklyEvent;

@property (weak, nonatomic) IBOutlet UIButton *btn_weekly;
@property (weak, nonatomic) IBOutlet UIButton *btn_Monthly;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Date;
@property (weak, nonatomic) IBOutlet UILabel *lbl_noEvent;

@property(nonatomic,retain)NSMutableArray *arr_AllEvent;
@property(nonatomic,retain)NSMutableArray *arr_AllEventDate;
@property(nonatomic,retain)NSString *Selecteddate;
@property (strong, nonatomic) IBOutlet UIView *view_header;
@property (weak, nonatomic) IBOutlet UILabel *lbl_tblHeader;


@property(nonatomic,retain)NSString *str_Flag;

- (IBAction)btn_Notification:(id)sender;
- (IBAction)btn_Monthly:(id)sender;

@end
