//
//  NewWeeklyViewController.m
//  Saaj
//
//  Created by mac  on 12/30/16.
//  Copyright © 2016 mac . All rights reserved.
//

#import "NewWeeklyViewController.h"
#import <RKTabView.h>
#import "DonationScreen.h"
#import "ScheduleScreen.h"
#import "ProjectScreen.h"
#import "VideosScreen.h"
#import "ContactScreen.h"
#import "NotificationScreen.h"

#import "Constants.h"
#import "AFNetworking.h"
#import "HTTPClient.h"
#import "SVProgressHUD.h"
#import <UIImageView+WebCache.h>
#import "HomeCell.h"
#import "MainHomeViewController.h"
#import "NewCalendarViewController.h"
#import "NotificationScreen.h"
#import "EventDetailViewController.h"
#import "WeeklyEventTableViewCell.h"
#import "NewWeeklyViewController.h"
@interface NewWeeklyViewController ()<RKTabViewDelegate,HTTPClientDeleagte>
{
    RKTabItem *FirstTabItem,*SecondTabItem,*ThirdTabItem,*FourTabItem,*FiveTabItem,*SixTabItem ,*HomeTabItem;
    HTTPClient *ClientWeekly;
    NSMutableArray *arr_WeeklyEvent;
    NSString *Str_CurrenDate;
    NSString *Str_WeekEndDate;
    
    

}
@end

@implementation NewWeeklyViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
//    self.tbl_WeeklyEvent.estimatedRowHeight = 70; // put max you expect here.
//    self.tbl_WeeklyEvent.rowHeight = UITableViewAutomaticDimension;
    
    _lbl_noEvent.hidden=YES;
    
    if ([_str_Flag isEqualToString:@"calendar"] || _str_Flag!=nil)
    {
        
        _btn_weekly.hidden=YES;
        _btn_Monthly.hidden=YES;
        _lbl_Date.hidden=NO;
        _lbl_Date.text=_Selecteddate;
        
        arr_WeeklyEvent=[[NSMutableArray alloc] init];
        
        NSLog(@"%@ - %@ %@",_arr_AllEvent,_Selecteddate,_arr_AllEventDate);
        
        for (int i=0; i<_arr_AllEventDate.count; i++)
        {
            if ([[_arr_AllEventDate objectAtIndex:i] isEqualToString:_Selecteddate])
            {
                [arr_WeeklyEvent addObject:[_arr_AllEvent objectAtIndex:i]];
            }
        }
        [_tbl_WeeklyEvent reloadData];

    }
    else
    {
        _btn_weekly.hidden=NO;
        _btn_Monthly.hidden=NO;
        _lbl_Date.hidden=YES;
        
        NSCalendar *calendar=[NSCalendar currentCalendar];
        NSDateComponents *components = [[NSDateComponents alloc]init];
        components.day = 7;
        NSDate *fireDate =[calendar dateByAddingComponents:components toDate:[NSDate date] options:0];
        
        Str_CurrenDate=[self SetDateFormat_YYYY_MM_DD:[NSDate date]];
        Str_WeekEndDate=[self SetDateFormat_YYYY_MM_DD:fireDate];
        NSLog(@" Current date = %@ ,WeekEnd Date = %@",Str_CurrenDate,Str_WeekEndDate);
        
        NSLog(@"%@",fireDate);
        
        [self GetEventWeeklyList];
    }
    
    
    
   
    
//    arr_data=[[NSMutableArray alloc] initWithArray:@[@"Event 1",@"Event 2",@"Event 3",@"Event 4",@"Event 5",@"Event 6",@"Event 7"]];
    
    HomeTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"home"] imageDisabled:[UIImage imageNamed:@"home"]];
    HomeTabItem.titleString=@"Home";
    HomeTabItem.titleFontColor=[UIColor whiteColor];
    HomeTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];
    
    FirstTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabprayIconY"] imageDisabled:[UIImage imageNamed:@"TabprayIconW"]];
    FirstTabItem.titleString=@"Prayer Times";
    FirstTabItem.titleFontColor=[UIColor whiteColor];
    FirstTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];
    
    ThirdTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcalendarIconY"] imageDisabled:[UIImage imageNamed:@"TabcalendarIconY"]];
    ThirdTabItem.titleString=@"Calendar";
    ThirdTabItem.titleFontColor=[UIColor whiteColor];
    ThirdTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];
    ThirdTabItem.tabState = TabStateEnabled;

    FiveTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabvideoIconW"] imageDisabled:[UIImage imageNamed:@"TabvideoIconW"]];
    FiveTabItem.titleString=@"Media";
    FiveTabItem.titleFontColor=[UIColor whiteColor];
    FiveTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];
    
    SixTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcontactBookIconY"] imageDisabled:[UIImage imageNamed:@"TabcontactBookIconW"]];
    SixTabItem.titleString=@"Contact";
    SixTabItem.titleFontColor=[UIColor whiteColor];
    SixTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];

    
    // Do any additional setup after loading the view from its nib.
}
-(void)viewDidAppear:(BOOL)animated
{
    RKTabView *tabView = [[RKTabView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-60,self.view.bounds.size.width, 60)];
    tabView.delegate = self;
    tabView.horizontalInsets = HorizontalEdgeInsetsMake(0, 0);;
    tabView.backgroundColor=[UIColor colorWithRed:(48.0f/255.0f) green:(82.0f/255.0f) blue:(1.0f/255.0f) alpha:1];
    tabView.tabItems =@[HomeTabItem,FirstTabItem, ThirdTabItem, FiveTabItem,  SixTabItem];
    
    [self.view addSubview:tabView];
}
#pragma mark - RKTabViewDelegate
- (void)tabView:(RKTabView *)tabView tabBecameEnabledAtIndex:(NSUInteger)index tab:(RKTabItem *)tabItem
{
    NSLog(@"Tab %tu enabled", index);
    
    if (index==0)
    {
        MainHomeViewController *vc=[[MainHomeViewController alloc]initWithNibName:@"MainHomeViewController" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    if (index==1)
    {
        HomeScreen *vc=[[HomeScreen alloc]initWithNibName:@"HomeScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    else if (index==3)
    {
        NSString *str_channelId1=[[NSUserDefaults standardUserDefaults] objectForKey:@"Youtubelink"];
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.youtube.com/channel/%@",str_channelId1]]])
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.youtube.com/channel/%@",str_channelId1]]];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:@"Invalid url" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
            [alert show];
        }
        [self setagain_click];

    }
    else if (index==4)
    {
        ContactScreen *vc=[[ContactScreen alloc]initWithNibName:@"ContactScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
        
    }
}

- (void)tabView:(RKTabView *)tabView tabBecameDisabledAtIndex:(NSUInteger)index tab:(RKTabItem *)tabItem
{
    NSLog(@"Tab %tu disabled", index);
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - GetEventWeeklyList
-(void)GetEventWeeklyList
{
    [SVProgressHUD show];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    
    NSMutableDictionary *parameter=[[NSMutableDictionary alloc] init];
    
    [parameter setValue:Str_CurrenDate forKey:@"start_date"];
    [parameter setValue:Str_WeekEndDate forKey:@"end_date"];
    [parameter setValue:@"1" forKey:@"category"];

    NSLog(@"%@ param ",parameter);
    
    NSString *registerURL = [NSString stringWithFormat:@"%@get_events_data.php",kAPIURL];
    ClientWeekly = [[HTTPClient alloc] init];
    ClientWeekly.delegate = self;
    [ClientWeekly getResponseFromAPI:registerURL andParameters:parameter];
}

#pragma mark -
#pragma mark - HTTPClient Delegate methods
- (void)httpClient:(HTTPClient *)client didUpdateWithResponse:(id)response
{
    //NSLog(@"Response home screen: %@",response);
    
    [SVProgressHUD dismiss];
    if (client == ClientWeekly)
        
    {
        
        NSLog(@" Response is == %@",response);
        
        arr_WeeklyEvent=[[NSMutableArray alloc] init];
        
        arr_WeeklyEvent=[[response valueForKey:@"data"] valueForKey:@"event"];
        
        NSString *str_Array=[NSString stringWithFormat:@"%@",[[response valueForKey:@"data"] valueForKey:@"event"]];
        
        if (![str_Array isEqualToString:@""])
        {
            [_tbl_WeeklyEvent reloadData];
             _lbl_noEvent.hidden=YES;
        }
        else
        {
             _lbl_noEvent.hidden=NO;
        }
      
        NSLog(@" array wekkly Event %@",arr_WeeklyEvent);
        
        _lbl_tblHeader.text=[NSString stringWithFormat:@"%@  To  %@",[self SetDateFormatDD_MM_YYYY:Str_CurrenDate],[self SetDateFormatDD_MM_YYYY:Str_WeekEndDate]];
       
        
        
//        NSDate *curDate = [NSDate date];
//        NSCalendar* calendar = [NSCalendar currentCalendar];
//        NSDateComponents* comps = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSWeekCalendarUnit|NSWeekdayCalendarUnit fromDate:curDate]; // Get necessary date components
//        
//        // set last of month
//        [comps setMonth:[comps month]+1];
//        [comps setDay:0];
//        NSDate *tDateMonth = [calendar dateFromComponents:comps];
//        NSLog(@"%@", tDateMonth);
    }
}
- (void)httpClient:(HTTPClient *)client didFailedWithError:(NSError *)error
{
    [SVProgressHUD dismiss];
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:error.localizedDescription delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}
-(NSString *)SetDateFormat_YYYY_MM_DD:(NSDate *)Date
{
    NSDateFormatter *dateformat=[[NSDateFormatter alloc] init];
    [dateformat setDateFormat:@"yyyy-MM-dd T HH:mm:ss ZZZZ"];
    
    NSString *strDate=[dateformat stringFromDate:Date];
    
    return strDate;
}
-(NSString *)SetDateFormatDD_MM_YYYY:(NSString *)Date
{
    NSDateFormatter *dateformat=[[NSDateFormatter alloc] init];
    [dateformat  setDateFormat:@"yyyy-MM-dd"];
    NSDate *date1=[dateformat dateFromString:Date];
    [dateformat  setDateFormat:@"dd-MM-yyyy"];
    return [dateformat stringFromDate:date1];
}
//-(NSDate *)SetDateFormat_YYYY_MM_DD:(NSString *)str
//{
//    
//    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
//    [dateFormat setDateFormat:@"yyyy-MM-dd T HH:mm:ss ZZZZ"];
//    
//   
//    
//    [dateFormat setDateFormat:@"yyyy-MM-dd"];
//    ;
//    
//    return ;
//}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma  --------- Weekly butoon Method ------------

- (IBAction)btn_Monthly:(id)sender
{
    NewCalendarViewController *vc=[[NewCalendarViewController alloc]initWithNibName:@"NewCalendarViewController" bundle:nil];
    [self.navigationController pushViewController:vc animated:NO];
}

#pragma  --------- Tableview delegate Method ------------

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return arr_WeeklyEvent.count;
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    if ([_str_Flag isEqualToString:@"calendar"] || _str_Flag!=nil)
    {
        UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
        
        if (cell==nil)
        {
            cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        }
        cell.textLabel.text=[NSString stringWithFormat:@"%@",[[arr_WeeklyEvent objectAtIndex:indexPath.row] valueForKey:@"EventTitle"]];
        
        cell.textLabel.numberOfLines = 0;
        [cell.textLabel sizeToFit];
        cell.backgroundColor=[UIColor clearColor];
        
        return cell;
        
    }
    else
    {
        WeeklyEventTableViewCell *cell = (WeeklyEventTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cell"];
        if(cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"WeeklyEventTableViewCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        [cell.img_bg layoutIfNeeded];
        NSString *myDateString = [[arr_WeeklyEvent objectAtIndex:indexPath.row] valueForKey:@"EventDate"];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        
        NSDate *date = [dateFormatter dateFromString:myDateString];
//        NSDateComponents *components = [[NSCalendar currentCalendar] components:NSWeekdayCalendarUnit fromDate:date];
//        
//        NSInteger weekday = [components weekday];
//        NSString *weekdayName = [dateFormatter weekdaySymbols][weekday - 1];
        
        dateFormatter.dateFormat = @"MMM yyyy";
        NSString *dayName = [dateFormatter stringFromDate:date];
        NSLog(@"%@ is a %@", myDateString, dayName);
        cell.lbl_day.text=dayName;
        
        NSString *EventDate=[self setDateFormat:[[arr_WeeklyEvent objectAtIndex:indexPath.row] valueForKey:@"EventDate"]];
        
        cell.lbl_date.text=EventDate;
       
        cell.lbl_EventDetail.text=[[arr_WeeklyEvent objectAtIndex:indexPath.row] valueForKey:@"EventDetails"];
        
        NSString *colorcode=[[arr_WeeklyEvent objectAtIndex:indexPath.row] valueForKey:@"ColorCode"];
        
        if ([colorcode isEqualToString:@"R"])
        {
            cell.lbl_EventTitle.textColor =[UIColor colorWithRed:0.93 green:0.17 blue:0.05 alpha:1];
            cell.lbl_datebg.backgroundColor =[UIColor colorWithRed:0.93 green:0.17 blue:0.05 alpha:1];
            
        }
        else if ([colorcode isEqualToString:@"G"])
        {
            cell.lbl_EventTitle.textColor =[UIColor colorWithRed:0.19 green:0.40 blue:0.04 alpha:1];
              cell.lbl_datebg.backgroundColor =[UIColor colorWithRed:0.19 green:0.40 blue:0.04 alpha:1];
        }
        else if ([colorcode isEqualToString:@"B"])
        {
            cell.lbl_EventTitle.textColor =[UIColor blackColor];
            cell.lbl_datebg.backgroundColor =[UIColor blackColor];
        }

        
        if ([_str_Flag isEqualToString:@"calendar"] || _str_Flag!=nil)
        {
            cell.textLabel.text=[NSString stringWithFormat:@"%@",[[arr_WeeklyEvent objectAtIndex:indexPath.row] valueForKey:@"EventTitle"]];
            
        }
        else
        {
            cell.lbl_EventTitle.text=[NSString stringWithFormat:@"%@",[[arr_WeeklyEvent objectAtIndex:indexPath.row] valueForKey:@"EventTitle"]];
            // cell.textLabel.text=[NSString stringWithFormat:@"%@   -  %@",EventDate,[[arr_WeeklyEvent objectAtIndex:indexPath.row] valueForKey:@"EventTitle"]];
        }
        
        
        //    else
        //    {
        //         cell.textLabel.text=[NSString stringWithFormat:@"%@   - %@  - %@",EventDate,[[arr_WeeklyEvent objectAtIndex:indexPath.row] valueForKey:@"EventTitle"],[[arr_WeeklyEvent objectAtIndex:indexPath.row] valueForKey:@"EventDetails"]];
        //    }
        //
        cell.textLabel.numberOfLines = 0;
        [cell.textLabel sizeToFit];
        cell.backgroundColor=[UIColor clearColor];
        
        return cell;

    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    EventDetailViewController *vc=[[EventDetailViewController alloc]initWithNibName:@"EventDetailViewController" bundle:nil];
    
    NSMutableDictionary *dic=[arr_WeeklyEvent objectAtIndex:indexPath.row];
    NSMutableArray *arr_selectevent=[[NSMutableArray alloc] init];
    [arr_selectevent addObject:dic];
    vc.ArrEvent=arr_selectevent;
    [self.navigationController pushViewController:vc animated:NO];

}

-(NSString *)setDateFormat:(NSString *)EventDate
{
    NSDateFormatter *dateformat=[[NSDateFormatter alloc] init];
    [dateformat setDateFormat:@"yyyy-MM-dd"];
    NSDate *str_date=[dateformat dateFromString:EventDate];
    [dateformat setDateFormat:@"dd"];
    NSString *finalDate = [dateformat stringFromDate:str_date];
    NSLog(@"%@", finalDate);
    return finalDate;
}

#pragma  --------- Notification Button Method ------------

- (IBAction)btn_Notification:(id)sender
{
    NotificationScreen *vc=[[NotificationScreen alloc]initWithNibName:@"NotificationScreen" bundle:nil];
    [self.navigationController pushViewController:vc animated:NO];
}
-(void)setagain_click
{
    HomeTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"home"] imageDisabled:[UIImage imageNamed:@"home"]];
    HomeTabItem.titleString=@"Home";
    HomeTabItem.titleFontColor=[UIColor whiteColor];
    HomeTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];
    
    FirstTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabprayIconY"] imageDisabled:[UIImage imageNamed:@"TabprayIconW"]];
    FirstTabItem.titleString=@"Prayer Times";
    FirstTabItem.titleFontColor=[UIColor whiteColor];
    FirstTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];
    
    ThirdTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcalendarIconY"] imageDisabled:[UIImage imageNamed:@"TabcalendarIconY"]];
    ThirdTabItem.titleString=@"Calendar";
    ThirdTabItem.titleFontColor=[UIColor whiteColor];
    ThirdTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];
    ThirdTabItem.tabState = TabStateEnabled;
    
    FiveTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabvideoIconW"] imageDisabled:[UIImage imageNamed:@"TabvideoIconW"]];
    FiveTabItem.titleString=@"Media";
    FiveTabItem.titleFontColor=[UIColor whiteColor];
    FiveTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];
    
    SixTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcontactBookIconY"] imageDisabled:[UIImage imageNamed:@"TabcontactBookIconW"]];
    SixTabItem.titleString=@"Contact";
    SixTabItem.titleFontColor=[UIColor whiteColor];
    SixTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];
    
    
    RKTabView *tabView = [[RKTabView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-60,self.view.bounds.size.width, 60)];
    tabView.delegate = self;
    tabView.horizontalInsets = HorizontalEdgeInsetsMake(0, 0);;
    
    tabView.backgroundColor=[UIColor colorWithRed:(48.0f/255.0f) green:(82.0f/255.0f) blue:(1.0f/255.0f) alpha:1];
    
    tabView.tabItems =@[HomeTabItem,FirstTabItem, ThirdTabItem, FiveTabItem,  SixTabItem];
    
    [self.view addSubview:tabView];
    
}


@end
