//
//  EventdetailTableViewCell.h
//  Saaj
//
//  Created by mac  on 1/11/17.
//  Copyright © 2017 mac . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventdetailTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbl_title;
@property (weak, nonatomic) IBOutlet UILabel *lbl_date;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Time;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Location;
@property (weak, nonatomic) IBOutlet UILabel *lbl_EventDetail;
@property (weak, nonatomic) IBOutlet UILabel *lbl_SpeakerName;
@property (weak, nonatomic) IBOutlet UILabel *lbl_SpeakerDetail;
@property (weak, nonatomic) IBOutlet UIImageView *img_speaker;
@property (weak, nonatomic) IBOutlet UIImageView *img_user;
@property (weak, nonatomic) IBOutlet UILabel *lbl_aboutofSpeaker;
@property (weak, nonatomic) IBOutlet UIImageView *img_location;
@property (weak, nonatomic) IBOutlet UILabel *lbl_sleepline;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lbl_EventDetailTopConstraints;
@property (weak, nonatomic) IBOutlet UIButton *btn_addtocalendar;

@end
