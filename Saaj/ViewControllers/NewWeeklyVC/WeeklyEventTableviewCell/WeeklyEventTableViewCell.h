//
//  WeeklyEventTableViewCell.h
//  Saaj
//
//  Created by mac  on 1/4/17.
//  Copyright © 2017 mac . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WeeklyEventTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *img_bg;
@property (weak, nonatomic) IBOutlet UILabel *lbl_date;
@property (weak, nonatomic) IBOutlet UILabel *lbl_EventTitle;
@property (weak, nonatomic) IBOutlet UILabel *lbl_day;
@property (weak, nonatomic) IBOutlet UILabel *lbl_EventDetail;
@property (weak, nonatomic) IBOutlet UILabel *lbl_datebg;

@end
