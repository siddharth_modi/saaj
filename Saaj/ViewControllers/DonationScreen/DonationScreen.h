//
//  DonationScreen.h
//  KSIJ Mumbai
//
//  Created by Kishan on 21/10/16.
//  Copyright © 2016 Latitudetechnolabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RKTabView.h>
#import "HomeScreen.h"
#import "ScheduleScreen.h"
#import "ProjectScreen.h"
#import "VideosScreen.h"
#import "ContactScreen.h"
#import "NotificationScreen.h"
@interface DonationScreen : UIViewController
{
    
}
@property (strong, nonatomic) IBOutlet UIView *ViewSub;
@property (strong, nonatomic) IBOutlet UIButton *btnDonateNow;
@property (strong, nonatomic) IBOutlet UIButton *btnAppeal;

- (IBAction)btnDonateNow:(id)sender;
- (IBAction)btnAppeal:(id)sender;
- (IBAction)btnNoti:(id)sender;

@end
