//
//  ProjectScreen.m
//  KSIJ Mumbai
//
//  Created by Kishan on 21/10/16.
//  Copyright © 2016 Latitudetechnolabs. All rights reserved.
//

#import "ProjectScreen.h"
#import "MainHomeViewController.h"

@interface ProjectScreen ()<RKTabViewDelegate,HTTPClientDeleagte>
{
     HTTPClient *ClientGetProjectList;
    NSMutableArray *ArrProjectList;
    
    RKTabItem *FirstTabItem,*SecondTabItem,*ThirdTabItem,*FourTabItem,*FiveTabItem,*SixTabItem,*HomeTabItem;
}
@end

@implementation ProjectScreen
@synthesize tblProjectList;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    HomeTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"home"] imageDisabled:[UIImage imageNamed:@"home"]];
    FirstTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabprayIconY"] imageDisabled:[UIImage imageNamed:@"TabprayIconW"]];
    
    SecondTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TdonateIconY"] imageDisabled:[UIImage imageNamed:@"TdonateIconW"]];
    
    ThirdTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcalendarIconY"] imageDisabled:[UIImage imageNamed:@"TabcalendarIconW"]];
    
    FourTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabprojectIconY"] imageDisabled:[UIImage imageNamed:@"TabprojectIconY"]];
    
    FourTabItem.tabState = TabStateEnabled;
    
    FiveTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabvideoIconW"] imageDisabled:[UIImage imageNamed:@"TabvideoIconW"]];
    
    SixTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcontactBookIconY"] imageDisabled:[UIImage imageNamed:@"TabcontactBookIconW"]];
    
    
    [self GetProjectList];
    
    self.tblProjectList.tableFooterView = [[UIView alloc] init];
    self.tblProjectList.backgroundColor=[UIColor clearColor];
}
-(void)viewDidAppear:(BOOL)animated
{
    RKTabView *tabView = [[RKTabView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-60,self.view.bounds.size.width, 60)];
    tabView.delegate = self;
    tabView.horizontalInsets = HorizontalEdgeInsetsMake(0, 0);;
    
    tabView.backgroundColor=[UIColor colorWithRed:0.43 green:0.63 blue:0.44 alpha:1];
    
    tabView.tabItems =@[HomeTabItem,FirstTabItem, SecondTabItem, ThirdTabItem, FourTabItem, FiveTabItem,  SixTabItem];
    
    [self.view addSubview:tabView];
}

#pragma mark -
#pragma mark - TabBar Delegate methods
- (void)tabView:(RKTabView *)tabView tabBecameEnabledAtIndex:(NSUInteger)index tab:(RKTabItem *)tabItem
{
    NSLog(@"Tab %tu enabled", index);
    
    if (index==0)
    {
        MainHomeViewController *vc=[[MainHomeViewController alloc]initWithNibName:@"MainHomeViewController" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }

    else if (index==1)
    {
        HomeScreen *vc=[[HomeScreen alloc]initWithNibName:@"HomeScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    else if (index==2)
    {
        DonationScreen *vc=[[DonationScreen alloc]initWithNibName:@"DonationScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    else if (index==3)
    {
        ScheduleScreen *vc=[[ScheduleScreen alloc]initWithNibName:@"ScheduleScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    else if (index==5)
    {
        NSString *str_channelId1=[[NSUserDefaults standardUserDefaults] objectForKey:@"Youtubelink"];
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.youtube.com/channel/%@",str_channelId1]]])
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.youtube.com/channel/%@",str_channelId1]]];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:@"Invalid url" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
            [alert show];
        }

    }
    else if (index==6)
    {
        ContactScreen *vc=[[ContactScreen alloc]initWithNibName:@"ContactScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
}
- (void)tabView:(RKTabView *)tabView tabBecameDisabledAtIndex:(NSUInteger)index tab:(RKTabItem *)tabItem
{
    NSLog(@"Tab %tu disabled", index);
}

#pragma mark -
#pragma mark - Button notification
- (IBAction)btnNoti:(id)sender
{
    NotificationScreen *vc=[[NotificationScreen alloc]initWithNibName:@"NotificationScreen" bundle:nil];
    [self.navigationController pushViewController:vc animated:NO];
}

#pragma mark -
#pragma mark - GetProjectList
-(void)GetProjectList
{
    [SVProgressHUD show];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];

    NSString *registerURL = [NSString stringWithFormat:@"%@get_projects_data_web.php",kAPIURL];
    ClientGetProjectList = [[HTTPClient alloc] init];
    ClientGetProjectList.delegate = self;
    [ClientGetProjectList getResponseFromAPI:registerURL andParameters:nil];
}
#pragma mark -
#pragma mark - HTTPClient Delegate methods
- (void)httpClient:(HTTPClient *)client didUpdateWithResponse:(id)response
{
   // NSLog(@"Response Project screen: %@",response);
    
    [SVProgressHUD dismiss];
    if (client== ClientGetProjectList)
    {
        ArrProjectList=[[NSMutableArray alloc]init];
        ArrProjectList=[response objectForKey:@"projects"];
        [tblProjectList reloadData];
    }
}

- (void)httpClient:(HTTPClient *)client didFailedWithError:(NSError *)error
{
    [SVProgressHUD dismiss];
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:error.localizedDescription delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

#pragma mark -
#pragma mark - Tableview Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return ArrProjectList.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"StatusCellIdentifier";
    ProjectListCell *cell = (ProjectListCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ProjectListCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.layer.borderWidth = 0.5;
    cell.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    cell.lblDate.text = [self changeYerFormate:[[ArrProjectList objectAtIndex:indexPath.row] objectForKey:@"project_date"]];
    cell.lblname.text = [[ArrProjectList objectAtIndex:indexPath.row] objectForKey:@"project_name"];
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ProjectDetailscreen *vc=[[ProjectDetailscreen alloc]initWithNibName:@"ProjectDetailscreen" bundle:nil];
    vc.ArrPassData=[ArrProjectList objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:vc animated:NO];
}
#pragma mark - Date changer Method
-(NSString *)changeYerFormate:(NSString *)dateis
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd";
    NSDate *date = [dateFormatter dateFromString:dateis];
    dateFormatter.dateFormat = @"dd MMMM yyyy";
    return [dateFormatter stringFromDate:date];
}

#pragma mark -
#pragma mark - didReceiveMemoryWarning
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
@end
