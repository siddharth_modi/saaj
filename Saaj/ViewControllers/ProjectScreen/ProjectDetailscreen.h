//
//  ProjectDetailscreen.h
//  KSIJ Mumbai
//
//  Created by Kishan on 24/10/16.
//  Copyright © 2016 Latitudetechnolabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RKTabView.h>
#import "HomeScreen.h"
#import "DonationScreen.h"
#import "ScheduleScreen.h"
#import "ProjectScreen.h"
#import "VideosScreen.h"
#import "ContactScreen.h"
#import "NotificationScreen.h"

@interface ProjectDetailscreen : UIViewController
{
    
}

@property(strong,nonatomic)NSMutableArray *ArrPassData;

@property (strong, nonatomic) IBOutlet UILabel *lbltitle;
@property (strong, nonatomic) IBOutlet UIImageView *imgdetail;
@property (strong, nonatomic) IBOutlet UIView *viewdetail;
@property (strong, nonatomic) IBOutlet UILabel *lbldate;
@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UITextView *txtDetailText;

- (IBAction)btnNoti:(id)sender;

@end
