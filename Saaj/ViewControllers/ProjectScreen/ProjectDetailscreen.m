//
//  ProjectDetailscreen.m
//  KSIJ Mumbai
//
//  Created by Kishan on 24/10/16.
//  Copyright © 2016 Latitudetechnolabs. All rights reserved.
//

#import "ProjectDetailscreen.h"
#import "MainHomeViewController.h"

@interface ProjectDetailscreen ()<RKTabViewDelegate>
{
    RKTabItem *FirstTabItem,*SecondTabItem,*ThirdTabItem,*FourTabItem,*FiveTabItem,*SixTabItem,*HomeTabItem;
    
}
@end

@implementation ProjectDetailscreen
@synthesize ArrPassData,lbltitle;
@synthesize viewdetail,lbldate,lblName,imgdetail,txtDetailText;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
  //  NSLog(@"Pass data:- %@",ArrPassData);
    
    HomeTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"home"] imageDisabled:[UIImage imageNamed:@"home"]];
    FirstTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabprayIconY"] imageDisabled:[UIImage imageNamed:@"TabprayIconW"]];
    
    SecondTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TdonateIconY"] imageDisabled:[UIImage imageNamed:@"TdonateIconW"]];
    
    ThirdTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcalendarIconY"] imageDisabled:[UIImage imageNamed:@"TabcalendarIconW"]];
    
    FourTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabprojectIconY"] imageDisabled:[UIImage imageNamed:@"TabprojectIconY"]];
    
   // FourTabItem.tabState = TabStateEnabled;
    
    FiveTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabvideoIconW"] imageDisabled:[UIImage imageNamed:@"TabvideoIconW"]];
    
    SixTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcontactBookIconY"] imageDisabled:[UIImage imageNamed:@"TabcontactBookIconW"]];
    
    [txtDetailText setEditable:NO];
}
-(void)viewDidAppear:(BOOL)animated
{
    RKTabView *tabView = [[RKTabView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-60,self.view.bounds.size.width, 60)];
    tabView.delegate = self;
    tabView.horizontalInsets = HorizontalEdgeInsetsMake(0, 0);;
    
    tabView.backgroundColor=[UIColor colorWithRed:0.43 green:0.63 blue:0.44 alpha:1];
    
    tabView.tabItems =@[HomeTabItem,FirstTabItem, SecondTabItem, ThirdTabItem, FourTabItem, FiveTabItem,  SixTabItem];
    
    [self.view addSubview:tabView];
}
-(void)viewWillAppear:(BOOL)animated
{
    viewdetail.layer.borderWidth = 1;
    viewdetail.layer.borderColor =[UIColor lightGrayColor].CGColor;
    
    lbltitle.text=[[ArrPassData valueForKey:@"project_name"] uppercaseString];
    
    lbldate.text=[ArrPassData valueForKey:@"show_project_date"];
    lblName.text=[ArrPassData valueForKey:@"project_name"];
    [lblName sizeToFit];

    txtDetailText.text=[ArrPassData valueForKey:@"project_details"];
  //  [txtDetailText sizeToFit];
    
    
    NSString *url1=[NSString stringWithFormat:@"%@",[ArrPassData valueForKey:@"image_path"]];
    [imgdetail setShowActivityIndicatorView:YES];
    [imgdetail setIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [imgdetail sd_setImageWithURL:[NSURL URLWithString:url1] placeholderImage:[UIImage imageNamed:@"no-image"]];
    imgdetail.contentMode=UIViewContentModeScaleAspectFit;
    //imgdetail.layer.borderWidth = 1;
    //imgdetail.layer.borderColor =[UIColor colorWithRed:0.43 green:0.63 blue:0.44 alpha:1].CGColor;
    //imgdetail.backgroundColor=[UIColor whiteColor];
    imgdetail.clipsToBounds=YES;
    
}

#pragma mark -
#pragma mark - TabBar Delegate methods
- (void)tabView:(RKTabView *)tabView tabBecameEnabledAtIndex:(NSUInteger)index tab:(RKTabItem *)tabItem
{
    NSLog(@"Tab %tu enabled", index);
    
    if (index==0)
    {
        MainHomeViewController *vc=[[MainHomeViewController alloc]initWithNibName:@"MainHomeViewController" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    if (index==1)
    {
        HomeScreen *vc=[[HomeScreen alloc]initWithNibName:@"HomeScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    else if (index==2)
    {
        DonationScreen *vc=[[DonationScreen alloc]initWithNibName:@"DonationScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    else if (index==3)
    {
        ScheduleScreen *vc=[[ScheduleScreen alloc]initWithNibName:@"ScheduleScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    else if (index==4)
    {
        ProjectScreen *vc=[[ProjectScreen alloc]initWithNibName:@"ProjectScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    else if (index==5)
    {
        VideosScreen *vc=[[VideosScreen alloc]initWithNibName:@"VideosScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    else if (index==6)
    {
        ContactScreen *vc=[[ContactScreen alloc]initWithNibName:@"ContactScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
}
- (void)tabView:(RKTabView *)tabView tabBecameDisabledAtIndex:(NSUInteger)index tab:(RKTabItem *)tabItem
{
    NSLog(@"Tab %tu disabled", index);
}
- (IBAction)btnNoti:(id)sender
{
    NotificationScreen *vc=[[NotificationScreen alloc]initWithNibName:@"NotificationScreen" bundle:nil];
    [self.navigationController pushViewController:vc animated:NO];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}




@end
