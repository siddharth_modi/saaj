//
//  ContactScreen.m
//  KSIJ Mumbai
//
//  Created by Kishan on 21/10/16.
//  Copyright © 2016 Latitudetechnolabs. All rights reserved.
//

#import "ContactScreen.h"
#import "MainHomeViewController.h"
#import "NewWeeklyViewController.h"
@interface ContactScreen ()<RKTabViewDelegate,HTTPClientDeleagte>
{
    HTTPClient *ClientContactList,*ClientNewslater;
    NSMutableArray *ArrAddressList;
    NSMutableArray *ArrContactList;
    NSString *str_donationlink;
    
    RKTabItem *FirstTabItem,*SecondTabItem,*ThirdTabItem,*FourTabItem,*FiveTabItem,*SixTabItem,*HomeTabItem;
    
}
@end

@implementation ContactScreen

@synthesize viewSub;
@synthesize tblContactList,lblContact;
@synthesize ViewFutter;
@synthesize viewSocial,viewSubscribe,btnSend;
@synthesize txtMail,txtMobileNo;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    HomeTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"home"] imageDisabled:[UIImage imageNamed:@"home"]];
    HomeTabItem.titleString=@"Home";
    HomeTabItem.titleFontColor=[UIColor whiteColor];
    HomeTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];
    
    FirstTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabprayIconY"] imageDisabled:[UIImage imageNamed:@"TabprayIconW"]];
    FirstTabItem.titleString=@"Prayer Times";
    FirstTabItem.titleFontColor=[UIColor whiteColor];
    FirstTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];
    
    ThirdTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcalendarIconY"] imageDisabled:[UIImage imageNamed:@"TabcalendarIconW"]];
    ThirdTabItem.titleString=@"Calendar";
    ThirdTabItem.titleFontColor=[UIColor whiteColor];
    ThirdTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];
    
    FiveTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabvideoIconW"] imageDisabled:[UIImage imageNamed:@"TabvideoIconW"]];
    FiveTabItem.titleString=@"Media";
    FiveTabItem.titleFontColor=[UIColor whiteColor];
    FiveTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];
    
    SixTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcontactBookIconY"] imageDisabled:[UIImage imageNamed:@"TabcontactBookIconY"]];
    SixTabItem.titleString=@"Contact";
    SixTabItem.titleFontColor=[UIColor whiteColor];
    SixTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];

    
    SixTabItem.tabState = TabStateEnabled;
    
    viewSocial.layer.borderWidth = 1;
    viewSocial.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    viewSubscribe.layer.borderWidth = 1;
    viewSubscribe.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    btnSend.clipsToBounds = YES;
    btnSend.layer.cornerRadius =5;
    
//    txtMail.layer.shadowColor = [UIColor darkGrayColor].CGColor;
//    txtMail.layer.shadowOffset = CGSizeMake(0, 1);
//    txtMail.layer.cornerRadius=3;
//    txtMail.layer.borderWidth=0.5;
//    txtMail.layer.borderColor=[UIColor lightGrayColor].CGColor;
//    txtMail.layer.shadowOpacity = 1;
//    txtMail.layer.shadowRadius = 1.0;
//    
//    txtMobileNo.layer.shadowColor = [UIColor darkGrayColor].CGColor;
//    txtMobileNo.layer.shadowOffset = CGSizeMake(0, 1);
//    txtMobileNo.layer.cornerRadius=3;
//    txtMobileNo.layer.borderWidth=0.5;
//    txtMobileNo.layer.borderColor=[UIColor lightGrayColor].CGColor;
//    txtMobileNo.layer.shadowOpacity = 1;
//    txtMobileNo.layer.shadowRadius = 1.0;
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:)name:UIKeyboardWillShowNotification object:nil];
//    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:)name:UIKeyboardWillHideNotification object:nil];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    RKTabView *tabView = [[RKTabView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-60,self.view.bounds.size.width, 60)];
    tabView.delegate = self;
    tabView.horizontalInsets = HorizontalEdgeInsetsMake(0, 0);;
    
    tabView.backgroundColor=[UIColor colorWithRed:(48.0f/255.0f) green:(82.0f/255.0f) blue:(1.0f/255.0f) alpha:1];
    
    tabView.tabItems =@[HomeTabItem,FirstTabItem, ThirdTabItem, FiveTabItem,  SixTabItem];
    
    [self.view addSubview:tabView];
    
    
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [self GetContactDeatil];
}
#pragma mark -
#pragma mark - TabBar Delegate methods
- (void)tabView:(RKTabView *)tabView tabBecameEnabledAtIndex:(NSUInteger)index tab:(RKTabItem *)tabItem
{
    NSLog(@"Tab %tu enabled", index);
    if (index==0)
    {
        MainHomeViewController *vc=[[MainHomeViewController alloc]initWithNibName:@"MainHomeViewController" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    if (index==1)
    {
        HomeScreen *vc=[[HomeScreen alloc]initWithNibName:@"HomeScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    else if (index==2)
    {
        NewWeeklyViewController *vc=[[NewWeeklyViewController alloc]initWithNibName:@"NewWeeklyViewController" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    else if (index==3)
    {
        NSString *str_channelId1=[[NSUserDefaults standardUserDefaults] objectForKey:@"Youtubelink"];
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.youtube.com/channel/%@",str_channelId1]]])
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.youtube.com/channel/%@",str_channelId1]]];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:@"Invalid url" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
            [alert show];
        }
        [self setagain_click];
    }
   
}
- (void)tabView:(RKTabView *)tabView tabBecameDisabledAtIndex:(NSUInteger)index tab:(RKTabItem *)tabItem
{
    NSLog(@"Tab %tu disabled", index);
}

#pragma mark - Button notification
- (IBAction)btnNoti:(id)sender
{
    NotificationScreen *vc=[[NotificationScreen alloc]initWithNibName:@"NotificationScreen" bundle:nil];
    [self.navigationController pushViewController:vc animated:NO];
}

- (IBAction)btnSend:(id)sender
{
    if ([txtMail.text isEqualToString:@""])
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:@"Please enter email Address." delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if (![self validateEmail:[txtMail text]])
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:@"Please Enter Valid Email Address !" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [alert show];
        txtMail.text = @"";
        [txtMail becomeFirstResponder];
    }
    else if ([txtMobileNo.text isEqualToString:@""])
    {
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:@"Please enter Mobile No." delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if (txtMobileNo.text.length<10)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:@"Mobile No should be 10 digits." delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [alert show];
        
    }
    else
    {
         [SVProgressHUD show];
        NSDictionary *parameters = @{
                                     @"email" :txtMail.text,
                                     @"tel":txtMobileNo.text
                                     };
        
        
        NSString *registerURL = [NSString stringWithFormat:@"%@newsletters_web.php",kAPIURL];
        ClientNewslater = [[HTTPClient alloc] init];
        ClientNewslater.delegate = self;
        [ClientNewslater getResponseFromAPI:registerURL andParameters:parameters];
        
    }
}

- (IBAction)btnFb:(id)sender
{
    [self.view endEditing:YES];
    
    NSString *strFblink=[ArrContactList valueForKey:@"facebook_link"];
    
    NSString *link=[NSString stringWithFormat:@"http://%@",strFblink];
    
    [[UIApplication sharedApplication] openURL:[NSURL
                                                URLWithString:link]];
}
- (IBAction)btnTwetter:(id)sender
{
    [self.view endEditing:YES];
    
    NSString *strTwetterlink=[ArrContactList valueForKey:@"twitter_link"];
    
    NSString *link=[NSString stringWithFormat:@"http://%@",strTwetterlink];
    
    NSURL *url=[NSURL URLWithString:link];
    
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}
- (IBAction)btnyoutube:(id)sender
{
    [self.view endEditing:YES];
    
    NSString *stryoutubelink=[ArrContactList valueForKey:@"youtube_link"];
    
    NSString *link=[NSString stringWithFormat:@"http://%@",stryoutubelink];
    
    [[UIApplication sharedApplication] openURL:[NSURL
                                                URLWithString:link]];
}

#pragma mark - GetContactDeatil
-(void)GetContactDeatil
{
    [SVProgressHUD show];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];

    NSString *registerURL = [NSString stringWithFormat:@"%@setting_web.php",kAPIURL];
    ClientContactList = [[HTTPClient alloc] init];
    ClientContactList.delegate = self;
    [ClientContactList getResponseFromAPI:registerURL andParameters:nil];
}
#pragma mark -
#pragma mark - HTTPClient Delegate methods
- (void)httpClient:(HTTPClient *)client didUpdateWithResponse:(id)response
{
    //NSLog(@"Response Project screen: %@",response);
    
    [SVProgressHUD dismiss];
    if (client== ClientContactList)
    {
        ArrAddressList=[[NSMutableArray alloc]init];
        ArrContactList=[[NSMutableArray alloc]init];
        
        ArrAddressList=[[response objectForKey:@"contact_us"] objectForKey:@"address_list"];
        ArrContactList=[response objectForKey:@"contact_us"];
        str_donationlink=[[response objectForKey:@"setting"]valueForKey:@"donation_page_link"];
        
        
        
        NSString *str_Facebook=[ArrContactList valueForKey:@"facebook_link"];
        NSString *str_Twitter=[ArrContactList valueForKey:@"twitter_link"];
        NSString *str_Youtube=[ArrContactList valueForKey:@"youtube_link"];
        
//        str_Facebook=@"";
//        str_Twitter=@"twitte";
//        str_Youtube=@"";
        
        if (![str_Facebook isEqualToString:@""] && ![str_Twitter isEqualToString:@""] && ![str_Youtube isEqualToString:@""])
        {
            NSLog(@"Nohidden");
            self.tblContactList.tableFooterView=ViewFutter;
            [_btn_facebook addTarget:self action:@selector(ShowFacebookView:) forControlEvents:UIControlEventTouchUpInside];

            [_btn_Twitter addTarget:self action:@selector(ShowTWitterView:) forControlEvents:UIControlEventTouchUpInside];

            [_btn_Youtube addTarget:self action:@selector(ShoYouTubeView:) forControlEvents:UIControlEventTouchUpInside];

            _view_2icon.hidden=YES;
        }
        else if (![str_Facebook isEqualToString:@""] && ![str_Twitter isEqualToString:@""] && [str_Youtube isEqualToString:@""])
        {
             _view_2icon.hidden=NO;
            self.tblContactList.tableFooterView=ViewFutter;
            
            [_btn_View2btn1 setImage:[UIImage imageNamed:@"fbIcon"] forState:UIControlStateNormal];
            [_btn_View2btn1 addTarget:self action:@selector(ShowFacebookView:) forControlEvents:UIControlEventTouchUpInside];
            
            [_btn_view2btn2 setImage:[UIImage imageNamed:@"twitterIcon"] forState:UIControlStateNormal];
            [_btn_view2btn2 addTarget:self action:@selector(ShowTWitterView:) forControlEvents:UIControlEventTouchUpInside];
            
            NSLog(@"Show facebook  twitter ");
        }
        else if (![str_Facebook isEqualToString:@""] && [str_Twitter isEqualToString:@""] && ![str_Youtube isEqualToString:@""])
        {
             _view_2icon.hidden=NO;
            NSLog(@"Show facebook  youtube");
            
            self.tblContactList.tableFooterView=ViewFutter;
            
            [_btn_View2btn1 setImage:[UIImage imageNamed:@"fbIcon"] forState:UIControlStateNormal];
            [_btn_View2btn1 addTarget:self action:@selector(ShowFacebookView:) forControlEvents:UIControlEventTouchUpInside];
            
            [_btn_view2btn2 setImage:[UIImage imageNamed:@"youtubeIcon"] forState:UIControlStateNormal];
            [_btn_view2btn2 addTarget:self action:@selector(ShoYouTubeView:) forControlEvents:UIControlEventTouchUpInside];
            
        }
        else if ([str_Facebook isEqualToString:@""] && ![str_Twitter isEqualToString:@""] && ![str_Youtube isEqualToString:@""])
        {
             _view_2icon.hidden=NO;
            self.tblContactList.tableFooterView=ViewFutter;
            
             NSLog(@"Show Twitter  youtube");
            [_btn_View2btn1 setImage:[UIImage imageNamed:@"twitterIcon"] forState:UIControlStateNormal];
            [_btn_View2btn1 addTarget:self action:@selector(ShowTWitterView:) forControlEvents:UIControlEventTouchUpInside];
            
            [_btn_view2btn2 setImage:[UIImage imageNamed:@"youtubeIcon"] forState:UIControlStateNormal];
            [_btn_view2btn2 addTarget:self action:@selector(ShoYouTubeView:) forControlEvents:UIControlEventTouchUpInside];
        }
        else if ([str_Facebook isEqualToString:@""] && [str_Twitter isEqualToString:@""] && ![str_Youtube isEqualToString:@""])
        {
            NSLog(@"Show Only YouTube");
            self.tblContactList.tableFooterView=ViewFutter;
             _view_2icon.hidden=YES;
            
            [_btn_Twitter setImage:[UIImage imageNamed:@"youtubeIcon"] forState:UIControlStateNormal];
            [_btn_Twitter addTarget:self action:@selector(ShoYouTubeView:) forControlEvents:UIControlEventTouchUpInside];
            _btn_Youtube.hidden=YES;
            _btn_facebook.hidden=YES;
        }
        else if (![str_Facebook isEqualToString:@""] && [str_Twitter isEqualToString:@""] && [str_Youtube isEqualToString:@""])
        {
            NSLog(@"Show Only Facebook button");
            self.tblContactList.tableFooterView=ViewFutter;
            [_btn_Twitter setImage:[UIImage imageNamed:@"fbIcon"] forState:UIControlStateNormal];
            [_btn_Twitter addTarget:self action:@selector(ShowFacebookView:) forControlEvents:UIControlEventTouchUpInside];
            _btn_Youtube.hidden=YES;
            _btn_facebook.hidden=YES;
            _view_2icon.hidden=YES;
        }
        else if ([str_Facebook isEqualToString:@""] && ![str_Twitter isEqualToString:@""] && [str_Youtube isEqualToString:@""])
        {
            NSLog(@"Show Only Twitter button");
            self.tblContactList.tableFooterView=ViewFutter;
            [_btn_Twitter setImage:[UIImage imageNamed:@"twitterIcon"] forState:UIControlStateNormal];
            [_btn_Twitter addTarget:self action:@selector(ShowTWitterView:) forControlEvents:UIControlEventTouchUpInside];
            _view_2icon.hidden=YES;
            _btn_Youtube.hidden=YES;
            _btn_facebook.hidden=YES;
        }
        else
        {
            NSLog(@"View Hidden");
        }
        
        
        
//        if (![str_Facebook isEqualToString:@""])
//        {
//            _btn_facebook.hidden=NO;
//             self.tblContactList.tableFooterView=ViewFutter;
//            if ([str_Twitter isEqualToString:@""])
//            {
//                _btn_Twitter.hidden=YES;
//            }
//            else
//            {
//                
//                 _btn_Twitter.hidden=NO;
//            }
//            
//            if ([str_Youtube isEqualToString:@""])
//            {
//                _btn_Youtube.hidden=YES;
//            }
//            else
//            {
//                _btn_Youtube.hidden=NO;
//            }
//        }
//        else
//        {
//            _btn_facebook.hidden=YES;
//        }
//        
//        if (![str_Twitter isEqualToString:@""])
//        {
//            _btn_Twitter.hidden=NO;
//             self.tblContactList.tableFooterView=ViewFutter;
//            if ([str_Facebook isEqualToString:@""])
//            {
//                _btn_facebook.hidden=YES;
//            }
//            else
//            {
//                _btn_facebook.hidden=NO;
//            }
//            
//            if ([str_Youtube isEqualToString:@""])
//            {
//                _btn_Youtube.hidden=YES;
//                
//            }
//            else
//            {
//               
//                _btn_Youtube.hidden=NO;
//            }
//
//        }
//        else
//        {
//            _btn_Twitter.hidden=YES;
//        }
//        
//
//        if (![str_Youtube isEqualToString:@""])
//        {
//            _btn_Youtube.hidden=NO;
//             self.tblContactList.tableFooterView=ViewFutter;
//            
//            if ([str_Facebook isEqualToString:@""])
//            {
//                _btn_facebook.hidden=YES;
//            }
//            else
//            {
//                _btn_facebook.hidden=NO;
//            }
//            
//            if ([str_Twitter isEqualToString:@""])
//            {
//                _btn_Twitter.hidden=YES;
//            }
//            else
//            {
//                _btn_Twitter.hidden=NO;
//            }
//        }
//        else
//        {
//            _btn_Youtube.hidden=YES;
//        }
//        if (![str_Facebook isEqualToString:@""] || [str_Twitter isEqualToString:@""] || [str_Youtube isEqualToString:@""])
//        {
//            self.tblContactList.tableFooterView=ViewFutter;
//            _btn_facebook.hidden=NO;
//            _btn_Twitter.hidden=YES;
//            _btn_Youtube.hidden=YES;
//        }
//        else if ([str_Facebook isEqualToString:@""] || ![str_Twitter isEqualToString:@""]  ||[str_Youtube isEqualToString:@""])
//        {
//            self.tblContactList.tableFooterView=ViewFutter;
//            _btn_facebook.hidden=YES;
//            _btn_Twitter.hidden=NO;
//            _btn_Youtube.hidden=YES;
//
//        }
//        else if ([str_Facebook isEqualToString:@""] && [str_Twitter isEqualToString:@""] && ![str_Youtube isEqualToString:@""])
//        {
//            self.tblContactList.tableFooterView=ViewFutter;
//            _btn_facebook.hidden=YES;
//            _btn_Twitter.hidden=YES;
//            _btn_Youtube.hidden=NO;
//        }
//        else if (![str_Facebook isEqualToString:@""] && ![str_Twitter isEqualToString:@""] &&[str_Youtube isEqualToString:@""])
//        {
//            self.tblContactList.tableFooterView=ViewFutter;
//            _btn_facebook.hidden=NO;
//            _btn_Twitter.hidden=NO;
//            _btn_Youtube.hidden=YES;
//        }
//        else if ([str_Facebook isEqualToString:@""] && ![str_Twitter isEqualToString:@""] && ![str_Youtube isEqualToString:@""])
//        {
//            self.tblContactList.tableFooterView=ViewFutter;
//            _btn_facebook.hidden=YES;
//            _btn_Twitter.hidden=NO;
//            _btn_Youtube.hidden=NO;
//        }
//        else if (![str_Facebook isEqualToString:@""] && [str_Twitter isEqualToString:@""] && ![str_Youtube isEqualToString:@""])
//        {
//            self.tblContactList.tableFooterView=ViewFutter;
//            _btn_facebook.hidden=NO;
//            _btn_Twitter.hidden=YES;
//            _btn_Youtube.hidden=NO;
//        }
//        else if ([str_Facebook isEqualToString:@""] && [str_Twitter isEqualToString:@""] && [str_Youtube isEqualToString:@""])
//        {
////            self.tblContactList.tableFooterView=ViewFutter;
////            _btn_facebook.hidden=NO;
////            _btn_Twitter.hidden=YES;
////            _btn_Youtube.hidden=NO;
//        }
        

        
        NSLog(@" donation link %@",str_donationlink);
        [self SetFramTableview];
        [tblContactList reloadData];
        
        self.tblContactList.tableHeaderView = _view_header;
        
        _btn_Donate.layer.cornerRadius=5;
        _btn_Donate.layer.masksToBounds=YES;
        
    }
    else if (client== ClientNewslater)
    {
        NSString *Code=[NSString stringWithFormat:@"%@",[response objectForKey:@"success"]];
        
        if ([Code isEqualToString:@"1"])
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:[response objectForKey:@"msg"] delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
            [alert show];
            
            txtMail.text=@"";
            txtMobileNo.text=@"";
        }
        else if ([Code isEqualToString:@"0"])
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:[response objectForKey:@"msg"] delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
}
-(void)ShowTWitterView:(UIButton *)sender
{
    [self.view endEditing:YES];
    
    NSString *strTwetterlink=[ArrContactList valueForKey:@"twitter_link"];
    
    NSString *link=[NSString stringWithFormat:@"http://%@",strTwetterlink];
    
    NSURL *url=[NSURL URLWithString:link];
    
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }

}
-(void)ShoYouTubeView:(UIButton *)sender
{
    [self.view endEditing:YES];
    
    NSString *stryoutubelink=[ArrContactList valueForKey:@"youtube_link"];
    
    NSString *link=[NSString stringWithFormat:@"http://%@",stryoutubelink];
    
    [[UIApplication sharedApplication] openURL:[NSURL
                                                URLWithString:link]];
}
-(void)ShowFacebookView:(UIButton *)sender
{
    [self.view endEditing:YES];
    
    NSString *strFblink=[ArrContactList valueForKey:@"facebook_link"];
    
    NSString *link=[NSString stringWithFormat:@"http://%@",strFblink];
    
    [[UIApplication sharedApplication] openURL:[NSURL
                                                URLWithString:link]];

}
- (void)httpClient:(HTTPClient *)client didFailedWithError:(NSError *)error
{
    [SVProgressHUD dismiss];
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:error.localizedDescription delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

#pragma mark - SetFramTableview
-(void)SetFramTableview
{
    CGRect currentFrame = tblContactList.frame;
    currentFrame.origin.x=0;
    currentFrame.origin.y=0;
    currentFrame.size.width=viewSub.frame.size.width;
    currentFrame.size.height =viewSub.frame.size.height-10;
    tblContactList.frame = currentFrame;

}

#pragma mark - Tableview Delegate Method

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return ArrAddressList.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ContactListCell *cell=(ContactListCell *)[tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ContactListCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.layer.borderWidth = 1;
    cell.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    
    if ([[[ArrAddressList objectAtIndex:indexPath.section] objectForKey:@"phone_2"] isEqualToString:@""])
    {
        [cell.p2 setHidden:YES];

        CGRect FrmImgPhone=cell.imgPhone.frame;
        FrmImgPhone.origin.y=(cell.p1.frame.origin.y+cell.p1.frame.size.height/2)-(cell.imgPhone.frame.size.height/2);
        cell.imgPhone.frame=FrmImgPhone;
        
        CGRect Frmbtnmail=cell.btnMail.frame;
        Frmbtnmail.origin.y=cell.p1.frame.origin.y+cell.p1.frame.size.height+5;
        cell.btnMail.frame=Frmbtnmail;
        
        CGRect FrmImgmail=cell.imgMail.frame;
        FrmImgmail.origin.y=cell.btnMail.frame.origin.y+5;
        cell.imgMail.frame=FrmImgmail;
        
    
        CGRect FrmbtnWeb=cell.btnWeb.frame;
        FrmbtnWeb.origin.y=cell.btnMail.frame.origin.y+cell.btnMail.frame.size.height+5;
        cell.btnWeb.frame=FrmbtnWeb;
        
        CGRect FrmImgWeb=cell.imgWeb.frame;
        FrmImgWeb.origin.y=cell.btnWeb.frame.origin.y+5;
        cell.imgWeb.frame=FrmImgWeb;

    }
    if ([[[ArrAddressList objectAtIndex:indexPath.section] objectForKey:@"phone_3"] isEqualToString:@""])
    {
        [cell.p3 setHidden:YES];
    }
    cell.lblPhone1Title.text=[[ArrAddressList objectAtIndex:indexPath.section] objectForKey:@"phone_1_heading"];
    cell.lblPhone2title.text=[[ArrAddressList objectAtIndex:indexPath.section] objectForKey:@"phone_2_heading"];
    cell.lbladdress.text=[[ArrAddressList objectAtIndex:indexPath.section] objectForKey:@"address"];
    
 
    [cell.btn_Address addTarget:self action:@selector(BtnAddressClicked:) forControlEvents:UIControlEventTouchUpInside];
    cell.btn_Address.tag=indexPath.section;
    
    [cell.btnphone1 setTitle: [[ArrAddressList objectAtIndex:indexPath.section] objectForKey:@"phone_1"] forState: UIControlStateNormal];
    cell.btnphone1.tag=indexPath.section;
   // [cell.btnphone1 sizeToFit];
    [cell.btnphone1 addTarget:self action:@selector(BtnPhone1Clicked:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    [cell.btnPhone2 setTitle: [[ArrAddressList objectAtIndex:indexPath.section] objectForKey:@"phone_2"] forState: UIControlStateNormal];
    cell.btnPhone2.tag=indexPath.section;
   // [cell.btnPhone2 sizeToFit];
    [cell.btnPhone2 addTarget:self action:@selector(BtnPhone2Clicked:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [cell.btnFax setTitle: [[ArrAddressList objectAtIndex:indexPath.section] objectForKey:@"phone_3"] forState: UIControlStateNormal];
    cell.btnFax.tag=indexPath.section;
    //[cell.btnFax sizeToFit];
    [cell.btnFax addTarget:self action:@selector(BtnFaxClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [cell.btnMail setTitle: [[ArrAddressList objectAtIndex:indexPath.section] objectForKey:@"email"] forState: UIControlStateNormal];
    cell.btnMail.tag=indexPath.section;
   // [cell.btnMail sizeToFit];
    [cell.btnMail addTarget:self action:@selector(BtnMailClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [cell.btnWeb setTitle: [[ArrAddressList objectAtIndex:indexPath.section] objectForKey:@"website"] forState: UIControlStateNormal];
    cell.btnWeb.tag=indexPath.section;
   // [cell.btnWeb sizeToFit];
    [cell.btnWeb addTarget:self action:@selector(BtnWebClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([[[ArrAddressList objectAtIndex:indexPath.section] objectForKey:@"phone_2"] isEqualToString:@""])
    {
        return 226;
    }
    else
    {
       return 330;
    }
}

#pragma mark - Button Clicked Event

-(void)BtnAddressClicked:(UIButton *)sender
{
    [self.view endEditing:YES];
    
    NSLog(@"BtnAddress Clicked %ld",(long)sender.tag);
    
     NSMutableArray *arr_setting=[[NSUserDefaults standardUserDefaults] objectForKey:@"Address"];
    
    NSMutableArray *array_main=[[arr_setting valueForKey:@"contact_us"] objectForKey:@"address_list"];
    NSString *str_latitude=[[array_main objectAtIndex:0] valueForKey:@"latitude"];
    NSString *str_longitude=[[array_main objectAtIndex:0] valueForKey:@"longitude"];
    
    NSLog(@"%@",str_latitude);
    

    
    
    
    NSString *mapURLStr = [NSString stringWithFormat:@"http://maps.apple.com/?address=%@",[[array_main objectAtIndex:0] valueForKey:@"address"]];
    mapURLStr = [mapURLStr stringByReplacingOccurrencesOfString:@" " withString:@"+"];
//    NSURL *url = [NSURL URLWithString:[mapURLStr stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
//    if ([[UIApplication sharedApplication] canOpenURL:url])
//    {
//        [[UIApplication sharedApplication] openURL:url];
//    }
    
 //   CLLocationCoordinate2D empireStateLocation = CLLocationCoordinate2DMake(str_latitude.floatValue, str_longitude.floatValue);
    
     NSString *queryString = [NSString stringWithFormat:@"%@",mapURLStr];
    //NSString *queryString = [NSString stringWithFormat:@"http://maps.apple.com/?ll=%@,%@",str_latitude,str_longitude];
//
    queryString=[queryString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    queryString=[queryString stringByReplacingOccurrencesOfString:@"\r" withString:@""];

    NSURL *url = [NSURL URLWithString:queryString];
    
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}
-(void)BtnPhone1Clicked:(UIButton *)sender
{
    [self.view endEditing:YES];
    NSLog(@"BtnPhone1 Clicked %ld",(long)sender.tag);
    
    NSString *Mno=[NSString stringWithFormat:@"telprompt://%@",[[ArrAddressList objectAtIndex:sender.tag]objectForKey:@"phone_1"]];
    Mno=[Mno stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:Mno]];
}

-(void)BtnPhone2Clicked:(UIButton *)sender
{
    [self.view endEditing:YES];
    NSLog(@"BtnPhone2 Clicked %ld",(long)sender.tag);
    
    NSString *Mno=[NSString stringWithFormat:@"telprompt://%@",[[ArrAddressList objectAtIndex:sender.tag]objectForKey:@"phone_2"]];
    
    Mno=[Mno stringByReplacingOccurrencesOfString:@" " withString:@""];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:Mno]];
    
}
-(void)BtnFaxClicked:(UIButton *)sender
{
    [self.view endEditing:YES];
    NSLog(@"BtnFax Clicked %ld",(long)sender.tag);
}

-(void)BtnMailClicked:(UIButton *)sender
{
    [self.view endEditing:YES];
    NSLog(@"BtnMail Clicked %ld",(long)sender.tag);
    
    NSString *EmailId=[[ArrAddressList objectAtIndex:sender.tag] objectForKey:@"email"];
    
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        mailer.mailComposeDelegate = self;
        NSArray *toRecipients = [NSArray arrayWithObjects:EmailId, nil];
        [mailer setToRecipients:toRecipients];
        
        [self presentViewController:mailer animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:@"Your device doesn't support." delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [alert show];
    }
}
-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    UIAlertView *alert;
    switch (result)
    {
        case MFMailComposeResultCancelled:
            
            alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:@"Mail canceled" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
            [alert show];
            break;
        case MFMailComposeResultSaved:
            
            alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:@"Mail saved" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
            [alert show];
            break;
        case MFMailComposeResultSent:
            
            alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:@"Mail send" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
            [alert show];
            break;
        case MFMailComposeResultFailed:
            
            alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:@"Mail failed" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
            [alert show];
            break;
        default:
            
            alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:@"Mail not sent" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
            [alert show];
            break;
    }
    [self dismissViewControllerAnimated:NO completion:nil];
}
-(void)BtnWebClicked:(UIButton *)sender
{
    [self.view endEditing:YES];
    NSLog(@"BtnWeb Clicked %ld",(long)sender.tag);
    
    NSString *strweb=[NSString stringWithFormat:@"http://%@",[[ArrAddressList objectAtIndex:sender.tag] objectForKey:@"website"]];
    
    strweb=[strweb stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSURL *url=[NSURL URLWithString:strweb];
    
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:@"Invalid url" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        
        [alert show];
    }
}
#pragma mark - TextField Delegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    self.activeTextField = textField;
    
    return YES;
    
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    
    self.activeTextField = nil;
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}
#pragma mark-
#pragma mark Keyboard Mange Method
- (void)keyboardWasShown:(NSNotification *)notification
{
   [tblContactList setContentSize:CGSizeMake(self.view.frame.size.width, self.tblContactList.frame.size.height)];
    NSDictionary* info = [notification userInfo];
    
    CGRect kbRect = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    
    kbRect = [self.view convertRect:kbRect fromView:nil];
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbRect.size.height+10, 0.0);
    
    self.tblContactList.contentInset = contentInsets;
    
    self.tblContactList.scrollIndicatorInsets = contentInsets;
    
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbRect.size.height;
    [self.tblContactList scrollRectToVisible:self.activeTextField.frame animated:YES];
}
- (void) keyboardWillHide:(NSNotification *)notification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    
    self.tblContactList.contentInset = contentInsets;
    
    self.tblContactList.scrollIndicatorInsets = UIEdgeInsetsZero;
    
    self.tblContactList.contentOffset = CGPointMake(0.0, 0.0);
    
    [self.view endEditing:YES];
}

#pragma mark email validation
- (BOOL)validateEmail:(NSString *)emailStr
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+]+@[A-Za-z0-9.]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailStr];
}
#pragma mark - didReceiveMemoryWarning
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
- (IBAction)btn_Donate:(id)sender
{
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:str_donationlink]])
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str_donationlink]];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:@"Invalid url" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [alert show];
    }
}
-(void)setagain_click
{
    HomeTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"home"] imageDisabled:[UIImage imageNamed:@"home"]];
    HomeTabItem.titleString=@"Home";
    HomeTabItem.titleFontColor=[UIColor whiteColor];
    HomeTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];
    
    FirstTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabprayIconY"] imageDisabled:[UIImage imageNamed:@"TabprayIconW"]];
    FirstTabItem.titleString=@"Prayer Times";
    FirstTabItem.titleFontColor=[UIColor whiteColor];
    FirstTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];
    
    ThirdTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcalendarIconY"] imageDisabled:[UIImage imageNamed:@"TabcalendarIconW"]];
    ThirdTabItem.titleString=@"Calendar";
    ThirdTabItem.titleFontColor=[UIColor whiteColor];
    ThirdTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];
    
    FiveTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabvideoIconW"] imageDisabled:[UIImage imageNamed:@"TabvideoIconW"]];
    FiveTabItem.titleString=@"Media";
    FiveTabItem.titleFontColor=[UIColor whiteColor];
    FiveTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];
    
    SixTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcontactBookIconY"] imageDisabled:[UIImage imageNamed:@"TabcontactBookIconY"]];
    SixTabItem.titleString=@"Contact";
    SixTabItem.titleFontColor=[UIColor whiteColor];
    SixTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];
    
    
    SixTabItem.tabState = TabStateEnabled;
    
    RKTabView *tabView = [[RKTabView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-60,self.view.bounds.size.width, 60)];
    tabView.delegate = self;
    tabView.horizontalInsets = HorizontalEdgeInsetsMake(0, 0);;
    
    tabView.backgroundColor=[UIColor colorWithRed:(48.0f/255.0f) green:(82.0f/255.0f) blue:(1.0f/255.0f) alpha:1];
    
    tabView.tabItems =@[HomeTabItem,FirstTabItem, ThirdTabItem, FiveTabItem,  SixTabItem];
    
    [self.view addSubview:tabView];

}
@end
