//
//  HomeScreen.m
//  KSIJ Mumbai
//
//  Created by Kishan on 20/10/16.
//  Copyright © 2016 Latitudetechnolabs. All rights reserved.
//

#import "HomeScreen.h"
#import "AppDelegate.h"
#import "HomeCell1.h"
#import "MainHomeViewController.h"
#import "NewWeeklyViewController.h"
@interface HomeScreen ()<RKTabViewDelegate,HTTPClientDeleagte>
{
    HTTPClient *ClientGetList,*ClientNoti;
    
    HTTPClient *bgFetchHTTPClient;
    NSMutableDictionary *DicServicesData;
    NSMutableArray *ArrTodayArt;
    NSMutableArray *ArrListAlarm;
    BOOL isTokonValid;
    RKTabItem *FirstTabItem,*SecondTabItem,*ThirdTabItem,*FourTabItem,*FiveTabItem,*SixTabItem,*HomeTabItem;
    
     NSMutableArray *ArrPreyerName;
}
@property (nonatomic, strong) HTTPClient *httpClient;

@end
@implementation HomeScreen

@synthesize tblListAlarm,ViewHeader,lbldayname,lblyearname1,lblYearName2;


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    [NSTimer scheduledTimerWithTimeInterval:5.0 target:self
                                   selector:@selector(printCounter) userInfo:nil repeats:YES];
    
    HomeTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"home"] imageDisabled:[UIImage imageNamed:@"home"]];
    HomeTabItem.titleString=@"Home";
    HomeTabItem.titleFontColor=[UIColor whiteColor];
    HomeTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];
    
    FirstTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabprayIconY"] imageDisabled:[UIImage imageNamed:@"TabprayIconY"]];
    FirstTabItem.titleString=@"Prayer Times";
    FirstTabItem.titleFontColor=[UIColor whiteColor];
    FirstTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];
    
    ThirdTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcalendarIconY"] imageDisabled:[UIImage imageNamed:@"TabcalendarIconW"]];
    ThirdTabItem.titleString=@"Calendar";
    ThirdTabItem.titleFontColor=[UIColor whiteColor];
    ThirdTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];
    
    FiveTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabvideoIconW"] imageDisabled:[UIImage imageNamed:@"TabvideoIconW"]];
    FiveTabItem.titleString=@"Media";
    FiveTabItem.titleFontColor=[UIColor whiteColor];
    FiveTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];
    
    SixTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcontactBookIconY"] imageDisabled:[UIImage imageNamed:@"TabcontactBookIconW"]];
    SixTabItem.titleString=@"Contact";
    SixTabItem.titleFontColor=[UIColor whiteColor];
    SixTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];
    
    FirstTabItem.tabState = TabStateEnabled;
    
  //  [self GetApiData];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(tablereloaddata) name:@"tablereload" object:nil];
   
    [[NSNotificationCenter defaultCenter]postNotificationName:@"tablereload" object:nil];

   //   [tblListAlarm reloadData];
      ArrPreyerName=[[NSMutableArray alloc]initWithObjects:@"Imsaak",@"Fajr",@"Sunrise",@"Zohr",@"Sunset",@"Maghrib",@"Midnight", nil];
    
    ViewHeader.layer.borderWidth = 1;
    ViewHeader.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.tblListAlarm.tableFooterView = [[UIView alloc] init];
    self.tblListAlarm.backgroundColor=[UIColor clearColor];
}
-(void)tablereloaddata
{
    ArrTodayArt=[[NSMutableArray alloc] init];
    DicServicesData=[[NSUserDefaults standardUserDefaults] objectForKey:@"PrayerTime"];
    
    ArrTodayArt=[[NSMutableArray alloc]init];
    ArrTodayArt=[[DicServicesData objectForKey:@"salat"] objectForKey:@"today"];
    
    self.tblListAlarm.tableHeaderView = ViewHeader;
    
    lbldayname.text=[self GetDayName:[ArrTodayArt valueForKey:@"prayer_date"]];
    lblyearname1.text=[self changeYerFormate:[ArrTodayArt valueForKey:@"prayer_date"]];
    
    NSString *d1=[ArrTodayArt valueForKey:@"hijri_day"];
    NSString *d2=[ArrTodayArt valueForKey:@"hijri_mnth_name"];
    NSString *d3=[ArrTodayArt valueForKey:@"hijri_year"];
    NSString *yersecond=[NSString stringWithFormat:@"%@ %@ %@",d1,d2,d3];
    lblYearName2.text=yersecond;
    [tblListAlarm reloadData];
}

#pragma mark - SetNotiFication

-(void)SetNotiFication
{
    [SVProgressHUD show];
    NSString* Identifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    
    NSString *divid=[[NSUserDefaults standardUserDefaults]valueForKey:@"DivID"];
    
    if (divid ==nil)
    {
        divid=@"1234-testing-deviciceID-40593960123";
    }
    NSDictionary *parameters = @{
                                 @"regID_ios" :divid,
                                 @"devicePlatform" :KDevicePlatform,
                                 @"deviceUUID": Identifier
                                 };
    
    NSString *registerURL = [NSString stringWithFormat:@"%@pushnotification_web.php",kAPIURL];
    ClientNoti = [[HTTPClient alloc] init];
    ClientNoti.delegate = self;
    [ClientNoti getResponseFromAPI:registerURL andParameters:parameters];
    
}
-(void)viewDidAppear:(BOOL)animated
{
    RKTabView *tabView = [[RKTabView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-60,self.view.bounds.size.width, 60)];
    tabView.delegate = self;
    tabView.horizontalInsets = HorizontalEdgeInsetsMake(0, 0);;
    
    tabView.backgroundColor=[UIColor colorWithRed:(48.0f/255.0f) green:(82.0f/255.0f) blue:(1.0f/255.0f) alpha:1];

    
    tabView.tabItems =@[HomeTabItem,FirstTabItem, ThirdTabItem, FiveTabItem,  SixTabItem];
    
    [self.view addSubview:tabView];
}
-(void)viewWillAppear:(BOOL)animated
{
    // [self SetNotiFication];
}

#pragma mark - RKTabViewDelegate
- (void)tabView:(RKTabView *)tabView tabBecameEnabledAtIndex:(NSUInteger)index tab:(RKTabItem *)tabItem
{
    NSLog(@"Tab %tu enabled", index);
    if (index==0)
    {
        MainHomeViewController *vc=[[MainHomeViewController alloc]initWithNibName:@"MainHomeViewController" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    if (index==2)
    {
        NewWeeklyViewController *vc=[[NewWeeklyViewController alloc]initWithNibName:@"NewWeeklyViewController" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    else if (index==3)
    {
         NSString *str_channelId1=[[NSUserDefaults standardUserDefaults] objectForKey:@"Youtubelink"];
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.youtube.com/channel/%@",str_channelId1]]])
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.youtube.com/channel/%@",str_channelId1]]];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:@"Invalid url" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
            [alert show];
        }
        [self setagain_click];
    }
    else if (index==4)
    {
        ContactScreen *vc=[[ContactScreen alloc]initWithNibName:@"ContactScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
        
    }
}
- (void)tabView:(RKTabView *)tabView tabBecameDisabledAtIndex:(NSUInteger)index tab:(RKTabItem *)tabItem
{
    //NSLog(@"Tab %tu disabled", index);
}

#pragma mark - Button Notification
- (IBAction)btnNoti:(id)sender
{
    NotificationScreen *vc=[[NotificationScreen alloc]initWithNibName:@"NotificationScreen" bundle:nil];
    [self.navigationController pushViewController:vc animated:NO];
}

- (IBAction)btnMail:(id)sender
{
    NSString *EmailId=@"kishan.r@latitudetechnolabs.com";
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        mailer.mailComposeDelegate = self;
        [mailer setSubject:@"HIC"];
        
        NSString *link = @"HIC App log file";
        NSString *messageBody =[NSString stringWithFormat:@"%@",link];
        //Attach the Crash Log..
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                             NSUserDomainMask, YES);NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *logPath = [documentsDirectory stringByAppendingPathComponent:@"Logfile.txt"];
        NSData *myData = [NSData dataWithContentsOfFile:logPath];
        [mailer addAttachmentData:myData mimeType:@"text/plain" fileName:@"Logfile.txt"];
        [mailer setMessageBody:messageBody isHTML:NO];
        
        
        
        NSArray *toRecipients = [NSArray arrayWithObjects:EmailId, nil];
        [mailer setToRecipients:toRecipients];
        
        [self presentViewController:mailer animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:@"Your device doesn't support." delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    
}
-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    UIAlertView *alert;
    switch (result)
    {
        case MFMailComposeResultCancelled:
            
            alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:@"Mail canceled" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
            [alert show];
            break;
        case MFMailComposeResultSaved:
            
            alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:@"Mail saved" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
            [alert show];
            break;
        case MFMailComposeResultSent:
            
            alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:@"Mail send" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
            [alert show];
            
            [self removeFile];
            //[SHARED_APPDELEGATE redirectConsoleLogToDocumentFolder];
            
            break;
        case MFMailComposeResultFailed:
            
            alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:@"Mail failed" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
            [alert show];
            break;
        default:
            
            alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:@"Mail not sent" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
            [alert show];
            break;
    }
    
    [self dismissViewControllerAnimated:NO completion:nil];
}


- (void)removeFile
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *filePath = [documentsPath stringByAppendingPathComponent:@"Logfile.txt"];
    NSError *error;
    BOOL success = [fileManager removeItemAtPath:filePath error:&error];
}


#pragma mark - GetApiData
-(void)GetApiData
{
    [SVProgressHUD show];
    NSString *registerURL = [NSString stringWithFormat:@"%@get_salat_data_web.php",kAPIURL];
    ClientGetList = [[HTTPClient alloc] init];
    ClientGetList.delegate = self;
    [ClientGetList getResponseFromAPI:registerURL andParameters:nil];
}
#pragma mark -
#pragma mark - HTTPClient Delegate methods
- (void)httpClient:(HTTPClient *)client didUpdateWithResponse:(id)response
{
    //NSLog(@"Response home screen: %@",response);
    
    [SVProgressHUD dismiss];
    if (client== ClientGetList)
    {
        DicServicesData=response;
        ArrTodayArt=[[NSMutableArray alloc]init];
        ArrTodayArt=[[response objectForKey:@"salat"] objectForKey:@"today"];
        
        
        [tblListAlarm reloadData];
        
    }
    else if (client== ClientNoti)
    {
        NSString *code=[response objectForKey:@"code"];
        if ([code isEqualToString:@"200"])
        {
            
        }
    }
}

- (void)httpClient:(HTTPClient *)client didFailedWithError:(NSError *)error
{
    [SVProgressHUD dismiss];
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:error.localizedDescription delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

-(NSDate*)StringToNotidate:(NSString *)Str
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate * dateis = [dateFormatter dateFromString: Str];
    return dateis;
    
}
#pragma mark - Tableview Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (ArrTodayArt.count!=0)
    {
        return ArrPreyerName.count;
    }
    else
    {
        return 0;
    }
    
}
- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"StatusCellIdentifier";
    HomeCell1 *cell = (HomeCell1 *)[tv dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"HomeCell1" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.layer.borderWidth = 1;
    cell.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    cell.lblPrayerName.text=[ArrPreyerName objectAtIndex:indexPath.row];
    
    cell.Switch.hidden=YES;
    
    [cell.Switch setDividerImage:[UIImage imageNamed:@"off"] forLeftSegmentState:UIControlStateSelected rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [cell.Switch setDividerImage:[UIImage imageNamed:@"on"] forLeftSegmentState:UIControlStateNormal rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    cell.Switch.tag=indexPath.row;
    [cell.Switch addTarget:self action:@selector(segmentSwitch:)forControlEvents:UIControlEventValueChanged];
    
    
    //Switch1
    if (indexPath.row==0)
    {
        cell.lblPrayerTime.text=[self TimeFormateChange:[ArrTodayArt valueForKey:@"imsaak"]];
        
//        NSString *str1=[[NSUserDefaults standardUserDefaults]valueForKey:@"Switch1"];
//        if (str1 !=nil)
//        {
//            if ([str1 isEqualToString:@"on"])
//            {
//                [cell.Switch setSelectedSegmentIndex:1];
//                cell.imgWatch.image=[UIImage imageNamed:@"green_alamIcon"];
//            }
//            else
//            {
//                [cell.Switch setSelectedSegmentIndex:0];
//                cell.imgWatch.image=[UIImage imageNamed:@"red_alamIcon"];
//            }
//        }
//        else
//        {
//            [cell.Switch setSelectedSegmentIndex:0];
//            cell.imgWatch.image=[UIImage imageNamed:@"red_alamIcon"];
//        }
        
    }
    //Switch2
    else if (indexPath.row==1)
    {
        cell.lblPrayerTime.text=[self TimeFormateChange:[ArrTodayArt valueForKey:@"fajr"]];
        
//        NSString *str2=[[NSUserDefaults standardUserDefaults]valueForKey:@"Switch2"];
//        if (str2 !=nil)
//        {
//            if ([str2 isEqualToString:@"on"])
//            {
//                [cell.Switch setSelectedSegmentIndex:1];
//                cell.imgWatch.image=[UIImage imageNamed:@"green_alamIcon"];
//            }
//            else
//            {
//                [cell.Switch setSelectedSegmentIndex:0];
//                cell.imgWatch.image=[UIImage imageNamed:@"red_alamIcon"];
//            }
//        }
//        else
//        {
//            [cell.Switch setSelectedSegmentIndex:0];
//            cell.imgWatch.image=[UIImage imageNamed:@"red_alamIcon"];
//        }
        
        
    }
    //Switch3
    else if (indexPath.row==2)
    {
        cell.lblPrayerTime.text=[self TimeFormateChange:[ArrTodayArt valueForKey:@"sunrise"]];
        
//        NSString *str3=[[NSUserDefaults standardUserDefaults]valueForKey:@"Switch3"];
//        if (str3 !=nil)
//        {
//            if ([str3 isEqualToString:@"on"])
//            {
//                [cell.Switch setSelectedSegmentIndex:1];
//                cell.imgWatch.image=[UIImage imageNamed:@"green_alamIcon"];
//            }
//            else
//            {
//                [cell.Switch setSelectedSegmentIndex:0];
//                cell.imgWatch.image=[UIImage imageNamed:@"red_alamIcon"];
//            }
//        }
//        else
//        {
//            [cell.Switch setSelectedSegmentIndex:0];
//            cell.imgWatch.image=[UIImage imageNamed:@"red_alamIcon"];
//        }
    }
    ////Switch4
    else if (indexPath.row==3)
    {
        cell.lblPrayerTime.text=[self TimeFormateChange:[ArrTodayArt valueForKey:@"zohar"]];
        
//        NSString *str4=[[NSUserDefaults standardUserDefaults]valueForKey:@"Switch4"];
//        if (str4 !=nil)
//        {
//            if ([str4 isEqualToString:@"on"])
//            {
//                [cell.Switch setSelectedSegmentIndex:1];
//                cell.imgWatch.image=[UIImage imageNamed:@"green_alamIcon"];
//            }
//            else
//            {
//                [cell.Switch setSelectedSegmentIndex:0];
//                cell.imgWatch.image=[UIImage imageNamed:@"red_alamIcon"];
//            }
//        }
//        else
//        {
//            [cell.Switch setSelectedSegmentIndex:0];
//            cell.imgWatch.image=[UIImage imageNamed:@"red_alamIcon"];
//        }
    }
    //Switch5
    else if (indexPath.row==4)
    {
        cell.lblPrayerTime.text=[self TimeFormateChange:[ArrTodayArt valueForKey:@"sunset"]];
        
        
//        NSString *str5=[[NSUserDefaults standardUserDefaults]valueForKey:@"Switch5"];
//        if (str5 !=nil)
//        {
//            if ([str5 isEqualToString:@"on"])
//            {
//                [cell.Switch setSelectedSegmentIndex:1];
//                cell.imgWatch.image=[UIImage imageNamed:@"green_alamIcon"];
//            }
//            else
//            {
//                [cell.Switch setSelectedSegmentIndex:0];
//                cell.imgWatch.image=[UIImage imageNamed:@"red_alamIcon"];
//            }
//        }
//        else
//        {
//            [cell.Switch setSelectedSegmentIndex:0];
//            cell.imgWatch.image=[UIImage imageNamed:@"red_alamIcon"];
//        }
    }
    //Switch6
    else if (indexPath.row==5)
    {
        cell.lblPrayerTime.text=[self TimeFormateChange:[ArrTodayArt valueForKey:@"maghrib"]];
        
//        NSString *str6=[[NSUserDefaults standardUserDefaults]valueForKey:@"Switch6"];
//        if (str6 !=nil)
//        {
//            if ([str6 isEqualToString:@"on"])
//            {
//                [cell.Switch setSelectedSegmentIndex:1];
//                cell.imgWatch.image=[UIImage imageNamed:@"green_alamIcon"];
//            }
//            else
//            {
//                [cell.Switch setSelectedSegmentIndex:0];
//                cell.imgWatch.image=[UIImage imageNamed:@"red_alamIcon"];
//            }
//        }
//        else
//        {
//            [cell.Switch setSelectedSegmentIndex:0];
//            cell.imgWatch.image=[UIImage imageNamed:@"red_alamIcon"];
//        }
    }
    //Switch7
    else if (indexPath.row==6)
    {
        cell.lblPrayerTime.text=[self TimeFormateChange:[ArrTodayArt valueForKey:@"midnight"]];
        
//        NSString *str7=[[NSUserDefaults standardUserDefaults]valueForKey:@"Switch7"];
//        if (str7 !=nil)
//        {
//            if ([str7 isEqualToString:@"on"])
//            {
//                [cell.Switch setSelectedSegmentIndex:1];
//                cell.imgWatch.image=[UIImage imageNamed:@"green_alamIcon"];
//            }
//            else
//            {
//                [cell.Switch setSelectedSegmentIndex:0];
//                cell.imgWatch.image=[UIImage imageNamed:@"red_alamIcon"];
//            }
//        }
//        else
//        {
//            [cell.Switch setSelectedSegmentIndex:0];
//            cell.imgWatch.image=[UIImage imageNamed:@"red_alamIcon"];
//        }
    }
    
    return cell;
}

-(void)printCounter
{
    int counter =(int) [[[UIApplication sharedApplication] scheduledLocalNotifications] count];
    NSLog(@"ALL_SCHEDULED_NOTIFICATION_COUNT : %u",[[[UIApplication sharedApplication] scheduledLocalNotifications] count]);
}
- (NSIndexPath *)indexPathWithSubview:(UIView *)subview
{
    while (![subview isKindOfClass:[UITableViewCell self]] && subview)
    {
        subview = subview.superview;
    }
    return [self.tblListAlarm indexPathForCell:(UITableViewCell *)subview];
}

-(void)cancelAllOldNotificationForNT:(NSString *)nt
{
    UIApplication *app = [UIApplication sharedApplication];
    NSArray *allNoti = [app scheduledLocalNotifications];
    for (int i=0; i<[allNoti count]; i++)
    {
        UILocalNotification* oneEvent = [allNoti objectAtIndex:i];
        NSDictionary *userInfoCurrent = oneEvent.userInfo;
        NSString *uid=[NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"NT"]];
        if ([uid isEqualToString:nt])
        {
            [app cancelLocalNotification:oneEvent];
            //            break;
        }
    }
    
}

#pragma mark - SetAlarm
-(void)ImssakSwitchAlaram
{
    [self cancelAllOldNotificationForNT:@"1"];
    
    
    //today
    NSString *strdatetoday=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"today"] objectForKey:@"prayer_date"];
    
    NSString *Tmimsaaktoday=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"today"] objectForKey:@"imsaak"];
    
    NSString *alarm1today=[NSString stringWithFormat:@"%@ %@",strdatetoday,Tmimsaaktoday];
    
    NSDate * dateistoday=[self StringToNotidate:alarm1today];
    
    [self dateComparision:dateistoday];
    if (isTokonValid ==YES)
    {
        NSLog(@"Imsaak Alarm on");
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = dateistoday;
        NSString *str_newTime=[NSString stringWithFormat:@"Imsaak - %@",[self TimeFormateChange:Tmimsaaktoday]];
        localNotification.alertBody =str_newTime;
        localNotification.soundName = @"alarm_sound.m4r";
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.applicationIconBadgeNumber = 0;
        NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
        [userinfo setValue:[NSString stringWithFormat:@"Imsaak %@",dateistoday] forKey:@"AlarmName"];
        [userinfo setValue:@"1" forKey:@"NT"];
        localNotification.userInfo=userinfo;
        
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
    
    
    
    //tomtomorrow
    NSString *strdate=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"tomorrow"] objectForKey:@"prayer_date"];
    
    NSString *Tmimsaak=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"tomorrow"] objectForKey:@"imsaak"];
    
    NSString *alarm1=[NSString stringWithFormat:@"%@ %@",strdate,Tmimsaak];
    
    NSDate * dateis=[self StringToNotidate:alarm1];
    
    [self dateComparision:dateis];
    
    if (isTokonValid ==YES)
    {
        NSLog(@"tomtomorrow Imsaak Alarm on");
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = dateis;
        NSString *str_newTime=[NSString stringWithFormat:@"Imsaak - %@",[self TimeFormateChange:Tmimsaak]];
        
        localNotification.alertBody = str_newTime;
        localNotification.soundName = @"alarm_sound.m4r";
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.applicationIconBadgeNumber = 0;
        NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
        [userinfo setValue:[NSString stringWithFormat:@"Imsaak %@",dateis] forKey:@"AlarmName"];
        [userinfo setValue:@"1" forKey:@"NT"];
        localNotification.userInfo=userinfo;
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
    
    //day_after_tomorrow
    
    NSString *strdate2=[[[DicServicesData objectForKey:@"salat"]objectForKey:@"day_after_tomorrow"] objectForKey:@"prayer_date"];
    
    NSString *Tmimsaak2=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_tomorrow"] objectForKey:@"imsaak"];
    
    NSString *alarm2=[NSString stringWithFormat:@"%@ %@",strdate2,Tmimsaak2];
    
    NSDate * dateis2=[self StringToNotidate:alarm2];
    
    [self dateComparision:dateis2];
    
    if (isTokonValid ==YES)
    {
        NSLog(@"day_after_tomorrow Imsaak Alarm on");
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = dateis2;
         NSString *str_newTime=[NSString stringWithFormat:@"Imsaak - %@",[self TimeFormateChange:Tmimsaak2]];
        localNotification.alertBody = str_newTime;
        localNotification.soundName = @"alarm_sound.m4r";
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.applicationIconBadgeNumber = 0;
        NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
        [userinfo setValue:@"Imsaak" forKey:@"AlarmName"];
        [userinfo setValue:[NSString stringWithFormat:@"Imsaak %@",dateis2] forKey:@"AlarmName"];
        [userinfo setValue:@"1" forKey:@"NT"];
        localNotification.userInfo=userinfo;
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
    
    //day_after_4_prayer_time
    NSString *strdate3=[[[DicServicesData objectForKey:@"salat"]objectForKey:@"day_after_4_prayer_time"] objectForKey:@"prayer_date"];
    
    NSString *Tmimsaak3=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_4_prayer_time"] objectForKey:@"imsaak"];
    
    NSString *alarm3=[NSString stringWithFormat:@"%@ %@",strdate3,Tmimsaak3];
    
    NSDate * dateis3=[self StringToNotidate:alarm3];
    
    [self dateComparision:dateis3];
    
    if (isTokonValid ==YES)
    {
        NSLog(@"day_after_4_prayer_time Imsaak Alarm on");
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = dateis3;
        NSString *str_newTime=[NSString stringWithFormat:@"Imsaak - %@",[self TimeFormateChange:Tmimsaak3]];
        localNotification.alertBody = str_newTime;
        localNotification.soundName = @"alarm_sound.m4r";
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.applicationIconBadgeNumber = 0;
        NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
        [userinfo setValue:@"Imsaak" forKey:@"AlarmName"];
        [userinfo setValue:[NSString stringWithFormat:@"Imsaak %@",dateis3] forKey:@"AlarmName"];
        [userinfo setValue:@"1" forKey:@"NT"];
        localNotification.userInfo=userinfo;
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
    
    //day_after_5_prayer_time
    NSString *strdate4=[[[DicServicesData objectForKey:@"salat"]objectForKey:@"day_after_5_prayer_time"] objectForKey:@"prayer_date"];
    
    NSString *Tmimsaak4=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_5_prayer_time"] objectForKey:@"imsaak"];
    
    NSString *alarm4=[NSString stringWithFormat:@"%@ %@",strdate4,Tmimsaak4];
    
    NSDate * dateis4=[self StringToNotidate:alarm4];
    
    [self dateComparision:dateis4];
    
    if (isTokonValid ==YES)
    {
        NSLog(@"day_after_5_prayer_time Imsaak Alarm on");
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = dateis4;
        NSString *str_newTime=[NSString stringWithFormat:@"Imsaak - %@",[self TimeFormateChange:Tmimsaak4]];
        localNotification.alertBody = str_newTime;
        localNotification.soundName = @"alarm_sound.m4r";
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.applicationIconBadgeNumber = 0;
        NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
        [userinfo setValue:[NSString stringWithFormat:@"Imsaak %@",dateis4] forKey:@"AlarmName"];
        [userinfo setValue:@"1" forKey:@"NT"];
        localNotification.userInfo=userinfo;
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
    
    
    //day_after_6_prayer_time
    NSString *strdate5=[[[DicServicesData objectForKey:@"salat"]objectForKey:@"day_after_6_prayer_time"] objectForKey:@"prayer_date"];
    
    NSString *Tmimsaak5=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_6_prayer_time"] objectForKey:@"imsaak"];
    
    NSString *alarm5=[NSString stringWithFormat:@"%@ %@",strdate5,Tmimsaak5];
    
    NSDate * dateis5=[self StringToNotidate:alarm5];
    
    [self dateComparision:dateis5];
    
    if (isTokonValid ==YES)
    {
        NSLog(@"day_after_6_prayer_time Imsaak Alarm on");
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = dateis5;
        NSString *str_newTime=[NSString stringWithFormat:@"Imsaak - %@",[self TimeFormateChange:Tmimsaak5]];
        localNotification.alertBody = str_newTime;
        localNotification.soundName = @"alarm_sound.m4r";
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.applicationIconBadgeNumber = 0;
        NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
        [userinfo setValue:[NSString stringWithFormat:@"Imsaak %@",dateis5] forKey:@"AlarmName"];
        [userinfo setValue:@"1" forKey:@"NT"];
        localNotification.userInfo=userinfo;
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
    
    //day_after_7_prayer_time
    
    NSString *strdate6=[[[DicServicesData objectForKey:@"salat"]objectForKey:@"day_after_7_prayer_time"] objectForKey:@"prayer_date"];
    
    NSString *Tmimsaak6=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_7_prayer_time"] objectForKey:@"imsaak"];
    
    NSString *alarm6=[NSString stringWithFormat:@"%@ %@",strdate6,Tmimsaak6];
    
    NSDate * dateis6=[self StringToNotidate:alarm6];
    
    [self dateComparision:dateis6];
    
    if (isTokonValid ==YES)
    {
        NSLog(@"day_after_7_prayer_time Imsaak Alarm on");
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = dateis6;
        NSString *str_newTime=[NSString stringWithFormat:@"Imsaak - %@",[self TimeFormateChange:Tmimsaak6]];
        localNotification.alertBody = str_newTime;
        localNotification.soundName = @"alarm_sound.m4r";
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.applicationIconBadgeNumber = 0;
        NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
        [userinfo setValue:[NSString stringWithFormat:@"Imsaak %@",dateis6] forKey:@"AlarmName"];
        [userinfo setValue:@"1" forKey:@"NT"];
        localNotification.userInfo=userinfo;
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
}
-(void)FajrSwitchAlaram
{
    [self cancelAllOldNotificationForNT:@"2"];
    
    
    //today
    NSString *strdatetoday=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"today"] objectForKey:@"prayer_date"];
    
    NSString *Tmimsaaktoday=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"today"] objectForKey:@"fajr"];
    
    NSString *alarm1today=[NSString stringWithFormat:@"%@ %@",strdatetoday,Tmimsaaktoday];
    
    NSDate * dateistoday=[self StringToNotidate:alarm1today];
    
    [self dateComparision:dateistoday];
    if (isTokonValid ==YES)
    {
        NSLog(@"fajr Alarm on");
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = dateistoday;
        NSString *str_newTime=[NSString stringWithFormat:@"Fajr - %@",[self TimeFormateChange:Tmimsaaktoday]];
        localNotification.alertBody = str_newTime;
        localNotification.soundName = @"alarm_sound.m4r";
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.applicationIconBadgeNumber = 0;
        NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
        [userinfo setValue:[NSString stringWithFormat:@"fajr %@",dateistoday] forKey:@"AlarmName"];
        [userinfo setValue:@"2" forKey:@"NT"];
        localNotification.userInfo=userinfo;
        
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
    
    
    
    //tomtomorrow
    NSString *strdate=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"tomorrow"] objectForKey:@"prayer_date"];
    
    NSString *TmFajr=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"tomorrow"] objectForKey:@"fajr"];
    
    NSString *alarm1=[NSString stringWithFormat:@"%@ %@",strdate,TmFajr];
    
    NSDate * dateis=[self StringToNotidate:alarm1];
    
    [self dateComparision:dateis];
    if (isTokonValid ==YES)
    {
        NSLog(@"tomtomorrow Fajr Alarm on");
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = dateis;
         NSString *str_newTime=[NSString stringWithFormat:@"Fajr - %@",[self TimeFormateChange:TmFajr]];
        localNotification.alertBody = str_newTime;
        localNotification.soundName = @"alarm_sound.m4r";
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.applicationIconBadgeNumber = 0;
        NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
        [userinfo setValue:[NSString stringWithFormat:@"Fajr %@",dateis] forKey:@"AlarmName"];
        [userinfo setValue:@"2" forKey:@"NT"];
        localNotification.userInfo=userinfo;
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
    
    
    //day_after_tomorrow
    NSString *strdate2=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_tomorrow"] objectForKey:@"prayer_date"];
    
    NSString *TmFajr2=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_tomorrow"] objectForKey:@"fajr"];
    
    NSString *alarm2=[NSString stringWithFormat:@"%@ %@",strdate2,TmFajr2];
    
    NSDate * dateis2=[self StringToNotidate:alarm2];
    
    [self dateComparision:dateis2];
    
    if (isTokonValid ==YES)
    {
        NSLog(@"day_after_tomorrow Fajr Alarm on");
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = dateis2;
        
        NSString *str_newTime=[NSString stringWithFormat:@"Fajr - %@",[self TimeFormateChange:TmFajr2]];

        localNotification.alertBody = str_newTime;
        localNotification.soundName = @"alarm_sound.m4r";
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.applicationIconBadgeNumber = 0;
        NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
        [userinfo setValue:[NSString stringWithFormat:@"Fajr %@",dateis2] forKey:@"AlarmName"];
        [userinfo setValue:@"2" forKey:@"NT"];
        localNotification.userInfo=userinfo;
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
    
    //day_after_4_prayer_time
    NSString *strdate3=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_4_prayer_time"] objectForKey:@"prayer_date"];
    
    NSString *TmFajr3=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_4_prayer_time"] objectForKey:@"fajr"];
    
    NSString *alarm3=[NSString stringWithFormat:@"%@ %@",strdate3,TmFajr3];
    
    NSDate * dateis3=[self StringToNotidate:alarm3];
    
    [self dateComparision:dateis3];
    
    if (isTokonValid ==YES)
    {
        NSLog(@"day_after_4_prayer_time Fajr Alarm on");
        UILocalNotification *localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = dateis3;
        NSString *str_newTime=[NSString stringWithFormat:@"Fajr - %@",[self TimeFormateChange:TmFajr3]];

        localNotification.alertBody = str_newTime;
        localNotification.soundName = @"alarm_sound.m4r";
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.applicationIconBadgeNumber = 0;
        NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
        [userinfo setValue:[NSString stringWithFormat:@"Fajr %@",dateis3] forKey:@"AlarmName"];
        [userinfo setValue:@"2" forKey:@"NT"];
        localNotification.userInfo=userinfo;
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
    
    //day_after_5_prayer_time
    NSString *strdate4=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_5_prayer_time"] objectForKey:@"prayer_date"];
    
    NSString *TmFajr4=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_5_prayer_time"] objectForKey:@"fajr"];
    
    NSString *alarm4=[NSString stringWithFormat:@"%@ %@",strdate4,TmFajr4];
    
    NSDate * dateis4=[self StringToNotidate:alarm4];
    
    [self dateComparision:dateis4];
    
    if (isTokonValid ==YES)
    {
        NSLog(@"day_after_5_prayer_time Fajr Alarm on");
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = dateis4;
        NSString *str_newTime=[NSString stringWithFormat:@"Fajr - %@",[self TimeFormateChange:TmFajr4]];

        localNotification.alertBody = str_newTime;
        localNotification.soundName = @"alarm_sound.m4r";
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.applicationIconBadgeNumber = 0;
        NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
        [userinfo setValue:[NSString stringWithFormat:@"Fajr %@",dateis4] forKey:@"AlarmName"];
        [userinfo setValue:@"2" forKey:@"NT"];
        localNotification.userInfo=userinfo;
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
    
    //day_after_6_prayer_time
    
    NSString *strdate5=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_6_prayer_time"] objectForKey:@"prayer_date"];
    
    NSString *TmFajr5=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_6_prayer_time"] objectForKey:@"fajr"];
    
    NSString *alarm5=[NSString stringWithFormat:@"%@ %@",strdate5,TmFajr5];
    
    NSDate * dateis5=[self StringToNotidate:alarm5];
    
    [self dateComparision:dateis5];
    
    if (isTokonValid ==YES)
    {
        NSLog(@"day_after_6_prayer_time Fajr Alarm on");
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = dateis5;
        NSString *str_newTime=[NSString stringWithFormat:@"Fajr - %@",[self TimeFormateChange:TmFajr5]];
        
        localNotification.alertBody = str_newTime;
        localNotification.soundName = @"alarm_sound.m4r";
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.applicationIconBadgeNumber = 0;
        NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
        [userinfo setValue:[NSString stringWithFormat:@"Fajr %@",dateis5] forKey:@"AlarmName"];
        [userinfo setValue:@"2" forKey:@"NT"];
        localNotification.userInfo=userinfo;
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
    
    //day_after_7_prayer_time
    NSString *strdate6=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_7_prayer_time"] objectForKey:@"prayer_date"];
    
    NSString *TmFajr6=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_7_prayer_time"] objectForKey:@"fajr"];
    
    NSString *alarm6=[NSString stringWithFormat:@"%@ %@",strdate6,TmFajr6];
    
    NSDate * dateis6=[self StringToNotidate:alarm6];
    
    [self dateComparision:dateis6];
    
    if (isTokonValid ==YES)
    {
        NSLog(@"day_after_7_prayer_time Fajr Alarm on");
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = dateis6;
        NSString *str_newTime=[NSString stringWithFormat:@"Fajr - %@",[self TimeFormateChange:TmFajr6]];
        
        localNotification.alertBody = str_newTime;
        localNotification.soundName = @"alarm_sound.m4r";
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.applicationIconBadgeNumber = 0;
        NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
        [userinfo setValue:[NSString stringWithFormat:@"Fajr %@",dateis6] forKey:@"AlarmName"];
        [userinfo setValue:@"2" forKey:@"NT"];
        localNotification.userInfo=userinfo;
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
    
}
-(void)SunriseSwitchAlaram
{
    [self cancelAllOldNotificationForNT:@"3"];
    
    
    //today
    NSString *strdatetoday=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"today"] objectForKey:@"prayer_date"];
    
    NSString *Tmimsaaktoday=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"today"] objectForKey:@"sunrise"];
    
    NSString *alarm1today=[NSString stringWithFormat:@"%@ %@",strdatetoday,Tmimsaaktoday];
    
    NSDate * dateistoday=[self StringToNotidate:alarm1today];
    
    [self dateComparision:dateistoday];
    if (isTokonValid ==YES)
    {
        NSLog(@"sunrise Alarm on");
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = dateistoday;
        
        NSString *str_newTime=[NSString stringWithFormat:@"Sunrise - %@",[self TimeFormateChange:Tmimsaaktoday]];
        localNotification.alertBody = str_newTime;
        localNotification.soundName = @"alarm_sound.m4r";
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.applicationIconBadgeNumber = 0;
        NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
        [userinfo setValue:[NSString stringWithFormat:@"sunrise %@",dateistoday] forKey:@"AlarmName"];
        [userinfo setValue:@"3" forKey:@"NT"];
        localNotification.userInfo=userinfo;
        
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
    
    
    
    //tomtomorrow
    NSString *strdate=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"tomorrow"] objectForKey:@"prayer_date"];
    
    NSString *Tmsunrise=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"tomorrow"] objectForKey:@"sunrise"];
    
    NSString *alarm1=[NSString stringWithFormat:@"%@ %@",strdate,Tmsunrise];
    
    NSDate * dateis=[self StringToNotidate:alarm1];
    
    [self dateComparision:dateis];
    
    if (isTokonValid ==YES)
    {
        NSLog(@"tomtomorrow Sunrise Alarm on");
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = dateis;
        NSString *str_newTime=[NSString stringWithFormat:@"Sunrise - %@",[self TimeFormateChange:Tmsunrise]];
        localNotification.alertBody = str_newTime;
        localNotification.soundName = @"alarm_sound.m4r";
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication]applicationIconBadgeNumber] + 1;
        NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
        [userinfo setValue:[NSString stringWithFormat:@"Sunrise %@",dateis] forKey:@"AlarmName"];
        [userinfo setValue:@"3" forKey:@"NT"];
        localNotification.userInfo=userinfo;
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
    
    //day_after_tomorrow
    NSString *strdate2=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_tomorrow"] objectForKey:@"prayer_date"];
    
    NSString *Tmsunrise2=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_tomorrow"] objectForKey:@"sunrise"];
    
    NSString *alarm2=[NSString stringWithFormat:@"%@ %@",strdate2,Tmsunrise2];
    
    NSDate * dateis2=[self StringToNotidate:alarm2];
    
    [self dateComparision:dateis2];
    
    if (isTokonValid ==YES)
    {
        NSLog(@"day_after_tomorrow Sunrise Alarm on");
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = dateis2;
        NSString *str_newTime=[NSString stringWithFormat:@"Sunrise - %@",[self TimeFormateChange:Tmsunrise2]];
        localNotification.alertBody = str_newTime;
        localNotification.soundName = @"alarm_sound.m4r";
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication]applicationIconBadgeNumber] + 1;
        NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
        [userinfo setValue:[NSString stringWithFormat:@"Sunrise %@",dateis2] forKey:@"AlarmName"];
        [userinfo setValue:@"3" forKey:@"NT"];
        localNotification.userInfo=userinfo;
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
    
    //day_after_4_prayer_time
    NSString *strdate3=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_4_prayer_time"] objectForKey:@"prayer_date"];
    
    NSString *Tmsunrise3=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_4_prayer_time"] objectForKey:@"sunrise"];
    
    NSString *alarm3=[NSString stringWithFormat:@"%@ %@",strdate3,Tmsunrise3];
    
    NSDate * dateis3=[self StringToNotidate:alarm3];
    
    [self dateComparision:dateis3];
    
    if (isTokonValid ==YES)
    {
        NSLog(@"day_after_4_prayer_time Sunrise Alarm on");
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = dateis3;
        NSString *str_newTime=[NSString stringWithFormat:@"Sunrise - %@",[self TimeFormateChange:Tmsunrise3]];
        localNotification.alertBody = str_newTime;
        localNotification.soundName = @"alarm_sound.m4r";
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication]applicationIconBadgeNumber] + 1;
        NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
        [userinfo setValue:[NSString stringWithFormat:@"Sunrise %@",dateis3] forKey:@"AlarmName"];
        [userinfo setValue:@"3" forKey:@"NT"];
        localNotification.userInfo=userinfo;
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
    
    //day_after_5_prayer_time
    NSString *strdate4=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_5_prayer_time"] objectForKey:@"prayer_date"];
    
    NSString *Tmsunrise4=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_5_prayer_time"] objectForKey:@"sunrise"];
    
    NSString *alarm4=[NSString stringWithFormat:@"%@ %@",strdate4,Tmsunrise4];
    
    NSDate * dateis4=[self StringToNotidate:alarm4];
    
    [self dateComparision:dateis4];
    
    if (isTokonValid ==YES)
    {
        NSLog(@"day_after_5_prayer_time Sunrise Alarm on");
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = dateis4;
        NSString *str_newTime=[NSString stringWithFormat:@"Sunrise - %@",[self TimeFormateChange:Tmsunrise4]];
        localNotification.alertBody = str_newTime;
        localNotification.soundName = @"alarm_sound.m4r";
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication]applicationIconBadgeNumber] + 1;
        NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
        [userinfo setValue:[NSString stringWithFormat:@"Sunrise %@",dateis4] forKey:@"AlarmName"];
        [userinfo setValue:@"3" forKey:@"NT"];
        localNotification.userInfo=userinfo;
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
    
    //day_after_6_prayer_time
    NSString *strdate5=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_6_prayer_time"] objectForKey:@"prayer_date"];
    
    NSString *Tmsunrise5=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_6_prayer_time"] objectForKey:@"sunrise"];
    
    NSString *alarm5=[NSString stringWithFormat:@"%@ %@",strdate5,Tmsunrise5];
    
    NSDate *dateis5=[self StringToNotidate:alarm5];
    
    [self dateComparision:dateis5];
    
    if (isTokonValid ==YES)
    {
        NSLog(@"day_after_6_prayer_time Sunrise Alarm on");
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = dateis5;
        NSString *str_newTime=[NSString stringWithFormat:@"Sunrise - %@",[self TimeFormateChange:Tmsunrise5]];
        localNotification.alertBody = str_newTime;
        localNotification.soundName = @"alarm_sound.m4r";
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication]applicationIconBadgeNumber] + 1;
        NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
        [userinfo setValue:[NSString stringWithFormat:@"Sunrise %@",dateis5] forKey:@"AlarmName"];
        [userinfo setValue:@"3" forKey:@"NT"];
        localNotification.userInfo=userinfo;
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
    
    //day_after_7_prayer_time
    NSString *strdate6=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_7_prayer_time"] objectForKey:@"prayer_date"];
    
    NSString *Tmsunrise6=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_7_prayer_time"] objectForKey:@"sunrise"];
    
    NSString *alarm6=[NSString stringWithFormat:@"%@ %@",strdate6,Tmsunrise6];
    
    NSDate * dateis6=[self StringToNotidate:alarm6];
    
    [self dateComparision:dateis6];
    
    if (isTokonValid ==YES)
    {
        NSLog(@"day_after_7_prayer_time Sunrise Alarm on");
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = dateis6;
        NSString *str_newTime=[NSString stringWithFormat:@"Sunrise - %@",[self TimeFormateChange:Tmsunrise6]];
        localNotification.alertBody = str_newTime;
        localNotification.soundName = @"alarm_sound.m4r";
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication]applicationIconBadgeNumber] + 1;
        NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
        [userinfo setValue:[NSString stringWithFormat:@"Sunrise %@",dateis6] forKey:@"AlarmName"];
        [userinfo setValue:@"3" forKey:@"NT"];
        localNotification.userInfo=userinfo;
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
}
-(void)ZoharSwitchAlaram
{
    [self cancelAllOldNotificationForNT:@"4"];
    
    //today
    NSString *strdatetoday=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"today"] objectForKey:@"prayer_date"];
    
    NSString *Tmimsaaktoday=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"today"] objectForKey:@"zohar"];
    
    NSString *alarm1today=[NSString stringWithFormat:@"%@ %@",strdatetoday,Tmimsaaktoday];
    
    NSDate * dateistoday=[self StringToNotidate:alarm1today];
    
    [self dateComparision:dateistoday];
    if (isTokonValid ==YES)
    {
        NSLog(@"zohar Alarm on");
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = dateistoday;
        NSString *str_newTime=[NSString stringWithFormat:@"Zohr - %@",[self TimeFormateChange:Tmimsaaktoday]];
        localNotification.alertBody = str_newTime;
        localNotification.soundName = @"alarm_sound.m4r";
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.applicationIconBadgeNumber = 0;
        NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
        [userinfo setValue:[NSString stringWithFormat:@"zohr %@",dateistoday] forKey:@"AlarmName"];
        [userinfo setValue:@"4" forKey:@"NT"];
        localNotification.userInfo=userinfo;
        
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
    
    
    
    
    //tomtomorrow
    NSString *strdate=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"tomorrow"] objectForKey:@"prayer_date"];
    
    NSString *Tmzohar=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"tomorrow"] objectForKey:@"zohar"];
    
    NSString *alarm1=[NSString stringWithFormat:@"%@ %@",strdate,Tmzohar];
    
    NSDate * dateis=[self StringToNotidate:alarm1];
    
    [self dateComparision:dateis];
    
    if (isTokonValid ==YES)
    {
        NSLog(@"tomtomorrow Zohar Alarm on");
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = dateis;
        NSString *str_newTime=[NSString stringWithFormat:@"Zohr - %@",[self TimeFormateChange:Tmzohar]];
        localNotification.alertBody = str_newTime;
        localNotification.soundName = @"alarm_sound.m4r";
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.applicationIconBadgeNumber = 0;
        NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
        [userinfo setValue:[NSString stringWithFormat:@"Zohr %@",dateis] forKey:@"AlarmName"];
        [userinfo setValue:@"4" forKey:@"NT"];
        localNotification.userInfo=userinfo;
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
    
    //day_after_tomorrow
    NSString *strdate2=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_tomorrow"] objectForKey:@"prayer_date"];
    
    NSString *Tmzohar2=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_tomorrow"] objectForKey:@"zohar"];
    
    NSString *alarm2=[NSString stringWithFormat:@"%@ %@",strdate2,Tmzohar2];
    
    NSDate * dateis2=[self StringToNotidate:alarm2];
    
    [self dateComparision:dateis2];
    
    if (isTokonValid ==YES)
    {
        NSLog(@"day_after_tomorrow Zohar Alarm on");
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = dateis2;
        NSString *str_newTime=[NSString stringWithFormat:@"Zohr - %@",[self TimeFormateChange:Tmzohar2]];
        localNotification.alertBody = str_newTime;
        localNotification.soundName = @"alarm_sound.m4r";
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.applicationIconBadgeNumber = 0;
        NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
        [userinfo setValue:[NSString stringWithFormat:@"Zohr %@",dateis2] forKey:@"AlarmName"];
        [userinfo setValue:@"4" forKey:@"NT"];
        localNotification.userInfo=userinfo;
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
    
    //day_after_4_prayer_time
    NSString *strdate3=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_4_prayer_time"] objectForKey:@"prayer_date"];
    
    NSString *Tmzohar3=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_4_prayer_time"] objectForKey:@"zohar"];
    
    NSString *alarm3=[NSString stringWithFormat:@"%@ %@",strdate3,Tmzohar3];
    
    NSDate * dateis3=[self StringToNotidate:alarm3];
    
    [self dateComparision:dateis3];
    
    if (isTokonValid ==YES)
    {
        NSLog(@"day_after_4_prayer_time Zohar Alarm on");
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = dateis3;
        NSString *str_newTime=[NSString stringWithFormat:@"Zohr - %@",[self TimeFormateChange:Tmzohar3]];
        localNotification.alertBody = str_newTime;
        localNotification.soundName = @"alarm_sound.m4r";
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.applicationIconBadgeNumber = 0;
        NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
        [userinfo setValue:[NSString stringWithFormat:@"Zohar %@",dateis3] forKey:@"AlarmName"];
        [userinfo setValue:@"4" forKey:@"NT"];
        localNotification.userInfo=userinfo;
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
    
    //day_after_5_prayer_time
    NSString *strdate4=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_5_prayer_time"] objectForKey:@"prayer_date"];
    
    NSString *Tmzohar4=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_5_prayer_time"] objectForKey:@"zohar"];
    
    NSString *alarm4=[NSString stringWithFormat:@"%@ %@",strdate4,Tmzohar4];
    
    NSDate * dateis4=[self StringToNotidate:alarm4];
    
    [self dateComparision:dateis4];
    
    if (isTokonValid ==YES)
    {
        NSLog(@"day_after_5_prayer_time Zohar Alarm on");
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = dateis4;
        NSString *str_newTime=[NSString stringWithFormat:@"Zohr - %@",[self TimeFormateChange:Tmzohar4]];
        localNotification.alertBody = str_newTime;
        
        localNotification.soundName = @"alarm_sound.m4r";
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.applicationIconBadgeNumber = 0;
        NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
        [userinfo setValue:[NSString stringWithFormat:@"Zohar %@",dateis4] forKey:@"AlarmName"];
        [userinfo setValue:@"4" forKey:@"NT"];
        localNotification.userInfo=userinfo;
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
    
    //day_after_6_prayer_time
    NSString *strdate5=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_6_prayer_time"] objectForKey:@"prayer_date"];
    
    NSString *Tmzohar5=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_6_prayer_time"] objectForKey:@"zohar"];
    
    NSString *alarm5=[NSString stringWithFormat:@"%@ %@",strdate5,Tmzohar5];
    
    NSDate * dateis5=[self StringToNotidate:alarm5];
    
    [self dateComparision:dateis5];
    
    if (isTokonValid ==YES)
    {
        NSLog(@"day_after_6_prayer_time Zohar Alarm on");
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = dateis5;
        
        NSString *str_newTime=[NSString stringWithFormat:@"Zohr - %@",[self TimeFormateChange:Tmzohar5]];
        localNotification.alertBody = str_newTime;
        
        localNotification.soundName = @"alarm_sound.m4r";
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.applicationIconBadgeNumber = 0;
        NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
        [userinfo setValue:[NSString stringWithFormat:@"Zohar %@",dateis5] forKey:@"AlarmName"];
        [userinfo setValue:@"4" forKey:@"NT"];
        localNotification.userInfo=userinfo;
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
    
    //
    //day_after_7_prayer_time
    NSString *strdate6=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_7_prayer_time"] objectForKey:@"prayer_date"];
    
    NSString *Tmzohar6=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_7_prayer_time"] objectForKey:@"zohar"];
    
    NSString *alarm6=[NSString stringWithFormat:@"%@ %@",strdate6,Tmzohar6];
    
    NSDate * dateis6=[self StringToNotidate:alarm6];
    
    [self dateComparision:dateis6];
    
    if (isTokonValid ==YES)
    {
        NSLog(@"day_after_7_prayer_time Zohar Alarm on");
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = dateis6;
        
        NSString *str_newTime=[NSString stringWithFormat:@"Zohr - %@",[self TimeFormateChange:Tmzohar6]];
        localNotification.alertBody = str_newTime;
        
        localNotification.soundName = @"alarm_sound.m4r";
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.applicationIconBadgeNumber = 0;
        NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
        [userinfo setValue:@"Zohar" forKey:@"AlarmName"];
        [userinfo setValue:[NSString stringWithFormat:@"Zohar %@",dateis6] forKey:@"AlarmName"];
        [userinfo setValue:@"4" forKey:@"NT"];
        localNotification.userInfo=userinfo;
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
}
-(void)SunsetSwitchAlaram
{
    [self cancelAllOldNotificationForNT:@"5"];
    
    
    //today
    NSString *strdatetoday=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"today"] objectForKey:@"prayer_date"];
    
    NSString *Tmimsaaktoday=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"today"] objectForKey:@"sunset"];
    
    NSString *alarm1today=[NSString stringWithFormat:@"%@ %@",strdatetoday,Tmimsaaktoday];
    
    NSDate * dateistoday=[self StringToNotidate:alarm1today];
    
    [self dateComparision:dateistoday];
    if (isTokonValid ==YES)
    {
        NSLog(@"sunset Alarm on");
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = dateistoday;
        
        NSString *str_newTime=[NSString stringWithFormat:@"Sunset - %@",[self TimeFormateChange:Tmimsaaktoday]];
        localNotification.alertBody = str_newTime;
        localNotification.soundName = @"alarm_sound.m4r";
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.applicationIconBadgeNumber = 0;
        NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
        [userinfo setValue:[NSString stringWithFormat:@"sunset %@",dateistoday] forKey:@"AlarmName"];
        [userinfo setValue:@"5" forKey:@"NT"];
        localNotification.userInfo=userinfo;
        
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
    
    
    
    //tomtomorrow
    NSString *strdate=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"tomorrow"] objectForKey:@"prayer_date"];
    
    NSString *Tmsunset=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"tomorrow"] objectForKey:@"sunset"];
    
    NSString *alarm1=[NSString stringWithFormat:@"%@ %@",strdate,Tmsunset];
    
    NSDate * dateis=[self StringToNotidate:alarm1];
    
    [self dateComparision:dateis];
    
    if (isTokonValid ==YES)
    {
        NSLog(@"tomtomorrow Sunset Alarm on");
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = dateis;
        
        NSString *str_newTime=[NSString stringWithFormat:@"Sunset - %@",[self TimeFormateChange:Tmsunset]];
        localNotification.alertBody = str_newTime;
        
        localNotification.soundName = @"alarm_sound.m4r";
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.applicationIconBadgeNumber = 0;
        NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
        [userinfo setValue:[NSString stringWithFormat:@"Sunset %@",dateis] forKey:@"AlarmName"];
        [userinfo setValue:@"5" forKey:@"NT"];
        localNotification.userInfo=userinfo;
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
    
    //day_after_tomorrow
    NSString *strdate2=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_tomorrow"] objectForKey:@"prayer_date"];
    
    NSString *Tmsunset2=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_tomorrow"] objectForKey:@"sunset"];
    
    NSString *alarm2=[NSString stringWithFormat:@"%@ %@",strdate2,Tmsunset2];
    
    NSDate * dateis2=[self StringToNotidate:alarm2];
    
    [self dateComparision:dateis2];
    
    if (isTokonValid ==YES)
    {
        NSLog(@"day_after_tomorrow Sunset Alarm on");
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = dateis2;
        
        NSString *str_newTime=[NSString stringWithFormat:@"Sunset - %@",[self TimeFormateChange:Tmsunset2]];
        localNotification.alertBody = str_newTime;
        
        localNotification.soundName = @"alarm_sound.m4r";
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.applicationIconBadgeNumber = 0;
        NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
        [userinfo setValue:[NSString stringWithFormat:@"Sunset %@",dateis2] forKey:@"AlarmName"];
        [userinfo setValue:@"5" forKey:@"NT"];
        localNotification.userInfo=userinfo;
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
    
    //day_after_4_prayer_time
    NSString *strdate3=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_4_prayer_time"] objectForKey:@"prayer_date"];
    
    NSString *Tmsunset3=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_4_prayer_time"] objectForKey:@"sunset"];
    
    NSString *alarm3=[NSString stringWithFormat:@"%@ %@",strdate3,Tmsunset3];
    
    NSDate * dateis3=[self StringToNotidate:alarm3];
    
    [self dateComparision:dateis3];
    
    if (isTokonValid ==YES)
    {
        NSLog(@"day_after_4_prayer_time Sunset Alarm on");
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = dateis3;
        
        NSString *str_newTime=[NSString stringWithFormat:@"Sunset - %@",[self TimeFormateChange:Tmsunset3]];
        localNotification.alertBody = str_newTime;
        
        localNotification.soundName = @"alarm_sound.m4r";
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.applicationIconBadgeNumber = 0;
        NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
        [userinfo setValue:[NSString stringWithFormat:@"Sunset %@",dateis3] forKey:@"AlarmName"];
        [userinfo setValue:@"5" forKey:@"NT"];
        localNotification.userInfo=userinfo;
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
    
    //day_after_5_prayer_time
    NSString *strdate4=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_5_prayer_time"] objectForKey:@"prayer_date"];
    
    NSString *Tmsunset4=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_5_prayer_time"] objectForKey:@"sunset"];
    
    NSString *alarm4=[NSString stringWithFormat:@"%@ %@",strdate4,Tmsunset4];
    
    NSDate * dateis4=[self StringToNotidate:alarm4];
    
    [self dateComparision:dateis4];
    
    if (isTokonValid ==YES)
    {
        NSLog(@"day_after_5_prayer_time Sunset Alarm on");
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = dateis4;
        
        NSString *str_newTime=[NSString stringWithFormat:@"Sunset - %@",[self TimeFormateChange:Tmsunset4]];
        localNotification.alertBody = str_newTime;
        
        localNotification.soundName = @"alarm_sound.m4r";
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.applicationIconBadgeNumber = 0;
        NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
        [userinfo setValue:[NSString stringWithFormat:@"Sunset %@",dateis4] forKey:@"AlarmName"];
        [userinfo setValue:@"5" forKey:@"NT"];
        localNotification.userInfo=userinfo;
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
    
    //day_after_6_prayer_time
    NSString *strdate5=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_6_prayer_time"] objectForKey:@"prayer_date"];
    
    NSString *Tmsunset5=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_6_prayer_time"] objectForKey:@"sunset"];
    
    NSString *alarm5=[NSString stringWithFormat:@"%@ %@",strdate5,Tmsunset5];
    
    NSDate * dateis5=[self StringToNotidate:alarm5];
    
    [self dateComparision:dateis5];
    
    if (isTokonValid ==YES)
    {
        NSLog(@"day_after_6_prayer_time Sunset Alarm on");
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = dateis5;
        
        NSString *str_newTime=[NSString stringWithFormat:@"Sunset - %@",[self TimeFormateChange:Tmsunset5]];
        localNotification.alertBody = str_newTime;
        localNotification.soundName = @"alarm_sound.m4r";
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.applicationIconBadgeNumber = 0;
        NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
        [userinfo setValue:[NSString stringWithFormat:@"Sunset %@",dateis5] forKey:@"AlarmName"];
        [userinfo setValue:@"5" forKey:@"NT"];
        localNotification.userInfo=userinfo;
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
    
    //day_after_7_prayer_time
    NSString *strdate6=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_7_prayer_time"] objectForKey:@"prayer_date"];
    
    NSString *Tmsunset6=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_7_prayer_time"] objectForKey:@"sunset"];
    
    NSString *alarm6=[NSString stringWithFormat:@"%@ %@",strdate6,Tmsunset6];
    
    NSDate * dateis6=[self StringToNotidate:alarm6];
    
    [self dateComparision:dateis6];
    
    if (isTokonValid ==YES)
    {
        NSLog(@"day_after_7_prayer_time Sunset Alarm on");
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = dateis6;
        
        NSString *str_newTime=[NSString stringWithFormat:@"Sunset - %@",[self TimeFormateChange:Tmsunset6]];
        localNotification.alertBody = str_newTime;
        
        localNotification.soundName = @"alarm_sound.m4r";
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.applicationIconBadgeNumber = 0;
        NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
        [userinfo setValue:[NSString stringWithFormat:@"Sunset %@",dateis6] forKey:@"AlarmName"];
        [userinfo setValue:@"5" forKey:@"NT"];
        localNotification.userInfo=userinfo;
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
}
-(void)MaghribSwitchAlaram
{
    [self cancelAllOldNotificationForNT:@"6"];
    
    
    //today
    NSString *strdatetoday=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"today"] objectForKey:@"prayer_date"];
    
    NSString *Tmimsaaktoday=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"today"] objectForKey:@"maghrib"];
    
    NSString *alarm1today=[NSString stringWithFormat:@"%@ %@",strdatetoday,Tmimsaaktoday];
    
    NSDate * dateistoday=[self StringToNotidate:alarm1today];
    
    [self dateComparision:dateistoday];
    if (isTokonValid ==YES)
    {
        NSLog(@"maghrib Alarm on");
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = dateistoday;
        NSString *str_newTime=[NSString stringWithFormat:@"Maghrib - %@",[self TimeFormateChange:Tmimsaaktoday]];
        localNotification.alertBody = str_newTime;
        localNotification.soundName = @"alarm_sound.m4r";
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.applicationIconBadgeNumber = 0;
        NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
        [userinfo setValue:[NSString stringWithFormat:@"maghrib %@",dateistoday] forKey:@"AlarmName"];
        [userinfo setValue:@"6" forKey:@"NT"];
        localNotification.userInfo=userinfo;
        
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
    
    
    //tomtomorrow
    NSString *strdate=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"tomorrow"] objectForKey:@"prayer_date"];
    
    NSString *Tmmaghrib=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"tomorrow"] objectForKey:@"maghrib"];
    
    NSString *alarm1=[NSString stringWithFormat:@"%@ %@",strdate,Tmmaghrib];
    
    NSDate * dateis=[self StringToNotidate:alarm1];
    
    [self dateComparision:dateis];
    
    if (isTokonValid ==YES)
    {
        NSLog(@"tomtomorrow Maghrib Alarm on");
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = dateis;
        
        NSString *str_newTime=[NSString stringWithFormat:@"Maghrib - %@",[self TimeFormateChange:Tmmaghrib]];
        localNotification.alertBody = str_newTime;
        
        localNotification.soundName = @"alarm_sound.m4r";
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.applicationIconBadgeNumber = 0;
        NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
        [userinfo setValue:[NSString stringWithFormat:@"Maghrib %@",dateis] forKey:@"AlarmName"];
        [userinfo setValue:@"6" forKey:@"NT"];
        localNotification.userInfo=userinfo;
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
    
    //day_after_tomorrow
    NSString *strdate2=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_tomorrow"] objectForKey:@"prayer_date"];
    
    NSString *Tmmaghrib2=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_tomorrow"] objectForKey:@"maghrib"];
    
    NSString *alarm2=[NSString stringWithFormat:@"%@ %@",strdate2,Tmmaghrib2];
    
    NSDate * dateis2=[self StringToNotidate:alarm2];
    
    [self dateComparision:dateis2];
    
    if (isTokonValid ==YES)
    {
        NSLog(@"day_after_tomorrow Maghrib Alarm on");
        
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = dateis2;
        
        NSString *str_newTime=[NSString stringWithFormat:@"Maghrib - %@",[self TimeFormateChange:Tmmaghrib2]];
        localNotification.alertBody = str_newTime;
        
        localNotification.soundName = @"alarm_sound.m4r";
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.applicationIconBadgeNumber = 0;
        NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
        [userinfo setValue:[NSString stringWithFormat:@"Maghrib %@",dateis2] forKey:@"AlarmName"];
        [userinfo setValue:@"6" forKey:@"NT"];
        localNotification.userInfo=userinfo;
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
    
    
    //day_after_4_prayer_time
    NSString *strdate3=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_4_prayer_time"] objectForKey:@"prayer_date"];
    
    NSString *Tmmaghrib3=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_4_prayer_time"] objectForKey:@"maghrib"];
    
    NSString *alarm3=[NSString stringWithFormat:@"%@ %@",strdate3,Tmmaghrib3];
    
    NSDate * dateis3=[self StringToNotidate:alarm3];
    
    [self dateComparision:dateis3];
    
    if (isTokonValid ==YES)
    {
        NSLog(@"day_after_4_prayer_time Maghrib Alarm on");
        
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = dateis3;
        
        NSString *str_newTime=[NSString stringWithFormat:@"Maghrib - %@",[self TimeFormateChange:Tmmaghrib3]];
        
        localNotification.alertBody = str_newTime;
        
        localNotification.soundName = @"alarm_sound.m4r";
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.applicationIconBadgeNumber = 0;
        NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
        [userinfo setValue:[NSString stringWithFormat:@"Maghrib %@",dateis3] forKey:@"AlarmName"];
        [userinfo setValue:@"6" forKey:@"NT"];
        localNotification.userInfo=userinfo;
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
    
    //day_after_5_prayer_time
    NSString *strdate4=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_5_prayer_time"] objectForKey:@"prayer_date"];
    
    NSString *Tmmaghrib4=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_5_prayer_time"] objectForKey:@"maghrib"];
    
    NSString *alarm4=[NSString stringWithFormat:@"%@ %@",strdate4,Tmmaghrib4];
    
    NSDate * dateis4=[self StringToNotidate:alarm4];
    
    [self dateComparision:dateis4];
    
    if (isTokonValid ==YES)
    {
        NSLog(@"day_after_5_prayer_time Maghrib Alarm on");
        
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = dateis4;
        NSString *str_newTime=[NSString stringWithFormat:@"Maghrib - %@",[self TimeFormateChange:Tmmaghrib4]];
        
        localNotification.alertBody = str_newTime;
        localNotification.soundName = @"alarm_sound.m4r";
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.applicationIconBadgeNumber = 0;
        NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
        [userinfo setValue:[NSString stringWithFormat:@"Maghrib %@",dateis4] forKey:@"AlarmName"];
        
        [userinfo setValue:@"6" forKey:@"NT"];
        localNotification.userInfo=userinfo;
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
    
    //day_after_6_prayer_time
    NSString *strdate5=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_6_prayer_time"] objectForKey:@"prayer_date"];
    
    NSString *Tmmaghrib5=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_6_prayer_time"] objectForKey:@"maghrib"];
    
    NSString *alarm5=[NSString stringWithFormat:@"%@ %@",strdate5,Tmmaghrib5];
    
    NSDate * dateis5=[self StringToNotidate:alarm5];
    
    [self dateComparision:dateis5];
    
    if (isTokonValid ==YES)
    {
        NSLog(@"day_after_6_prayer_time Maghrib Alarm on");
        
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = dateis5;
        NSString *str_newTime=[NSString stringWithFormat:@"Maghrib - %@",[self TimeFormateChange:Tmmaghrib5]];
        
        localNotification.alertBody = str_newTime;
        localNotification.soundName = @"alarm_sound.m4r";
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.applicationIconBadgeNumber = 0;
        NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
        [userinfo setValue:[NSString stringWithFormat:@"Maghrib %@",dateis5] forKey:@"AlarmName"];
        [userinfo setValue:@"6" forKey:@"NT"];
        localNotification.userInfo=userinfo;
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
    
    //day_after_7_prayer_time
    NSString *strdate6=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_7_prayer_time"] objectForKey:@"prayer_date"];
    
    NSString *Tmmaghrib6=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_7_prayer_time"] objectForKey:@"maghrib"];
    
    NSString *alarm6=[NSString stringWithFormat:@"%@ %@",strdate6,Tmmaghrib6];
    
    NSDate * dateis6=[self StringToNotidate:alarm6];
    
    [self dateComparision:dateis6];
    
    if (isTokonValid ==YES)
    {
        NSLog(@"day_after_7_prayer_time Maghrib Alarm on");
        
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = dateis6;
        NSString *str_newTime=[NSString stringWithFormat:@"Maghrib - %@",[self TimeFormateChange:Tmmaghrib6]];
        
        localNotification.alertBody = str_newTime;
        localNotification.soundName = @"alarm_sound.m4r";
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.applicationIconBadgeNumber = 0;
        NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
        [userinfo setValue:[NSString stringWithFormat:@"Maghrib %@",dateis6] forKey:@"AlarmName"];
        [userinfo setValue:@"6" forKey:@"NT"];
        localNotification.userInfo=userinfo;
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
}
-(void)MidnightSwitchAlaram
{
    [self cancelAllOldNotificationForNT:@"7"];
    
    
    //today
    NSString *strdatetoday=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"today"] objectForKey:@"prayer_date"];
    
    NSString *Tmimsaaktoday=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"today"] objectForKey:@"midnight"];
    
    NSString *alarm1today=[NSString stringWithFormat:@"%@ %@",strdatetoday,Tmimsaaktoday];
    
    NSDate * dateistoday=[self StringToNotidate:alarm1today];
    
    [self dateComparision:dateistoday];
    if (isTokonValid ==YES)
    {
        NSLog(@"midnight Alarm on");
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = dateistoday;
        
        NSString *str_newTime=[NSString stringWithFormat:@"Midnight - %@",[self TimeFormateChange:Tmimsaaktoday]];
        
        localNotification.alertBody = str_newTime;
        localNotification.soundName = @"alarm_sound.m4r";
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.applicationIconBadgeNumber = 0;
        NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
        [userinfo setValue:[NSString stringWithFormat:@"midnight %@",dateistoday] forKey:@"AlarmName"];
        [userinfo setValue:@"7" forKey:@"NT"];
        localNotification.userInfo=userinfo;
        
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
    
    
    
    //tomtomorrow
    NSString *strdate=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"tomorrow"] objectForKey:@"prayer_date"];
    
    NSString *Tmmidnight=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"tomorrow"] objectForKey:@"midnight"];
    
    NSString *alarm1=[NSString stringWithFormat:@"%@ %@",strdate,Tmmidnight];
    
    NSDate * dateis=[self StringToNotidate:alarm1];
    
    [self dateComparision:dateis];
    
    if (isTokonValid ==YES)
    {
        NSLog(@"tomtomorrow Midnight Alarm on");
        
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = dateis;
        NSString *str_newTime=[NSString stringWithFormat:@"Midnight - %@",[self TimeFormateChange:Tmmidnight]];
        
        localNotification.alertBody = str_newTime;
        localNotification.soundName = @"alarm_sound.m4r";
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.applicationIconBadgeNumber = 0;
        NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
        [userinfo setValue:[NSString stringWithFormat:@"Midnight %@",dateis] forKey:@"AlarmName"];
        [userinfo setValue:@"7" forKey:@"NT"];
        localNotification.userInfo=userinfo;
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
    
    //day_after_tomorrow
    NSString *strdate2=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_tomorrow"] objectForKey:@"prayer_date"];
    
    NSString *Tmmidnight2=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_tomorrow"] objectForKey:@"midnight"];
    
    NSString *alarm2=[NSString stringWithFormat:@"%@ %@",strdate2,Tmmidnight2];
    
    NSDate * dateis2=[self StringToNotidate:alarm2];
    
    [self dateComparision:dateis2];
    
    if (isTokonValid ==YES)
    {
        NSLog(@"day_after_tomorrow Midnight Alarm on");
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = dateis2;
        NSString *str_newTime=[NSString stringWithFormat:@"Midnight - %@",[self TimeFormateChange:Tmmidnight2]];
        
        localNotification.alertBody = str_newTime;
        localNotification.soundName = @"alarm_sound.m4r";
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.applicationIconBadgeNumber = 0;
        NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
        [userinfo setValue:[NSString stringWithFormat:@"Midnight %@",dateis2] forKey:@"AlarmName"];
        [userinfo setValue:@"7" forKey:@"NT"];
        localNotification.userInfo=userinfo;
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
    
    //day_after_4_prayer_time
    NSString *strdate3=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_4_prayer_time"] objectForKey:@"prayer_date"];
    
    NSString *Tmmidnight3=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_4_prayer_time"] objectForKey:@"midnight"];
    
    NSString *alarm3=[NSString stringWithFormat:@"%@ %@",strdate3,Tmmidnight3];
    
    NSDate * dateis3=[self StringToNotidate:alarm3];
    
    [self dateComparision:dateis3];
    
    if (isTokonValid ==YES)
    {
        NSLog(@"day_after_4_prayer_time Midnight Alarm on");
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = dateis3;
        NSString *str_newTime=[NSString stringWithFormat:@"Midnight - %@",[self TimeFormateChange:Tmmidnight3]];
        
        localNotification.alertBody = str_newTime;
        localNotification.soundName = @"alarm_sound.m4r";
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.applicationIconBadgeNumber = 0;
        NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
        [userinfo setValue:[NSString stringWithFormat:@"Midnight %@",dateis3] forKey:@"AlarmName"];
        [userinfo setValue:@"7" forKey:@"NT"];
        localNotification.userInfo=userinfo;
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
    
    //day_after_5_prayer_time
    NSString *strdate4=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_5_prayer_time"] objectForKey:@"prayer_date"];
    
    NSString *Tmmidnight4=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_5_prayer_time"] objectForKey:@"midnight"];
    
    NSString *alarm4=[NSString stringWithFormat:@"%@ %@",strdate4,Tmmidnight4];
    
    NSDate * dateis4=[self StringToNotidate:alarm4];
    
    [self dateComparision:dateis4];
    
    if (isTokonValid ==YES)
    {
        NSLog(@"day_after_5_prayer_time Midnight Alarm on");
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = dateis4;
        NSString *str_newTime=[NSString stringWithFormat:@"Midnight - %@",[self TimeFormateChange:Tmmidnight4]];
        
        localNotification.alertBody = str_newTime;        localNotification.soundName = @"alarm_sound.m4r";
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.applicationIconBadgeNumber = 0;
        NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
        [userinfo setValue:[NSString stringWithFormat:@"Midnight %@",dateis4] forKey:@"AlarmName"];
        [userinfo setValue:@"7" forKey:@"NT"];
        localNotification.userInfo=userinfo;
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
    
    //day_after_6_prayer_time
    NSString *strdate5=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_6_prayer_time"] objectForKey:@"prayer_date"];
    
    NSString *Tmmidnight5=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_6_prayer_time"] objectForKey:@"midnight"];
    
    NSString *alarm5=[NSString stringWithFormat:@"%@ %@",strdate5,Tmmidnight5];
    
    NSDate * dateis5=[self StringToNotidate:alarm5];
    
    [self dateComparision:dateis5];
    
    if (isTokonValid ==YES)
    {
        NSLog(@"day_after_6_prayer_time Midnight Alarm on");
        
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = dateis5;
        NSString *str_newTime=[NSString stringWithFormat:@"Midnight - %@",[self TimeFormateChange:Tmmidnight5]];
        
        localNotification.alertBody = str_newTime;
        localNotification.soundName = @"alarm_sound.m4r";
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.applicationIconBadgeNumber = 0;
        NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
        [userinfo setValue:[NSString stringWithFormat:@"Midnight %@",dateis5] forKey:@"AlarmName"];
        [userinfo setValue:@"7" forKey:@"NT"];
        localNotification.userInfo=userinfo;
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
    
    //day_after_7_prayer_time
    NSString *strdate6=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_7_prayer_time"] objectForKey:@"prayer_date"];
    
    NSString *Tmmidnight6=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_7_prayer_time"] objectForKey:@"midnight"];
    
    NSString *alarm6=[NSString stringWithFormat:@"%@ %@",strdate6,Tmmidnight6];
    
    NSDate * dateis6=[self StringToNotidate:alarm6];
    
    [self dateComparision:dateis6];
    
    if (isTokonValid ==YES)
    {
        NSLog(@"day_after_7_prayer_time Midnight Alarm on");
        
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = dateis6;
        NSString *str_newTime=[NSString stringWithFormat:@"Midnight - %@",[self TimeFormateChange:Tmmidnight6]];
        localNotification.alertBody = str_newTime;
        localNotification.soundName = @"alarm_sound.m4r";
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.applicationIconBadgeNumber = 0;
        NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
        [userinfo setValue:[NSString stringWithFormat:@"Midnight %@",dateis6] forKey:@"AlarmName"];
        [userinfo setValue:@"7" forKey:@"NT"];
        localNotification.userInfo=userinfo;
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
}
#pragma mark - Alarm Swich On/Off Clicked
- (IBAction)segmentSwitch:(UISegmentedControl *)sender
{
    NSIndexPath *path = [self indexPathWithSubview:(UIButton *)sender];
    HomeCell1 * cell = (HomeCell1 *)[self.tblListAlarm cellForRowAtIndexPath:path];
    
    NSInteger selectedSegment = sender.selectedSegmentIndex;
    
    
    if (selectedSegment == 0)
    {
        if (sender.tag==0)
        {
            NSLog(@"switch1 off");
            cell.imgWatch.image=[UIImage imageNamed:@"red_alamIcon"];
            [[NSUserDefaults standardUserDefaults]setObject:@"off" forKey:@"Switch1"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [self cancelAllOldNotificationForNT:@"1"];
        }
        else if (sender.tag==1)
        {
            NSLog(@"switch2 off");
            cell.imgWatch.image=[UIImage imageNamed:@"red_alamIcon"];
            [[NSUserDefaults standardUserDefaults]setObject:@"off" forKey:@"Switch2"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [self cancelAllOldNotificationForNT:@"2"];
        }
        else if (sender.tag==2)
        {
            NSLog(@"switch3 off");
            cell.imgWatch.image=[UIImage imageNamed:@"red_alamIcon"];
            [[NSUserDefaults standardUserDefaults]setObject:@"off" forKey:@"Switch3"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [self cancelAllOldNotificationForNT:@"3"];
        }
        else if (sender.tag==3)
        {
            NSLog(@"switch4 off");
            cell.imgWatch.image=[UIImage imageNamed:@"red_alamIcon"];
            [[NSUserDefaults standardUserDefaults]setObject:@"off" forKey:@"Switch4"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [self cancelAllOldNotificationForNT:@"4"];
        }
        else if (sender.tag==4)
        {
            NSLog(@"switch5 off");
            cell.imgWatch.image=[UIImage imageNamed:@"red_alamIcon"];
            [[NSUserDefaults standardUserDefaults]setObject:@"off" forKey:@"Switch5"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [self cancelAllOldNotificationForNT:@"5"];
        }
        else if (sender.tag==5)
        {
            NSLog(@"switch6 off");
            cell.imgWatch.image=[UIImage imageNamed:@"red_alamIcon"];
            [[NSUserDefaults standardUserDefaults]setObject:@"off" forKey:@"Switch6"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [self cancelAllOldNotificationForNT:@"6"];
        }
        else
        {
            NSLog(@"switch7 off");
            cell.imgWatch.image=[UIImage imageNamed:@"red_alamIcon"];
            [[NSUserDefaults standardUserDefaults]setObject:@"off" forKey:@"Switch7"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [self cancelAllOldNotificationForNT:@"7"];
        }
        
    }
    else
    {
        if (sender.tag==0)
        {
            cell.imgWatch.image=[UIImage imageNamed:@"green_alamIcon"];
            NSLog(@"switch1 on");
            [[NSUserDefaults standardUserDefaults]setObject:@"on" forKey:@"Switch1"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [self ImssakSwitchAlaram];
            
        }
        else if (sender.tag==1)
        {
            NSLog(@"switch2 on");
            cell.imgWatch.image=[UIImage imageNamed:@"green_alamIcon"];
            [[NSUserDefaults standardUserDefaults]setObject:@"on" forKey:@"Switch2"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [self FajrSwitchAlaram];
            
        }
        else if (sender.tag==2)
        {
            NSLog(@"switch3 on");
            cell.imgWatch.image=[UIImage imageNamed:@"green_alamIcon"];
            [[NSUserDefaults standardUserDefaults]setObject:@"on" forKey:@"Switch3"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [self SunriseSwitchAlaram];
            
        }
        else if (sender.tag==3)
        {
            NSLog(@"switch4 on");
            cell.imgWatch.image=[UIImage imageNamed:@"green_alamIcon"];
            [[NSUserDefaults standardUserDefaults]setObject:@"on" forKey:@"Switch4"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [self ZoharSwitchAlaram];
            
        }
        else if (sender.tag==4)
        {
            NSLog(@"Sunset switch5 on");
            cell.imgWatch.image=[UIImage imageNamed:@"green_alamIcon"];
            [[NSUserDefaults standardUserDefaults]setObject:@"on" forKey:@"Switch5"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [self SunsetSwitchAlaram];
        }
        else if (sender.tag==5)
        {
            NSLog(@"Maghrib switch6 on");
            cell.imgWatch.image=[UIImage imageNamed:@"green_alamIcon"];
            [[NSUserDefaults standardUserDefaults]setObject:@"on" forKey:@"Switch6"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [self MaghribSwitchAlaram];
            
        }
        else
        {
            NSLog(@"switch7 on");
            cell.imgWatch.image=[UIImage imageNamed:@"green_alamIcon"];
            [[NSUserDefaults standardUserDefaults]setObject:@"on" forKey:@"Switch7"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [self MidnightSwitchAlaram];
            
        }
    }
    //[[NSUserDefaults standardUserDefaults] synchronize];
    
    
    [tblListAlarm beginUpdates];
    [tblListAlarm reloadRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationNone];
    [tblListAlarm endUpdates];
    
    //[tblListAlarm reloadData];
    
    
}

#pragma mark - Date changer Method
-(NSString *)changeYerFormate:(NSString *)dateis
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd";
    NSDate *date = [dateFormatter dateFromString:dateis];
    dateFormatter.dateFormat = @"dd MMMM yyyy";
    return [dateFormatter stringFromDate:date];
}
-(NSString *)GetDayName:(NSString *)dateis
{
    NSString *dateString =[ArrTodayArt valueForKey:@"prayer_date"];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [[NSDate alloc] init];
    date = [dateFormatter dateFromString:dateString];
    [dateFormatter setDateFormat:@"EEEE"];
    return [dateFormatter stringFromDate:date];
}
-(NSString *)TimeFormateChange:(NSString *)DateStr
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"HH:mm:ss";
    NSDate *date = [dateFormatter dateFromString:DateStr];
    dateFormatter.dateFormat = @"hh:mm a";
    return [dateFormatter stringFromDate:date];
}

#pragma mark- Curent date and time
- (NSDate *)currentDateandTime
{
    NSDate *today = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *dateString = today;
    return dateString;
}
-(BOOL)dateComparision:(NSDate*)date1
{
    
    NSDate *CurrentDate=[self currentDateandTime];
    
    if ([date1 compare:CurrentDate] == NSOrderedDescending)
    {
        NSLog(@"curent date is less");
        isTokonValid = YES;
    }
    else if ([date1 compare:CurrentDate] == NSOrderedAscending)
    {
        NSLog(@"server date is less");
        isTokonValid = NO;
    }
    else
    {
        NSLog(@"Both dates are same");
        isTokonValid = YES;
    }
    return isTokonValid;
    
    
    
}

#pragma mark -
#pragma mark - didReceiveMemoryWarning
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


////bg Fatch


- (void)alarmSchedulerWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    NSString *registerURL = [NSString stringWithFormat:@"%@get_salat_data_web.php",kAPIURL];
    bgFetchHTTPClient = [[HTTPClient alloc] init];
    bgFetchHTTPClient.delegate = self;
    [bgFetchHTTPClient getResponseFromAPI:registerURL andParameters:nil];
    
    
    
    /*
     At the end of the fetch, invoke the completion handler.
     */
    completionHandler(UIBackgroundFetchResultNewData);
}
-(void)SetLogInTextFile:(NSString *)content
{
    // NSString *content = @"This is my log";
    //Get the file path
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *fileName = [documentsDirectory stringByAppendingPathComponent:@"LogFile.txt"];
    
    //create file if it doesn't exist
    if(![[NSFileManager defaultManager] fileExistsAtPath:fileName])
        [[NSFileManager defaultManager] createFileAtPath:fileName contents:nil attributes:nil];
    
    //append text to file (you'll probably want to add a newline every write)
    NSFileHandle *file = [NSFileHandle fileHandleForUpdatingAtPath:fileName];
    [file seekToEndOfFile];
    [file writeData:[content dataUsingEncoding:NSUTF8StringEncoding]];
    [file closeFile];
}
-(void)setagain_click
{
    HomeTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"home"] imageDisabled:[UIImage imageNamed:@"home"]];
    HomeTabItem.titleString=@"Home";
    HomeTabItem.titleFontColor=[UIColor whiteColor];
    HomeTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];
    
    FirstTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabprayIconY"] imageDisabled:[UIImage imageNamed:@"TabprayIconY"]];
    FirstTabItem.titleString=@"Prayer Times";
    FirstTabItem.titleFontColor=[UIColor whiteColor];
    FirstTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];
    
    ThirdTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcalendarIconY"] imageDisabled:[UIImage imageNamed:@"TabcalendarIconW"]];
    ThirdTabItem.titleString=@"Calendar";
    ThirdTabItem.titleFontColor=[UIColor whiteColor];
    ThirdTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];
    
    FiveTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabvideoIconW"] imageDisabled:[UIImage imageNamed:@"TabvideoIconW"]];
    FiveTabItem.titleString=@"Media";
    FiveTabItem.titleFontColor=[UIColor whiteColor];
    FiveTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];
    
    SixTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcontactBookIconY"] imageDisabled:[UIImage imageNamed:@"TabcontactBookIconW"]];
    SixTabItem.titleString=@"Contact";
    SixTabItem.titleFontColor=[UIColor whiteColor];
    SixTabItem.titleFont=[UIFont systemFontOfSize:10 weight:4];
    
    FirstTabItem.tabState = TabStateEnabled;
    
    RKTabView *tabView = [[RKTabView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-60,self.view.bounds.size.width, 60)];
    tabView.delegate = self;
    tabView.horizontalInsets = HorizontalEdgeInsetsMake(0, 0);;
    
    tabView.backgroundColor=[UIColor colorWithRed:(48.0f/255.0f) green:(82.0f/255.0f) blue:(1.0f/255.0f) alpha:1];
    
    tabView.tabItems =@[HomeTabItem,FirstTabItem, ThirdTabItem, FiveTabItem,  SixTabItem];
    
    [self.view addSubview:tabView];
    
}

@end
