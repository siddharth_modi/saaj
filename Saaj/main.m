//
//  main.m
//  Saaj
//
//  Created by mac  on 12/27/16.
//  Copyright © 2016 mac . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
